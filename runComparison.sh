#! /bin/bash

./build-cmake/src/amdis-vonKarman init/explicit.dat &
./build-cmake/src/amdis-vonKarman init/implicit.dat &
./build-cmake/src/amdis-vonKarman init/newton.dat &
./build-cmake/src/amdis-vonKarman init/globalizedNewton.dat &
