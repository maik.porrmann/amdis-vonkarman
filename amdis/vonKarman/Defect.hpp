#pragma once

#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/gridfunctions/AnalyticGridFunction.hpp>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <amdis/Initfile.hpp>
namespace AMDiS::VonKarman
{
  template <class K, int n, int d = 0>
  class Gaussian
  {
  public:
    Gaussian(Dune::FieldVector<K, n> const& x, K eps): x_0(x), eps_(eps) {}

    auto operator()(Dune::FieldVector<K, n> const& x) const
    {
      // value of gaussian
      auto diff = x - x_0;
      auto value = std::exp(-(diff) * (diff) / (2. * eps_)) / (std::pow(2. * pi * eps_, n / 2.));
      if constexpr (d == 0)
        return value;
      else if constexpr (d == 1)
        return -value * (diff) / (eps_);
      else if constexpr (d == 2)
      {
        Dune::FieldMatrix<K, n, n> diffMat{{diff[0] * diff[0], diff[0] * diff[1]},
                                           {diff[0] * diff[1], diff[1] * diff[1]}};
        return value / (eps_ * eps_) * diffMat;
      }
      else
      {
        DUNE_THROW(Dune::NotImplemented, "derivative higher than 2 not implemented");
        return 0.;
      }
    }

    friend auto derivativeOf(Gaussian<K, n, d> const& t, tag::gradient)
    {
      return Gaussian<K, n, d + 1>(t.x_0, t.eps_);
    }

  private:
    static constexpr K pi = 2. * std::acos(0.);
    Dune::FieldVector<K, n>const& x_0;
    K eps_;
  };


  struct Dislocation {
    Dune::FieldVector<double, 2> burger_;
    Dune::FieldVector<double, 2> position_;

    template<class GridView>
    auto getApproximation(GridView const& gv, double eps = 0.01) const
    {
      auto dislocationTerm = evalAtQP(VonKarman::Gaussian<double, 2, 1>(position_, eps));
      return makeGridFunction(2. * Dune::FieldVector<double, 2>{-burger_[1], burger_[0]} *dislocationTerm, gv);
    }

    template<class DiscreteFunction>
    double assemble(DiscreteFunction const& phi) const
    {
      return 2. * Dune::FieldVector<double, 2>{-burger_[1], burger_[0]} *derivativeOf(phi, tag::jacobian{})(position_);
    }

    Dune::FieldVector<double, 2> position() const { return position_; }
    Dune::FieldVector<double, 2> burger() const { return burger_; }

    Dislocation const& rotate(double rad)
    {
      Dune::FieldMatrix<double, 2, 2> mat = {{std::cos(rad), -std::sin(rad)}, {std::sin(rad), std::cos(rad)}};
      burger_ = mat * burger_;
      return *this;
    }

    Dislocation const& move(Dune::FieldVector<double, 2> shift)
    {
      position_ += shift;
      return *this;
    }

  };

  struct Disclination{
    double strength_;
    Dune::FieldVector<double, 2> position_;

    template<class GridView>
    auto getApproximation(GridView const& gv, double eps = 0.01) const
    {
      auto disclinationTerm  = evalAtQP(VonKarman::Gaussian<double, 2, 0>(position_, eps));
      return makeGridFunction(strength_ * disclinationTerm, gv);
    }

    template<class DiscreteFunction>
    double assemble(DiscreteFunction const& phi) const
    {
      return strength_ *phi(position_);
    }
    Dune::FieldVector<double, 2> position() const { return position_; };
    double strength() const { return strength_; }

  };

  auto createDislocation(std::string prefix, int i, bool relative = false, Dune::FieldVector<double, 2> shift = {0.,0.})
  {
    Dune::FieldVector<double, 2> position =
      Parameters::get<Dune::FieldVector<double, 2> >(prefix + "[" + std::to_string(i) + "]->position").value();

    if (relative)
    {
      Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value();
      position[0] *= upperRight[0];
      position[1] *= upperRight[1];
    }
    if (Parameters::get<bool>(prefix + "[" + std::to_string(i) + "]->shifted").value_or(false))
      position += shift;
    return Dislocation{
      Parameters::get<Dune::FieldVector<double, 2> >(prefix + "["+std::to_string(i)+"]->burger").value(),
      position};
  }

  auto createDisclination(std::string prefix, int i, bool relative = false, Dune::FieldVector<double, 2> shift = {0.,0.})
  {
    Dune::FieldVector<double, 2> position =
      Parameters::get<Dune::FieldVector<double, 2> >(prefix + "[" + std::to_string(i) + "]->position").value();
    if (relative)
    {
      Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value();
      position[0] *= upperRight[0];
      position[1] *= upperRight[1];
    }
    if (Parameters::get<bool>(prefix + "[" + std::to_string(i) + "]->shifted").value_or(false))
      position += shift;
    // throw error if has no value
    return Disclination{
      Parameters::get<double>(prefix + "["+std::to_string(i)+"]->strength").value(),
      position};
  }
} // namespace AMDiS::VonKarman