#pragma once
#include "Defect.hpp"
#include "amdis/gridfunctions/AnalyticGridFunction.hpp"
#include "dune/common/fmatrix.hh"
#include "dune/functions/common/differentiablefunctionfromcallables.hh"
#include <amdis/Output.hpp>
#include <amdis/PointwiseConstraint.hpp>
#include <amdis/io/VTKSequenceWriter.hpp>
#include <complex>
#include <dune/common/float_cmp.hh>
#include <dune/grid/io/file/vtk/common.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>
#include <dune/vtk/periodicdatacollector.hh>
#include <dune/vtk/pvdwriter.hh>
#include <dune/vtk/writers/vtkunstructuredgridwriter.hh>
#include <filesystem>

namespace fs = std::filesystem;

namespace AMDiS{
inline void adaptPaths(std::string outputDir, std::string type) {
  auto newtonSolutionPath = Parameters::get<std::string>("NewtonStep->" + type + "->solution")
                                .value_or("newton.solution");
  Parameters::set("NewtonStep->" + type + "->solution",
                  outputDir + "/backup/" + newtonSolutionPath);

  auto newtonGridPath =
      Parameters::get<std::string>("NewtonStep->" + type + "->grid").value_or("newton.grid");
  Parameters::set("NewtonStep->" + type + "->grid", outputDir + "/backup/" + newtonGridPath);
  // both problems work on the same grid
  Parameters::set("GradientFlow->" + type + "->grid", outputDir + "/backup/" + newtonGridPath);

  auto gradientflowSolutionPath =
      Parameters::get<std::string>("GradientFlow->" + type + "->solution")
          .value_or("gradientflow.solution");
  Parameters::set("GradientFlow->" + type + "->solution",
                  outputDir + "/backup/" + gradientflowSolutionPath);
}

inline void adaptBackupAndRestorePaths(std::string outputDir) {
  // adaptPaths(outputDir, "backup");
  adaptPaths(outputDir, "restore");
}

namespace VonKarman {
template <class K>
Dune::FieldMatrix<K, 2, 2>
cof(Dune::FieldMatrix<K, 2, 2> const &mat) {
  return {{mat[1][1], -mat[0][1]}, {-mat[1][0], mat[0][0]}};
}

template<class K>
K tr(Dune::FieldMatrix<K, 2, 2> const& mat)
{
  return mat[0][0] + mat[1][1];
}

template<class K>
Dune::FieldVector<K, 2> rowTr(Dune::FieldMatrix<Dune::FieldVector<K,2>, 2, 2> const& tensor)
{
  return { tensor[0][0][0] + tensor[0][1][1], tensor[1][0][0] + tensor[1][1][1] };
}

template <class K>
K frobeniusProduct(Dune::FieldMatrix<K, 2, 2> const &left,
                          Dune::FieldMatrix<K, 2, 2> const &right) {
  return left[0][0] * right[0][0] + left[1][0] * right[1][0] +
         left[0][1] * right[0][1] + left[1][1] * right[1][1];
}

template<class K = double, int dim = 2>
Dune::FieldMatrix<K, dim, dim> identity(){
  Dune::FieldMatrix<K,dim,dim> id = 0;
  for (std::size_t i = 0; i < dim; ++i)
    id[i][i] = 1.;
  return id;
}


  template<class Vec1, class Vec2, class Vec3>
    void axPy(Vec1& result, double scalar, Vec2 const& x, Vec3 const& y)
    {
      auto op = Operation::compose(Operation::Plus{},
        Operation::compose(Operation::Scale<double>{scalar},
          Operation::Arg<0>{}),
        Operation::Arg<1>{});
      Recursive::transform(result.impl(), op, x.impl(), y.impl());
    }

    template<class Vec1, class Vec2, class Vec3>
    void axPby(Vec1& result, double a, Vec2 const& x, double b, Vec3 const& y)
    {
      auto op = Operation::compose(Operation::Plus{},
        Operation::compose(Operation::Scale<double>{a},
          Operation::Arg<0>{}),
        Operation::compose(Operation::Scale<double>{b},
          Operation::Arg<1>{}));
      Recursive::transform(result.impl(), op, x.impl(), y.impl());
    }


    template <class GV>
     double getMeshWidth(GV const& gv)
    {
      static constexpr int dim = GV::dimension;
      double h = 0;
      for (auto const &e : elements(gv)) {
        auto geo = e.geometry();
        auto n = geo.corners();
        for (int i = 0; i < n; ++i)
        {
          auto v0 = geo.corner(i);
          auto v1 = geo.corner((i+1) % n);
          h = std::max(h, (v0 - v1).two_norm());
        }
      }

      return h;
    }

  template <class F>
   auto laplace(F const& f)
  {
    static_assert(static_size_v<typename F::Range> == 1);
    auto Hf = gradientOf(gradientOf(f));
    return invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<0>{}, Hf))
      + invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<1>{}, Hf));
  }

  template <class F>
   auto det(F const& f)
  {
    // F is DerivativePreGridFunction, aka has dimOfWorld dependent Range
    // static_assert(F::Range::rows == 2, " det only implemented for static 2x2 matrices");
    // static_assert(F::Range::cols == 2, " det only implemented for static 2x2 matrices");
    return invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<0>{}, f))
      * invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<1>{}, f))
      - invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<1>{}, f))
      * invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<0>{}, f));
  }

  template <class Mat>
   auto MatrixInnerProduct(Mat const& m1, Mat const& m2)
  {
    return invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<0>{}, m1))
      * invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<0>{}, m2))
      + invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<1>{}, m1))
      * invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<1>{}, m2))
      + invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<0>{}, m1))
      * invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<0>{}, m2))
      + invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<1>{}, m1))
      * invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<1>{}, m2));
  }

  template <class F>
   auto detHessian(F const& f)
  {
    static_assert(static_size_v<typename F::Range> == 1);
    auto Hf = gradientOf(gradientOf(f));
    return det(Hf);
  }

  template <class F, class G>
   auto MongeAmpereBracket(F const& f, G const& g)
  {
    auto Hf = gradientOf(gradientOf(f));
    auto Hg = gradientOf(gradientOf(g));

    return invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<0>{}, Hf))
      * invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<1>{}, Hg))
      + invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<1>{}, Hf))
      * invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<0>{}, Hg))
      - invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<1>{}, Hf))
      * invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<0>{}, Hg))
      - invokeAtQP(Operation::Get<1>{}, invokeAtQP(Operation::Get<0>{}, Hf))
      * invokeAtQP(Operation::Get<0>{}, invokeAtQP(Operation::Get<1>{}, Hg));
  }

  template <class F>
   auto normal(F const& f)
  {
    static_assert(static_size_v<typename F::Domain> == 2 && static_size_v<typename F::Range> == 1,
      "normal only implemented for scalar functions on 2d domain");
    auto df = gradientOf(f);
    auto makeNormal = [](auto const& x) { return Dune::FieldVector<double, 2>{x[1], -x[0]}; };
    return invokeAtQP(makeNormal, df);
  }

  template <class SolutionStress, class SolutionDisp, class GridView>
   double definiteCiterion(SolutionStress const& phi, SolutionDisp const& w, GridView const& gv)
  {
    return 0.5 * integrate(phi * detHessian(w), gv);
  }

  template <class SolutionDisp, class GridView>
   double displacementIncompatibility(SolutionDisp const& w, GridView const& gv)
  {
    return integrate(detHessian(w), gv);
  }

  template <class SolutionDisp, class GridView>
   double displacementIncompatibilityNorm(SolutionDisp const& w, GridView const& gv)
  {
    return sqrt(integrate(sqr(detHessian(w)), gv));
  }

  template <class SolutionStress, class SolutionDisp, class GridView>
   double Energy(SolutionStress const& phi, SolutionDisp const& w, GridView const& gv, double D = 1.,
    double E = 1., double nu = 0.0)
  {
    return integrate(D / 2. * sqr(laplace(w)) - D * (1. - nu) * detHessian(w)
      - 1. / (2. * E) * sqr(laplace(phi)) + (1 + nu) / E * detHessian(phi)
      + 0.5 * (gradientOf(gradientOf(phi)) * normal(w)) * normal(w),
      gv, 10);
  }

  template <class SolutionStress, class SolutionDisp, class Force, class GridView>
   double EnergyWithForce(SolutionStress const& phi, SolutionDisp const& w, Force const& f,
    GridView const& gv, double D = 1., double E = 1., double nu = 0.0)
  {
    return integrate(D / 2. * sqr(laplace(w)) - D * (1. - nu) * detHessian(w)
      - 1. / (2. * E) * sqr(laplace(phi)) + (1 + nu) / E * detHessian(phi)
      + 0.5 * (gradientOf(gradientOf(phi)) * normal(w)) * normal(w) - w * f,
      gv, 10);
  }

  template <class SolutionStress, class SolutionDisp, class Defect, class GridView>
   double EnergyWithDefect(SolutionStress const& phi, SolutionDisp const& w, std::vector<Defect> const& defects,
    GridView const& gv, double D = 1., double E = 1., double nu = 0.0)
  {
    return integrate((D / 2.) * sqr(laplace(w)) - (D * (1. - nu)) * detHessian(w)
      - (1. / (2. * E)) * sqr(laplace(phi)) + ((1 + nu) / E) * detHessian(phi)
      + 0.5 * (gradientOf(gradientOf(phi)) * normal(w)) * normal(w),
      gv, 10) + std::accumulate(defects.begin(), defects.end(), 0.,
        [&](double& init, Defect const& d){ return init + d.assemble(phi);});
  }

  template <class SolutionVector, class GridView>
   double directionalDerivativeAt(SolutionVector x, SolutionVector d, GridView gv, double D = 1.,
    double E = 1., double nu = 0.0)
  {
    auto const& phi = valueOf(x, 0);
    auto const& w = valueOf(x, 1);

    auto const& psi = valueOf(d, 0);
    auto const& v = valueOf(d, 1);

    return integrate(D * laplace(w) * laplace(v) - D * (1. - nu) * MongeAmpereBracket(w, v)
      - 1. / E * laplace(phi) * laplace(psi)
      + (1. + nu) / E * MongeAmpereBracket(phi, psi)
      - phi * MongeAmpereBracket(w, v) - psi * detHessian(w),
      gv, 10);
  }

  inline std::vector<Dune::FieldVector<double,2>> cartesian_product(Dune::FieldVector<double,2>const& x, int repeatPerDirection)
  {
    assert (repeatPerDirection >= 0);
    std::vector<double> left, right;
    std::vector<Dune::FieldVector<double,2>> cart_prod;
    for(int i = - repeatPerDirection; i <= repeatPerDirection; ++i)
    {
      left.push_back(double(i)*x[0]);
      right.push_back(double(i)*x[1]);
    }

    for(auto const& l : left)
      for(auto const& r : right)
        cart_prod.push_back({l,r});
    return cart_prod;
  }

  inline auto getContinuumStress(std::vector<Dislocation> const& dislocations, double E2d, int periodic = 1)
  {
    Dune::FieldVector<double,2> upperRight = Parameters::get<Dune::FieldVector<double,2>>("mesh->upper right").value_or(Dune::FieldVector<double,2>{1., 1.});

    auto lambda = [&dislocations, E2d, periodic, upperRight](auto const& x)->Dune::FieldVector<double,3>
    {
      Dune::FieldVector<double, 3> ret;
      ret = 0;
      // if periodic: include all neighboring domains aswell, i.e. shift position by all combinations of {0.,x} and {0.,y}
      std::vector<Dune::FieldVector<double,2>> shifts = cartesian_product(upperRight, periodic);
      // {{0.,0.}, -upperRight, {-upperRight[0], 0.}, {-upperRight[0], upperRight[1]}, {0.,upperRight[1]}, upperRight, {upperRight[0], 0.}, {upperRight[0], -upperRight[1]}, {0.,-upperRight[1]}};
      // avoid division by zero by removing influence if defect is closer than epslion
      double epsilon = upperRight.two_norm2()/1e8;
      for (auto const& d : dislocations)
      {
        for(auto const& shift : shifts)
        {
          // First is {0.,0.}
          auto x_0 = d.position() + shift;
          auto b = d.burger();
          assert(Dune::FloatCmp::eq(b[1], 0.));
          auto dx = x - x_0;
          auto c = std::complex<double>(dx[0], dx[1]);
          auto r = std::abs(c), phi = std::arg(c);
          if( Dune::FloatCmp::lt(r, epsilon))
            continue;

          auto D = b[0] * E2d / (4. * M_PI);
          ret[0] += -D * std::sin(phi) * (2. + std::cos(2. * phi)) / r;
          ret[1] += D * std::sin(phi) * std::cos(2. * phi) / r;
          ret[2] += D * std::cos(phi) * std::cos(2. * phi) / r;
          // end loop after original dislocation if not periodic
          if(!periodic)
            break;
        }
      }
      return ret;
    };
    return lambda;
  }

  inline auto getContinuumAiry(std::vector<Dislocation> const& dislocations, double E2d, int periodic = 1)
  {
    Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});

    auto lambda = [&dislocations, E2d, periodic, upperRight](auto const& x)->double
    {
      double ret = 0.;
      // if periodic: include all neighboring domains aswell, i.e. shift position by all combinations of {0.,x} and {0.,y}
      std::vector<Dune::FieldVector<double, 2>> shifts = cartesian_product(upperRight, periodic);
      // {{0.,0.}, -upperRight, {-upperRight[0], 0.}, {-upperRight[0], upperRight[1]}, {0.,upperRight[1]}, upperRight, {upperRight[0], 0.}, {upperRight[0], -upperRight[1]}, {0.,-upperRight[1]}};
      // avoid division by zero by removing influence if defect is closer than epslion
      double epsilon = upperRight.two_norm2() / 1e8;
      for (auto const& d : dislocations)
      {
        for (auto const& shift : shifts)
        {
          // First is {0.,0.}
          auto x_0 = d.position() + shift;
          auto b = d.burger();
          assert(Dune::FloatCmp::eq(b[1], 0.));
          auto dx = x - x_0;
          auto c = std::complex<double>(dx[0], dx[1]);
          auto r = std::abs(c), phi = std::arg(c);
          if (Dune::FloatCmp::lt(r, epsilon))
            continue;

          auto D = b[0] * E2d / (4. * M_PI);
          ret += D * r * std::log(r);

        }
      }
      return ret;
    };
    return evalAtQP(lambda);
  }

  inline auto getConstantFunction(double c)
  {
    auto f = [=](Dune::FieldVector<double, 2> const& x) -> double { return {c}; };
    auto df = [](Dune::FieldVector<double, 2> const& x) -> Dune::FieldVector<double, 2> {
      return {0., 0.};
    };
    auto Hf = [](Dune::FieldVector<double, 2> const& x) -> Dune::FieldMatrix<double, 2, 2> {
      return {{0., 0.}, {0., 0.}};
    };

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<double(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>{},
      f, df, Hf);
  }

  template<class Defect>
  inline auto getDefectIndicatorFunction(std::vector<Defect> const& defects, double radius)
  {
    // return 1 if closer than radius to some Defect, else 0
    auto f = [defects, radius](Dune::FieldVector<double, 2> const& x) -> double {
      for (auto const& d : defects)
      {
        if((x - d.position()).two_norm2() < radius*radius)
          return 1.;
      }
      return 0.;
    };
    // all derivatives are set to zero
    auto df = [](Dune::FieldVector<double, 2> const& x) -> Dune::FieldVector<double, 2> {
      return {0., 0.};
    };
    auto Hf = [](Dune::FieldVector<double, 2> const& x) -> Dune::FieldMatrix<double, 2, 2> {
      return {{0., 0.}, {0., 0.}};
    };

    // return Dune::Functions::makeDifferentiableFunctionFromCallables(
    //   Dune::Functions::SignatureTag<double(Dune::FieldVector<double, 2>),
    //   JacobianDerivativeTraits>{},
    //   f, df, Hf);
    return f;
  }

  inline auto getFlippingFunction(Dune::FieldVector<double, 2> topRight)
  {
    auto f = [](Dune::FieldVector<double, 2> const& x) -> double
    {
      Dune::FieldVector<double, 2> topRight = {0.5,0.5};
      if ((Dune::FloatCmp::le(x[0], topRight[0]) && Dune::FloatCmp::le(x[1], topRight[1])) || Dune::FloatCmp::ge(x[0], 1.))
        return -1.;
      else
        return 1.;
    };
    auto df = [](Dune::FieldVector<double, 2> const& x) -> Dune::FieldVector<double, 2>
    {
      Dune::FieldVector<double, 2> topRight = {0.5,0.5};

      if ((Dune::FloatCmp::le(x[0], topRight[0]) && Dune::FloatCmp::le(x[1], topRight[1])) || Dune::FloatCmp::ge(x[0], 1.))
        return {-1., -1.};
      else
        return {1.,1.};
    };
    auto Hf = [](Dune::FieldVector<double, 2> const& x) -> Dune::FieldMatrix<double, 2, 2>
    {
      Dune::FieldVector<double, 2> topRight = {0.5,0.5};
      if ((Dune::FloatCmp::le(x[0], topRight[0]) && Dune::FloatCmp::le(x[1], topRight[1])) || Dune::FloatCmp::ge(x[0], 1.))
        return {{-1., -1.},{-1., -1.}};
      else
        return {{1.,1.},{1.,1.}};
    };

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<double(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>{},
      f, df, Hf);
  }

  inline auto getZeroFunction()
  {
    return getConstantFunction(0.);
  }

  inline auto getConstantVectorFunction(Dune::FieldVector<double, 2> val)
  {
    // auto f = [](auto const &x) { return 0.; };
    auto df = [=](auto const& x) { return val; };
    auto Hf = [](auto const& x) { return Dune::FieldMatrix<double, 2, 2>{{0., 0.}, {0., 0.}}; };
    auto dHf = [](auto const& x)
    {
      return Dune::FieldVector<Dune::FieldMatrix<double, 2, 2>, 2>{{{0., 0.}, {0., 0.}},
        {{0., 0.}, {0., 0.}}};
    };
    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 2>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>{},
      df, Hf, dHf);
  }

  inline auto getZeroVectorFunction()
  {
    auto df = [=](auto const& x) { return Dune::FieldVector<double,2>{0.,0.}; };
    auto Hf = [](auto const& x) { return Dune::FieldMatrix<double, 2, 2>{{0., 0.}, {0., 0.}}; };
    auto dHf = [](auto const& x)
    {
      return Dune::FieldVector<Dune::FieldMatrix<double, 2, 2>, 2>{{{0., 0.}, {0., 0.}},
        {{0., 0.}, {0., 0.}}};
    };
    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 2>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>{},
      df, Hf, dHf);
  }

  inline auto get_4thOrder_Polynomial(double const& scale)
  {
    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(),
      [=](auto const& x) -> FieldVector<double, 1> {
      return scale * x[0] * x[0] * (x[0] - 1) * (x[0] - 1) * x[1] * x[1] * (x[1] - 1)
        * (x[1] - 1);
    },
      [=](auto const& x) -> FieldVector<double, 2>
    {
      return scale
        * FieldVector<double, 2>{(4. * x[0] * x[0] * x[0] - 6. * x[0] * x[0] + 2. * x[0])
        * x[1] * x[1] * (x[1] - 1)* (x[1] - 1),
        (4. * x[1] * x[1] * x[1] - 6. * x[1] * x[1] + 2. * x[1])
        * x[0] * x[0] * (x[0] - 1)* (x[0] - 1)};
    },
      [=](auto const& x) -> FieldMatrix<double, 2, 2>
    {
      return scale
        * FieldMatrix<double, 2, 2>{
          {(12. * x[0] * x[0] - 12. * x[0] + 2.)* x[1] * x[1] * (x[1] - 1)* (x[1] - 1),
            (4. * x[0] * x[0] * x[0] - 6. * x[0] * x[0] + 2. * x[0])
            * (4. * x[1] * x[1] * x[1] - 6. * x[1] * x[1] + 2. * x[1])},
          {(4. * x[0] * x[0] * x[0] - 6. * x[0] * x[0] + 2. * x[0])
               * (4. * x[1] * x[1] * x[1] - 6. * x[1] * x[1] + 2. * x[1]),
           (12. * x[1] * x[1] - 12. * x[1] + 2.) * x[0] * x[0] * (x[0] - 1) * (x[0] - 1)}};
    });
  }

  inline auto get_Constant(double scale)
  {

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(),
      [=](auto const& x) -> FieldVector<double, 1> { return scale * 1.; },
      [=](auto const& x) -> FieldVector<double, 2> {
      return FieldVector<double, 2>{0, 0};
    },
      [=](auto const& x) -> FieldMatrix<double, 2, 2> {
      return FieldMatrix<double, 2, 2>{{0., 0.}, {0., 0.}};
    });
  }

  inline auto getInitialGuess_AllDofs(double scale)
  {

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(),
      [=](auto const& x) -> FieldVector<double, 1> { return scale; },
      [=](auto const& x) -> FieldVector<double, 2> {
      return FieldVector<double, 2>{scale, scale};
    },
      [=](auto const& x) -> FieldMatrix<double, 2, 2> {
      return FieldMatrix<double, 2, 2>{{scale, scale}, {scale, scale}};
    });
  }

  inline auto get_Sum_Sin(double scale, bool xDirection, bool yDirection, Dune::FieldVector<double, 2> shift = {0.,0.}, Dune::FieldVector<double, 2> stretch = {1.,1.})
  {
    assert(xDirection || yDirection);

    Dune::FieldVector<double, 2> upperRight = stretch;
    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(),
      [=](auto const& x) -> FieldVector<double, 1>
    {
        // std::cout << "stretch" << upperRight[0] << upperRight[1] << " ";
      return scale
        * ((int) (xDirection) *sin((x[0] - shift[0]) * 2. * M_PI / upperRight[0])
          + (int) (yDirection) *sin((x[1] - shift[1]) * 2. * M_PI / upperRight[1]));
    },
      [=](auto const& x) -> FieldVector<double, 2>
    {
      return scale
        * FieldVector<double, 2>{(int) (xDirection) * 1. / upperRight[0] * cos((x[0] - shift[0]) * 2. * M_PI / upperRight[0]) * 2. * M_PI,
        (int) (yDirection) *cos(x[1] * 2. * M_PI / (upperRight[1] - shift[0])) * 2. * M_PI / upperRight[1]};
    },
      [=](auto const& x) -> FieldMatrix<double, 2, 2>
    {
      return scale
        * FieldMatrix<double, 2, 2>{
          {(int) (xDirection) * -sin((x[0] - shift[0]) * 2. * M_PI / upperRight[0]) * 4. * M_PI * M_PI / upperRight[0] / upperRight[0], 0},
          {0, (int) (yDirection) * -sin((x[1] - shift[0]) * 2. * M_PI / upperRight[1]) * 4. * M_PI * M_PI / upperRight[1] / upperRight[1]}};
    });
  }

  inline auto get_Manufactured_Force(double D)
  {
    return [=](auto const& x)->double
    {
      auto w = std::pow(std::sin(2 * M_PI * x[0]), 2);
      auto phi = std::pow(std::sin(2 * M_PI * x[1]), 2);
      return -64 * std::pow(M_PI, 4) * (1. - 2. * w) * (1. + 2. * D - 2. * phi);
    };
  }

  // NEGATIVE of rhs of strong problem
  inline auto get_Manufactured_Defect(double E)
  {
    return [=](auto const& x)->double
    {
      // auto w = std::pow(sin(2 * M_PI * x[0]), 2);
      auto phi = std::pow(sin(2 * M_PI * x[1]), 2);
      return 128 * std::pow(M_PI, 4) * (1. - 2. * phi) / E;
    };
  }

  inline auto get_Manufactured_Solution(bool perturbed = false, double perturbationScale = 0.001)
  {

    auto u = [=](auto const& x)->Dune::FieldVector<double, 2>
    {
      auto w = std::pow(std::sin(2 * M_PI * x[0]), 2);
      auto phi = std::pow(std::sin(2 * M_PI * x[1]), 2);
      if (perturbed)
      {
        return {phi - 0.5 + perturbationScale * std::sin(20 * M_PI * x[0]), w - 0.5 + perturbationScale * std::sin(20 * M_PI * x[0])};
      }
      else
        return {phi - 0.5, w - 0.5};
    };

    auto grad_u = [](auto const& x)->Dune::FieldMatrix<double, 2, 2>
    {

      return {
        {0, 4. * M_PI * std::sin(2 * M_PI * x[1]) * std::cos(2 * M_PI * x[1])},
        {4. * M_PI * std::sin(2 * M_PI * x[0]) * std::cos(2 * M_PI * x[0]), 0}
      };
    };

    auto hess_u = [](auto const& x)->Dune::FieldVector<Dune::FieldMatrix<double, 2, 2>, 2>
    {
      auto pi2 = std::pow(M_PI, 2);
      return {
        {
          {0,0}, {0, 8. * pi2 * (1. - 2. * std::pow(std::sin(2 * M_PI * x[1]), 2))}
        },
        {
          {8. * pi2 * (1. - 2. * std::pow(std::sin(2 * M_PI * x[0]), 2)), 0.},{0.,0.}
        }
      };
    };

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 2>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(), u, grad_u, hess_u);
  }

  inline auto get_Sin2(double scale, bool xDirection, bool yDirection)
  {
    assert(xDirection || yDirection);

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(),
      [=](auto const& x) -> FieldVector<double, 1>
    {
      return scale
        * ((int) (xDirection) *std::pow(sin(x[0] * 2. * M_PI), 2)
          + (int) (yDirection) *std::pow(sin(x[1] * 2. * M_PI), 2));
    },
      [=](auto const& x) -> FieldVector<double, 2>
    {
      return scale
        * FieldVector<double, 2>{(int) (xDirection) *cos(x[0] * 2. * M_PI) * 2. * M_PI * sin(x[0] * 2. * M_PI),
        (int) (yDirection) *cos(x[1] * 2. * M_PI) * 2. * M_PI * sin(x[1] * 2. * M_PI)};
    },
      [=](auto const& x) -> FieldMatrix<double, 2, 2>
    {
      return scale
        * FieldMatrix<double, 2, 2>{
          {(int) (xDirection)* (1. - 2. * std::pow(sin(x[0] * 2. * M_PI), 2)) * 4. * M_PI * M_PI, 0},
          {0, (int) (yDirection) * (1. - 2. * std::pow(sin(x[1] * 2. * M_PI),2)) * 4. * M_PI * M_PI}};
    });
  }

  inline auto get_Sin2Sin2(double scale)
  {
    Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<double(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(),
      [=](const auto& x) -> double
    {

      auto a = std::sin(2. * M_PI * x[0] / upperRight[0]);
      auto b = std::sin(M_PI * x[1] / upperRight[1]);
      return scale * a * a * b * b;
    },
      [=](auto const& x) -> FieldVector<double, 2>
    {
      auto a = std::sin(2. * M_PI * x[0] / upperRight[0]);
      auto b = std::sin(M_PI * x[1] / upperRight[1]);
      return
      {scale * 4. * M_PI / upperRight[0] * a * std::cos(2. * M_PI * x[0] / upperRight[0]) * b * b,
       scale * 2. * M_PI / upperRight[1] * a * a * b * std::cos(M_PI * x[1] / upperRight[1])};
    },

      [=](auto const& x) -> FieldMatrix<double, 2, 2>
    {
      auto a = std::sin(2. * M_PI * x[0] / upperRight[0]);
      auto b = std::sin(M_PI * x[1] / upperRight[1]);
      auto c = std::cos(2. * M_PI * x[0] / upperRight[0]);
      auto d = std::cos(M_PI * x[1] / upperRight[1]);
      return {
                {8. * M_PI * M_PI / upperRight[0] / upperRight[0] * (c * c - a * a) * b * b,
                  8. * M_PI * M_PI / upperRight[0] / upperRight[1] * a * b * c * d},
                {8. * M_PI * M_PI / upperRight[0] / upperRight[1] * a * b * c * d,
                 2. * M_PI * M_PI / upperRight[1] / upperRight[1] * a * a * (d * d - b * b)}};
    });
  }

  inline auto get_Sin2Sin2_shift(double scale)
  {
    Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<double(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(),
      [=](const auto& x) -> double
    {

      auto a = std::sin(2. * M_PI * x[0] / upperRight[0]);
      auto b = std::sin(M_PI * (x[1] / upperRight[1] - 0.5));
      return scale * a * a * b * b;
    },
      [=](auto const& x) -> FieldVector<double, 2>
    {
      auto a = std::sin(2. * M_PI * x[0] / upperRight[0]);
      auto b = std::sin(M_PI * (x[1] / upperRight[1] - 0.5));
      return
      {scale * 4. * M_PI / upperRight[0] * a * std::cos(2. * M_PI * x[0] / upperRight[0]) * b * b,
       scale * 2. * M_PI / upperRight[1] * a * a * b * std::cos(M_PI * (x[1] / upperRight[1] - 0.5))};
    },

      [=](auto const& x) -> FieldMatrix<double, 2, 2>
    {
      auto a = std::sin(2. * M_PI * x[0] / upperRight[0]);
      auto b = std::sin(M_PI * (x[1] / upperRight[1] - 0.5));
      auto c = std::cos(2. * M_PI * x[0] / upperRight[0]);
      auto d = std::cos(M_PI * (x[1] / upperRight[1] - 0.5));
      return {
                {8. * M_PI * M_PI / upperRight[0] / upperRight[0] * (c * c - a * a) * b * b,
                  8. * M_PI * M_PI / upperRight[0] / upperRight[1] * a * b * c * d},
                {8. * M_PI * M_PI / upperRight[0] / upperRight[1] * a * b * c * d,
                 2. * M_PI * M_PI / upperRight[1] / upperRight[1] * a * a * (d * d - b * b)}};
    });
  }

  inline auto get_SinSquared(double scale)
  {

    auto u = [=](auto const& x)->Dune::FieldVector<double, 2>
    {
      auto phi = scale * std::sin(2 * M_PI * x[1]);
      auto w = scale * std::pow(std::sin(2 * M_PI * x[0]), 2) * std::pow(std::sin(M_PI * x[1]), 2);
      return {phi, w};
    };

    auto grad_u = [=](auto const& x)->Dune::FieldMatrix<double, 2, 2>
    {

      return {
        {0, scale * 2. * M_PI * std::cos(2 * M_PI * x[1])},
        {scale * 4. * M_PI * std::sin(2 * M_PI * x[0]) * std::pow(std::sin(M_PI * x[1]), 2) * std::cos(2 * M_PI * x[0]),
        scale * 2. * M_PI * std::pow(std::sin(2. * M_PI * x[0]),2) * std::sin(M_PI * x[1]) * std::cos(M_PI * x[1])}
      };
    };

    auto hess_u = [=](auto const& x)->Dune::FieldVector<Dune::FieldMatrix<double, 2, 2>, 2>
    {
      auto pi2 = std::pow(M_PI, 2);
      return {
        {
          {0,0}, {0, -4. * scale * pi2 * std::sin(2 * M_PI * x[1])}
        },
        {
          {-8. * scale * pi2 * std::pow(std::sin(2 * M_PI * x[0]), 2) * std::pow(std::sin(M_PI * x[1]), 2)
          + 8. * scale * pi2 * std::pow(std::sin(M_PI * x[1]) * std::cos(2. * M_PI * x[0]), 2),
           8. * scale * pi2 * std::sin(2 * M_PI * x[0]) * std::cos(2 * M_PI * x[0]) * std::sin(M_PI * x[1]) * std::cos(M_PI * x[1])},
           {scale * 8. * pi2 * std::sin(2 * M_PI * x[0]) * std::cos(2 * M_PI * x[0]) * std::sin(M_PI * x[1]) * std::cos(M_PI * x[1]),
           -2. * scale * pi2 * std::pow(std::sin(2 * M_PI * x[0]), 2) * std::pow(std::sin(M_PI * x[1]), 2)
          + 2. * scale * pi2 * std::pow(std::sin(2. * M_PI * x[0]) * std::cos(M_PI * x[1]), 2)}
        }
      };
    };

    return Dune::Functions::makeDifferentiableFunctionFromCallables(
      Dune::Functions::SignatureTag<Dune::FieldVector<double, 2>(Dune::FieldVector<double, 2>),
      JacobianDerivativeTraits>(), u, grad_u, hess_u);
  }

  // TODO implement version for solution components
  template <class Problem, class TP>
  void addHomogeneousClampedBC(Problem& p, TP const& tp, std::vector<int> const& boundaries = {1})
  {
    auto zeroGridFct = makeGridFunction(evalAtQP(getZeroFunction()), p.gridView());
    for (auto const& boundary : boundaries)
    {
      auto clampedBC = ClampedBC{p.globalBasis(),
                               tp,
                               tp,
                               {*(p.boundaryManager()), boundary},
                               zeroGridFct};
      p.addConstraint(clampedBC);
    }

  }

  template <class Problem, class TP>
  void addPointConstraint(Problem& p, TP const& tp)
  {
    p.boundaryManager()->setBoxBoundary({1, 1, 1, 1});
    auto zeroGridFct = makeGridFunction(evalAtQP(getZeroVectorFunction()), p.gridView());
    auto predicate = [](auto const& x)
    {
      return Dune::FloatCmp::eq(x, Dune::FieldVector<double, 2>{0., 0.5}); //|| Dune::FloatCmp::eq(x,Dune::FieldVector<double,2>{0.5,0.})
    };
    PointwiseConstraint constraint(p.globalBasis(),
      tp,
      tp,
      zeroGridFct,
      predicate);
    // PointwiseConstraint constraint2(p.globalBasis(),
    //   makeTreePath(1),
    //   makeTreePath(1),
    //   zeroGridFct,
    //   predicate);
    p.addConstraint(constraint);
    // p.addConstraint(constraint2);
  }

  template <class Problem>
  void addPeriodicBC(Problem& p)
  {
    // boundary order: left,right,bottom,top
    p.boundaryManager()->setBoxBoundary({-1,-1,-2,-2});
    p.addPeriodicBC(-1, {{1.0,0.0}, {0.0,1.0}}, {1.0, 0.0});
    p.addPeriodicBC(-2, {{1.0,0.0}, {0.0,1.0}}, {0.0, 1.0});
  }

  template <class DiscreteFunction, class TP, class GF>
  void applyClampedBCToFunction(DiscreteFunction& f, TP const& tp, GF const& gridFunction)
  {
    auto const& basis = Dune::Functions::subspaceBasis(f.basis(), tp);
    std::vector<bool> dirichletNodes(basis.dimension(), false);
    auto& coeffs = f.coefficients();
    auto localView = basis.localView();
    auto seDOFs = clampedDOFs(localView);
    auto const& gridView = basis.gridView();
    for (auto const& element : entitySet(basis))
    {
      if (element.hasBoundaryIntersections())
      {
        localView.bind(element);
        for (auto const& intersection : Dune::intersections(gridView, element))
          if (intersection.boundary())
            for (auto localIndex : seDOFs.bind(localView, intersection))
              dirichletNodes[localView.index(localIndex)] = true;
      }
    }

    auto interpolator = InterpolatorFactory<tag::assign>::create(basis);
    interpolator(coeffs, gridFunction, dirichletNodes);
  }

  template< class DF, class TP>
  void applyClampedBCToFunction(DF& f, TP const& tp)
  {
    // assumes dim of range of DF is 1
    applyClampedBCToFunction(f, tp, makeGridFunction(getZeroFunction(), f.basis().gridView()));
  }

  template <class GV, class K, int dim>
  void isVertex(GV const& gv, Dune::FieldVector<K, dim> coord)
  {
    for (auto vertex : vertices(gv))
    {
      if (Dune::FloatCmp::eq(vertex.geometry().center(), coord))
      {
        // std::cout << "Dirac delta is at Vertex with index" << gv.indexSet().index(vertex) << "\n";
        return;
      }
    }
    error_exit("Coordinates {} are not on a vertex!", coord);
  }

  template <int derivative, class GlobalBasis, class RHS, class Treepath, class T, class K, int dim>
  void addDiracRhs(GlobalBasis const& gb, RHS& rhs, Dune::FieldVector<K, dim> location, T strength,
    Treepath tp)
  {
    // assert(decltype(gb.localView().tree().child(tp))::isLeaf);
    isVertex(gb.gridView(), location);

    // hack! create a function that, when interpolated into V_h gives a DofVector u such that u_i =
    // \delta_{x_0}(\phi_i)
    if constexpr (derivative == 0)
    {
      auto dirac = [x_0 = location, s = strength](Dune::FieldVector<K, dim> const& x)
      { return s * ((double) Dune::FloatCmp::eq(x, x_0)); };
      auto d_dirac = [](Dune::FieldVector<K, dim> const& x)
      {
        Dune::FieldVector<K, dim> ret;
        ret = 0;
        return ret;
      };

      auto dd_dirac = [](Dune::FieldVector<K, dim> const& x)
      {
        Dune::FieldMatrix<K, dim, dim> ret;
        ret = 0;
        return ret;
      };

      auto duneFunction = Dune::Functions::makeDifferentiableFunctionFromCallables(
        Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
        JacobianDerivativeTraits>(),
        dirac, d_dirac, dd_dirac);
      auto dofVec = makeDOFVector(gb);
      valueOf(dofVec, tp) << duneFunction;

      // add coefficientvector onto rhs
      Recursive::transform(rhs.impl(), Operation::Plus{}, rhs.impl(), dofVec.impl());
    }
    else if constexpr (derivative == 1)
    {
      auto dirac = [](Dune::FieldVector<K, dim> const& x) -> Dune::FieldVector<K, 1> { return 0.; };
      auto d_dirac = [x_0 = location, s = strength](Dune::FieldVector<K, dim> const& x)
      {
        Dune::FieldVector<K, dim> ret;
        ret = s * ((double) Dune::FloatCmp::eq(x, x_0));
        return ret;
      };

      auto dd_dirac = [](Dune::FieldVector<K, dim> const& x)
      {
        Dune::FieldMatrix<K, dim, dim> ret;
        ret = 0;
        return ret;
      };

      auto duneFunction = Dune::Functions::makeDifferentiableFunctionFromCallables(
        Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
        JacobianDerivativeTraits>(),
        dirac, d_dirac, dd_dirac);
      auto dofVec = makeDOFVector(gb);
      valueOf(dofVec, tp) << duneFunction;
      // add coefficientvector onto rhs
      Recursive::transform(rhs.impl(), Operation::Plus{}, rhs.impl(), dofVec.impl());
    }
    else
    {
      error_exit("Higher Derivatives of dirac Delta are not implemented!");
    }
  }

  class CSVWriter
  {

  public:
    CSVWriter() = delete;
    CSVWriter(std::string file, std::vector<std::string> colNames) : CSVWriter(file, colNames, ";")
    {}

    CSVWriter(std::string file, std::vector<std::string> colNames, std::string separator)
      : file_(file), colNames_(colNames), separator_(separator)
    {
      if (file == "")
      {
        throw std::invalid_argument("Calling constructor with empty file");
      }
      cols_ = colNames_.size();
      if (cols_ == 0)
      {
        std::cout << "CSVWriter initialized without columns" << std::endl;
      }
      writeHeader();
    }

    // No copy constructor to avoid race conditions for filewriting
    CSVWriter(CSVWriter const&) = delete;
    CSVWriter(CSVWriter&&) = default;

    CSVWriter& operator=(CSVWriter const&) = delete;
    CSVWriter& operator=(CSVWriter&& that)
    {
      output_.swap(that.output_);
      file_ = that.file_;
      colNames_ = that.colNames_;
      cols_ = that.cols_;
      return *this;
    }

    ~CSVWriter()
    {
      if (output_.is_open())
        output_.close();
    }

    void writeHeader()
    {
      if (output_.is_open())
        output_.close();
      if (!output_.is_open())
        output_.open(file_, std::ofstream::trunc | std::ofstream::ate);

      if (!output_.is_open())
        throw std::runtime_error("Couldn't open file " + file_);

      if (colNames_.size() > 0)
      {
        writeln<std::vector<std::string>>(colNames_, output_);
      }
    }

    void close() { output_.close(); }

    void writeln(std::vector<double> const& values) { writeln(values, output_); }

    template <class Values>
    void writeln(Values const& values, decltype((values[0], true)) = true)
    {
      writeln(values, output_);
    }

    void tee(std::vector<double> const& values)
    {
      writeln(values);
      writeln(values, std::cout);
    }

    void setSeparator(std::string newSep) { separator_ = newSep; }
    std::string getSeparator() { return separator_; }

  private:
    template <class Values>
    void writeln(Values const& values, std::ostream& output, decltype((values[0], true)) = true)
    {
      if (!output_.good())
        throw std::runtime_error("csv file is not good! <" + file_
          + "> Have you initialized with writeHeader()?");
      output << values[0];
      std::size_t max = (cols_ == 0) ? values.size() : cols_;
      max = std::min(max, values.size());
      for (std::size_t i = 1; i < max; ++i)
        output << separator_ << values[i];
      output << std::endl;
    }

    std::string file_;
    std::vector<std::string> colNames_;
    std::string separator_;
    std::size_t cols_;
    std::ofstream output_;
  };

  inline std::string removeDots(std::string input)
  {
    std::replace(input.begin(), input.end(), '.', ',');
    return input;
  }

  template <class GridView>
  class VTKWriter
  {
    using DataCollector = Dune::Vtk::LagrangeDataCollector<GridView>;
    using TimestepWriter = Dune::VtkUnstructuredGridWriter<GridView, DataCollector>;
    DataCollector dataCollector_;
    Dune::PvdWriter<TimestepWriter> vtkWriter_;
    std::string file_;
    std::string dataFileDirectory_;
    std::string outputDirectory_ = "output";

  public:
    VTKWriter() = delete;

    VTKWriter(std::string file, std::string dataFileDirectory, std::string outputDirectory,
      GridView const& gridView, std::size_t order)
      : dataCollector_(gridView, order), vtkWriter_(dataCollector_, Dune::Vtk::FormatTypes::ASCII, Dune::Vtk::DataTypes::FLOAT32),
      file_(removeDots(file)), dataFileDirectory_(dataFileDirectory), outputDirectory_(outputDirectory)
    {}

    VTKWriter(std::string file, GridView const& gridView, std::size_t refinementIntervals)
      : VTKWriter(file, file, "output", gridView, refinementIntervals)
    {}

    VTKWriter(std::string file, std::string dataFileDirectory, GridView const& gridView,
      std::size_t refinementIntervals)
      : VTKWriter(file, dataFileDirectory, "output", gridView, refinementIntervals)
    {}

    template <class F>
    void addVertexData(F&& f, Dune::VTK::FieldInfo info
      = Dune::VTK::FieldInfo("f", Dune::VTK::FieldInfo::Type::scalar, 1))
    {
      vtkWriter_.addPointData(std::forward<F>(f), info);
    }

    void write(double t) { vtkWriter_.writeTimestep(t, outputDirectory_ + "/" + file_, dataFileDirectory_); }

    // template <class F>
    // void writeFunction(F &&f,
    //                    VTK::FieldInfo info = VTK::FieldInfo("f", VTK::FieldInfo::Type::scalar,
    //                    1))
    // {
    //   vtkWriter.addVertexData(std::forward<F>(f), info);
    //   vtkWriter.write(file_);
    // }
  };

  class Experiment
  {
    std::string title_;
    std::string directory_;
    std::string description_;
    std::vector<std::string> csvFiles;



  public:
    Experiment() = delete;
    Experiment(std::string title, std::string directory, std::string description)
      : title_(title), directory_(directory), description_(description)
    {}
    Experiment(std::string directory) : Experiment("", directory, "") {}

    std::string getDirectory() { return directory_; }

    CSVWriter getCSVWriter(std::string file, std::vector<std::string> colNames,
      std::string title = "", std::string ylabel = "",
      std::string description = "", bool convergencePlot = false,
      std::vector<int> convOrders = {})
    {
      auto path = fs::path(directory_); // / "csv";
      csvFiles.push_back(path / (file + ".csv"));
      if (!fs::exists(path) || !fs::is_directory(path))
        fs::create_directories(path);
      // write a json file with additional information
      if (title != "" || ylabel != "" || description != "" || convergencePlot)
      {
        std::ofstream jsonFile;
        jsonFile.open(path / (file + ".json"), std::ofstream::out | std::ofstream::trunc);
        jsonFile << "{\"title\":" + jsonDump(title) + ",\"ylabel\":" + jsonDump(ylabel)
          + ",\"description\":" + jsonDump(description);
        if (convergencePlot)
          jsonFile << ",\"logy\":\"true\"";
        if (convOrders.size() != 0)
        {
          jsonFile << ",\"convergenceOrder\":\"";
          for (auto const& order : convOrders)
            jsonFile << order;
          jsonFile << "\"";
        }

        jsonFile << "}" << std::endl;
        jsonFile.close();
      }
      return CSVWriter(path / (file + ".csv"), colNames, ";");
    }

    void showPlotsFromCsvFiles(bool save = false)
    {
      if (save)
        for_each(csvFiles.begin(), csvFiles.end(),
          [&](std::string const& file) { system(("./plotCsv.py -m -s " + file).c_str()); });
      else
        for_each(csvFiles.begin(), csvFiles.end(),
          [&](std::string const& file) { system(("./plotCsv.py -m " + file).c_str()); });
    }

    template <class GridView>
    auto getVTKWriter(std::string file, GridView const& gridView,
      std::size_t refinementIntervals = 5)
    {
      return getVTKWriterImpl(file, gridView, refinementIntervals, Dune::PriorityTag<5>{});
    }

    template <class GridView>
    decltype(auto) getVTKWriterImpl(std::string file, GridView const& gridView,
      std::size_t refinementIntervals,
      Dune::PriorityTag<1> /*tag*/,
      decltype(gridView.grid().hostGrid(), 1) = 1)
    {
      return getVTKWriterImpl(file, gridView.grid().hostGrid()->leafGridView(), refinementIntervals, Dune::PriorityTag<5>{});
    }

    template <class GridView>
    decltype(auto) getVTKWriterImpl(std::string file, GridView const& gridView,
      std::size_t refinementIntervals,
      Dune::PriorityTag<2> /*tag*/,
      decltype(gridView.impl().hostGridView(), 1) = 1)
    {
      return getVTKWriterImpl(file, gridView.impl().hostGridView(), refinementIntervals, Dune::PriorityTag<5>{});
    }

    template <class GridView>
    VTKWriter<GridView> getVTKWriterImpl(std::string file, GridView const& gridView,
      std::size_t refinementIntervals, Dune::PriorityTag<0> /*tag*/)
    {
      auto path = fs::path(directory_) / "vtk";
      if (!fs::exists(path) || !fs::is_directory(path))
        fs::create_directories(path);
      if (!fs::exists(path / file) || !fs::is_directory(path / file))
        fs::create_directories(path / file);

      return VTKWriter<GridView>(file, file, path.string(), gridView, refinementIntervals);
    }

    template<class... Args>
    void trace(std::string const& s, Args&&... args)
    {
      AMDiS::info(4, s, FWD(args)...);
    }

    template<class... Args>
    void debug(std::string const& s, Args&&... args)
    {
      AMDiS::info(3, s, FWD(args)...);
    }

    template<class... Args>
    void info(std::string const& s, Args&&... args)
    {
      AMDiS::info(2, s, FWD(args)...);
    }

    template<class... Args>
    void warn(std::string const& s, Args&&... args)
    {
      AMDiS::info(1, s, FWD(args)...);
    }

    template<class... Args>
    void error(std::string const& s, Args&&... args)
    {
      AMDiS::info(0, s, FWD(args)...);
    }

  private:
    std::string jsonDump(std::string s) { return "\"" + s + "\""; }
  };
  } // namespace VonKarman
} // namespace AMDiS