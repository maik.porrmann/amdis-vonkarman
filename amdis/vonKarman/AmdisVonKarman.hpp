#pragma once

#include <amdis/AMDiS.hpp>

#include <amdis/localoperators/FourthOrderVonKarmanTestTrial.hpp>

#include <dune/functions/common/differentiablefunctionfromcallables.hh>
#include <dune/functions/functionspacebases/sizeinfo.hh>


#include <amdis/ClampedBC.hpp>
#include <amdis/PointwiseConstraint.hpp>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <amdis/AdaptStationary.hpp>
#include <amdis/io/VTKSequenceWriter.hpp>

#include <amdis/localoperators/VariableOrderGradientFlow.hpp>
#include <amdis/vonKarman/utility.hpp>
#include <amdis/vonKarman/Defect.hpp>
#include <amdis/vonKarman/MatMult.hpp>
using namespace AMDiS;

namespace AMDiS::VonKarman{

  enum GradientFlow
  {
    none = 0,
    localNewton = 1,
    globalNewton = 2,
    explicitEuler = 3,
    implicitEuler = 4,
    stringMethod = 5,
    gentlestAscend = 6
  };

  bool operator!(GradientFlow g) { return g == static_cast<GradientFlow>(0); }

  using Dune::FloatCmp::lt;
  using Dune::Indices::_0;
  using Dune::Indices::_1;
  using Dune::Indices::_2;


  template <class P>
  void run(P& p)
  {
    Dune::Timer timer;
    p.initialize();
    if (Parameters::get<bool>(p.name_ + "->search scale").value_or(false))
    {
      double start = Parameters::get<double>(p.name_ + "->search->start").value_or(0.0);
      double stop = Parameters::get<double>(p.name_ + "->search->stop").value_or(20.0);
      int steps = Parameters::get<int>(p.name_ + "->search->steps").value_or(20);
      info(0, "Running Scale search from {} to {} in {} steps", start, stop, steps);
      p.runParameterSearch_Scaling(start, stop, steps);
    }
    else if (Parameters::get<bool>(p.name_ + "->manufactured Solution").value_or(false) || Parameters::get<bool>(p.name_ + "->compareToContinuum").value_or(false))
    {
      info(0, "Solving for manufactured solution ");
      p.solveForRefinementLevels();
    }
    else if (Parameters::get<bool>(p.name_ + "->parameterEvolution with load control").value_or(false))
    {
      info(0, "Evolving youngs modulus and defect force");
      p.runParameterEvolutionWithLoadControl();
    }
    else if (Parameters::get<bool>(p.name_ + "->search E").value_or(false))
    {
      double start = Parameters::get<double>(p.name_ + "->search->start").value_or(0.0);
      double stop = Parameters::get<double>(p.name_ + "->search->stop").value_or(20.0);
      int steps = Parameters::get<int>(p.name_ + "->search->steps").value_or(20);
      info(0, "Running Youngs Modulus search from {} to {} in {} steps", start, stop, steps);

      p.runParameterSearch_E(start, stop, steps);
    }
    else if (Parameters::get<bool>(p.name_ + "->search D").value_or(false))
    {
      double start = Parameters::get<double>(p.name_ + "->search->start").value_or(0.0);
      double stop = Parameters::get<double>(p.name_ + "->search->stop").value_or(20.0);
      int steps = Parameters::get<int>(p.name_ + "->search->steps").value_or(20);
      info(0, "Running Bending Modulus search from {} to {} in {} steps", start, stop, steps);

      p.runParameterSearch_D(start, stop, steps);
    }

    bool taskFound = false;

    if (Parameters::get<bool>(p.name_ + "->artificialForce").value_or(false))
    {
      p.artificialForce();
      taskFound = true;
    }
    if (Parameters::get<bool>(p.name_ + "->loadControl").value_or(false))
    {
      p.loadControl();
      taskFound = true;
    }
    if (Parameters::get<bool>(p.name_ + "->parameterEvolution").value_or(false))
    {
      p.runParameterEvolution();
      taskFound = true;
    }
    if (Parameters::get<bool>(p.name_ + "->rotate defect").value_or(false))
    {
      p.rotateDefect();
      taskFound = true;
    }
    if (Parameters::get<bool>(p.name_ + "->move defect").value_or(false))
    {
      p.moveDefect();
      taskFound = true;
    }
    if (Parameters::get<bool>(p.name_ + "->vary thickness").value_or(false))
    {
      p.variateThickness();
      taskFound = true;
    }
    if (Parameters::get<bool>(p.name_ + "->search bifurcation").value_or(false))
    {
      p.searchForBifurcation();
      taskFound = true;
    }
    if (Parameters::get<bool>(p.name_ + "->evolve D").value_or(false))
    {
      p.runParameterEvolution_D();
      taskFound = true;
    }
    if (!taskFound)
    {
      warning("no task specified");
      p.run();
    }



    std::cout << "Experiment took " << timer.elapsed() << " seconds" << std::endl;
  }


  template <class Traits, class TP>
  class VonKarmanProblem : public Experiment
  {
    using Problem = ProblemStat<Traits>;
    using SolutionVector = typename Problem::SolutionVector;
    using GridView = typename Problem::GridView;
    using GlobalBasis = typename Problem::GlobalBasis;
    static constexpr int NormOrder = 2;

    // if we have a Lagrange condition, then TP is of type makeTreePath(_i, int)

  public:
    static constexpr bool Lagrange = not std::is_same_v<TYPEOF(makeTreePath(0)), AMDiS::remove_cvref_t<TP>>;
    VonKarmanProblem() = delete;

    //Base constructor
    VonKarmanProblem(Environment& e, Problem p, TP tpPhi, TP tpW, std::string name = "problem",
      std::string directory = "output")
      : Experiment(directory), env_(e), name_(name), gv_(p.gridView()), probNewton_(p),
      probGradientFlow_("GradientFlow", probNewton_.grid()),
      phi_(tpPhi), w_(tpW), u_(pop_back(phi_)),
      solutionNewton_(makeDOFVector(probNewton_.globalBasis())),
      solutionFlow_(makeDOFVector(probNewton_.globalBasis()))
    {
      // Parameters
      readParameters();
      refineAroundDefects();
      probNewton_.initialize(INIT_ALL & ~INIT_FILEWRITER);
      probGradientFlow_.initialize(INIT_SYSTEM, &probNewton_, INIT_ALL & ~INIT_FILEWRITER & ~INIT_SYSTEM);
      area_ = integrate(1., gv_, 1);

    }

    // Constructor with an index to prefix out-of-plane and stress solutions
    template <class Index>
    VonKarmanProblem(Environment& e, Problem p, Index ii, std::string name = "problem",
      std::string directory = "output")
      :VonKarmanProblem(e, p, makeTreePath(ii, 0), makeTreePath(ii, 1), name, directory)
    {

    }

    // Constructor without an index to prefix out-of-plane and stress solutions
    VonKarmanProblem(Environment& e, Problem p, std::string name = "problem",
      std::string directory = "output")
      :VonKarmanProblem(e, p, makeTreePath(0), makeTreePath(1), name, directory)
    {}

    void initialize()
    {
      if (isInit_)
        return;
      else
      {

        bool restoreB = Parameters::get<bool>(name_ + "->restore").value_or(false);
        if (restoreB)
          restore();

        // add operators
        addOperators(Parameters::get<int>(name_ + "->quad").value_or(5));
        std::array<int, 4>boundaryIds = Parameters::get<std::array<int, 4>>(name_ + "->boundary manager->ids").value_or(std::array<int, 4>{1, 1, 1, 1});
        probNewton_.boundaryManager()->setBoxBoundary(boundaryIds);
        probGradientFlow_.boundaryManager()->setBoxBoundary(boundaryIds);


        addBoundaryConditions();

        // assemble systemMatrix for explicit gradient descent only once
        probGradientFlow_.systemMatrix()->assemble();
        isInit_ = true;

        if (!restoreB)
        {
          if (method_ == globalNewton || method_ == localNewton)
            setInitialSolution(solutionNewton_);
          else
            setInitialSolution(solutionFlow_);
        }
      }
      this->backup("initialNewton.solution", "initialFlow.solution");
    }

    void checkInitialized()
    {
      if (!isInit_)
        error_exit("Problem not initialized!");
    }

    void readParameters();

    void createDefects();

    void refineAroundDefects();

    void addOperators(int quadOrder = 5)
    {
      if (operatorsSet_)
        error_exit("Adding Operators twice");
      else
      {
        switch (method_)
        {
        case localNewton:
          addVonKarmanNewtonOperator(solutionNewton_, quadOrder);
          addExplicitGradientFlowOperator(solutionFlow_, quadOrder);
          break;
        case globalNewton:
          addVonKarmanNewtonOperator(solutionNewton_, quadOrder);
          addExplicitGradientFlowOperator(solutionNewton_, quadOrder);
          break;
        case explicitEuler:
          addVonKarmanNewtonOperator(solutionNewton_, quadOrder);
          addExplicitGradientFlowOperator(solutionFlow_, quadOrder);
          break;
        case implicitEuler:
          addVonKarmanNewtonOperator(solutionNewton_, quadOrder);
          addImplicitGradientFlowOperator(quadOrder);
          break;
        case stringMethod:
          addVonKarmanNewtonOperator(solutionNewton_, quadOrder);
          addExplicitGradientFlowOperator(solutionFlow_, quadOrder);
          if (useTangentials_)
          {
            probGradientFlow_.addVectorOperator(makeOperator(tag::gradientFlow<NormOrder>{1., 1.},
              valueOf(solutionNewton_, phi_)), phi_);
            probGradientFlow_.addVectorOperator(makeOperator(tag::gradientFlow<NormOrder>{1., 1.},
              valueOf(solutionNewton_, w_)), w_);
          }
          break;
        case gentlestAscend:
          addVonKarmanNewtonOperator(solutionNewton_, quadOrder);
          addExplicitGradientFlowOperator(solutionFlow_, quadOrder);
          break;
        default:
          error_exit("Invalid method");
          break;
        }

        addForceAndDefectOperators(quadOrder + 3);
        operatorsSet_ = true;
      }
    }

    template <class DofVector>
    void addVonKarmanNewtonOperator(DofVector const& previous, int quadOrder = 5)
    {
      // left hand side
      tag::VonKarmanIteration<updateFormulation_> tag{D_, E_, Nu_};

      auto opVonKarman = makeOperator(tag, valueOf(previous, u_), quadOrder);
      probNewton_.addMatrixOperator(opVonKarman, u_, u_);
      // right hand side, contribution for newton iteration
      probNewton_.addVectorOperator(opVonKarman, u_);
    }

    void addExplicitGradientFlowOperator(SolutionVector const& previous, int quadOrder = 5)
    {
      auto opGradient
        = makeOperator(tag::VonKarmanIteration<true>{D_, E_, Nu_}, valueOf(previous, u_), quadOrder); // Think whether flows would also be applied to lagrange multipliers
      probGradientFlow_.addVectorOperator(opGradient, u_);

      double factor = minimize_ ? 1. : -1.;
      double tau = 1.;
      probGradientFlow_.addMatrixOperator(
        makeOperator(tag::gradientFlow<NormOrder>{tau, factor}, getZeroFunction(), quadOrder),
        phi_, phi_);
      probGradientFlow_.addMatrixOperator(
        makeOperator(tag::gradientFlow<NormOrder>{tau, factor}, getZeroFunction(), quadOrder),
        w_, w_);
    }

    // add Operators for implicit GradientFlow to Newtonproblem (!)
    void addImplicitGradientFlowOperator(int quadOrder = 5)
    {
      assert(method_ == implicitEuler);
      // TODO make this nicer!
      double factor = minimize_ ? 1. : -1.;

      if constexpr (updateFormulation_)
      {
        auto opFlow_0 = makeOperator(tag::gradientFlow<NormOrder>{tau_, -factor},
          valueOf(solutionNewton_, phi_), quadOrder);
        auto opFlow_1 = makeOperator(tag::gradientFlow<NormOrder>{tau_, -factor},
          valueOf(solutionNewton_, w_), quadOrder);

        probNewton_.addVectorOperator(opFlow_0, phi_);
        probNewton_.addVectorOperator(opFlow_1, w_);
      }
      auto opFlow_0 = makeOperator(tag::gradientFlow<NormOrder>{tau_, factor},
        valueOf(solutionFlow_, phi_), quadOrder);
      auto opFlow_1 = makeOperator(tag::gradientFlow<NormOrder>{tau_, factor},
        valueOf(solutionFlow_, w_), quadOrder);

      probNewton_.addMatrixOperator(opFlow_0, phi_, phi_);
      probNewton_.addMatrixOperator(opFlow_1, w_, w_);
      probNewton_.addVectorOperator(opFlow_0, phi_);
      probNewton_.addVectorOperator(opFlow_1, w_);
    }

    // add Lagrange Operators forcing the integral over domain to be Zero
    // only if basis contains corresponding multipliers
    void addLagrangeCondition(Problem& p, SolutionVector const& x, int quadOrder = 5)
    {
      if constexpr (Lagrange)
      {
        auto lambdaPhi = makeTreePath(_1);
        auto lambdaW = makeTreePath(_2);
        // LHS
        auto ref = AMDiS::reference(area_);
        // function update testet with multiplier variation
        p.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / ref, quadOrder), lambdaPhi, phi_);
        p.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / ref, quadOrder), lambdaW, w_);
        // multiplier update testet with function variation
        p.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / ref, quadOrder), phi_, lambdaPhi);
        p.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / ref, quadOrder), w_, lambdaW);
        // RHS
        // previous function testet with multiplier variation
        p.addVectorOperator(makeOperator(tag::test{}, -valueOf(x, phi_) / ref, quadOrder), lambdaPhi);
        p.addVectorOperator(makeOperator(tag::test{}, -valueOf(x, w_) / ref, quadOrder), lambdaW);
        // previous mulitplier testet with function variation
        p.addVectorOperator(makeOperator(tag::test{}, -valueOf(x, lambdaPhi) / ref, quadOrder), phi_);
        p.addVectorOperator(makeOperator(tag::test{}, -valueOf(x, lambdaW) / ref, quadOrder), w_);
      }
    }

    void addReactionTerm(Problem& p, SolutionVector const& x, int quadOrder = 5)
    {
      double eps = Parameters::get<double>(name_ + "->reaction term").value_or(1e-10);
      auto op = makeOperator(tag::test_trial{}, eps, quadOrder);
      p.addMatrixOperator(op, phi_, phi_);
      p.addMatrixOperator(op, w_, w_);
      p.addVectorOperator(makeOperator(tag::test{}, valueOf(x, phi_)* eps, quadOrder), phi_);
      p.addVectorOperator(makeOperator(tag::test{}, valueOf(x, w_)* eps, quadOrder), w_);

    }

    /**
     * @brief
     *        Currently only homogeneous clamped and periodic BCs are implemented
     *        TODO implement free bc
     */
    void addBoundaryConditions()
    {
      std::vector<int>boundaryIds = Parameters::get<std::vector<int>>(name_ + "->boundary manager->ids").value_or(std::vector<int>{1});
      std::sort(boundaryIds.begin(), boundaryIds.end());
      boundaryIds.erase(std::unique(boundaryIds.begin(), boundaryIds.end()), boundaryIds.end());
      // TODO add different Boundary Conditions
      //  Homogeneous Clamped BC on both solutions
      if (bc_ == "clamped")
      {
        addHomogeneousClampedBC(probNewton_, w_, boundaryIds);
        addHomogeneousClampedBC(probGradientFlow_, w_, boundaryIds);

        addHomogeneousClampedBC(probNewton_, phi_, boundaryIds);
        addHomogeneousClampedBC(probGradientFlow_, phi_, boundaryIds);
      }
      else if (bc_ == "free")
      {
        addHomogeneousClampedBC(probNewton_, phi_, boundaryIds);
        addHomogeneousClampedBC(probGradientFlow_, phi_, boundaryIds);
      }
      else if (bc_ == "mixed")
      {
        addHomogeneousClampedBC(probNewton_, phi_, boundaryIds);
        addHomogeneousClampedBC(probGradientFlow_, phi_, boundaryIds);

        auto const b = Parameters::get<std::vector<int>>(name_ + "->mixed boundary condition->clamped ids").value_or(std::vector<int>{1});
        addHomogeneousClampedBC(probNewton_, w_, b);
        addHomogeneousClampedBC(probGradientFlow_, w_, b);

        // Inhomogeneous mixed bc are not implemented !
      }
      else if (bc_ == "periodic")
      {
        // addPeriodicBC(probNewton_);
        // addPeriodicBC(probGradientFlow_);

        addPointConstraint(probNewton_, u_);
        addPointConstraint(probGradientFlow_, u_);
      }
      else if (bc_ == "lagrange")
      {
        if constexpr (Lagrange)
        {
          addLagrangeCondition(probNewton_, solutionNewton_);
          addLagrangeCondition(probGradientFlow_, solutionFlow_);
        }
        else
        {
          addReactionTerm(probNewton_, solutionNewton_);
          addReactionTerm(probGradientFlow_, solutionFlow_);
        }
      }
      else if (bc_ == "none")
        warning("Solving problem without boundary conditions!");
      else if (bc_ == "invalid")
        error_exit("Must specify boundary conditions!");
      else
        error_exit("Invalid boundary condition " + bc_ + " given in initfile");
    }
    void addGrowthOperators(Problem& p, int quadOrder);

    void addForceAndDefectOperators(int quadOrder = 5);

    void addDefectOperators(Problem& p, int quadOrder = 5);

    void globalRefine(std::size_t i)
    {
      probNewton_.globalRefine(i);
      gv_ = probNewton_.gridView();
      assemble(probGradientFlow_, true, true);
      assemble(probNewton_, true, true);
    }

    auto createWriter(std::string file, SolutionVector const& v)
    {
      auto writer = getVTKWriter(file, gv_);

      auto hessianAiry = makeGridFunction(gradientOf(gradientOf(valueOf(v, phi_))), gv_);
      auto stress = makeGridFunction(invokeAtQP(
        [&](auto const& mat) -> Dune::FieldVector<double, 3>
      {
        return {{Dune::MatVec::as_matrix(mat)[1][1],
                 Dune::MatVec::as_matrix(mat)[0][0],
                 -Dune::MatVec::as_matrix(mat)[0][1]}};
      },
        hessianAiry), gv_);

      writer.addVertexData(valueOf(v, phi_),
        Dune::VTK::FieldInfo("airy", Dune::VTK::FieldInfo::Type::scalar, 1));
      writer.addVertexData(
        valueOf(v, w_),
        Dune::VTK::FieldInfo("displacement", Dune::VTK::FieldInfo::Type::scalar, 1));
      writer.addVertexData(stress,
        Dune::VTK::FieldInfo("stress", Dune::VTK::FieldInfo::Type::vector, 3));
      return writer;
    }

    // calculate Energy if specified by init file or if force = true
    double calculateEnergy(SolutionVector const& x, bool force = false)
    {
      if (logEnergy_ || force)
      {
        if (dislocation_)
        {
          return VonKarman::EnergyWithDefect(valueOf(x, phi_), valueOf(x, w_), dislocations_,
            gv_, D_, E_, Nu_);
        }
        else if (disclination_)
        {
          return VonKarman::EnergyWithDefect(
            valueOf(x, phi_), valueOf(x, w_), disclinations_, gv_, D_, E_, Nu_);
        }
        else if (useForce_)
        {
          auto forceTerm = [f = this->constantForce_](auto const& x) { return f; };
          return VonKarman::EnergyWithForce(valueOf(x, phi_), valueOf(x, w_),
            makeGridFunction(forceTerm, gv_), gv_, D_, E_, Nu_);
        }
        else
          return VonKarman::Energy(valueOf(x, phi_), valueOf(x, w_), gv_, D_, E_, Nu_);
      }
      else
        return -1;
    }

    // TODO remove
    double computeStepSize(Problem const& p, SolutionVector const& direction)
    {
      return computeStepSize(p, direction, solutionNewton_);
    }

    /**
     * @brief compute stepsize for update "direction" at position "x"
     *        Dispatch method to switch between stepsize rules
     *
     * @param direction
     * @param x
     * @return stepsize
     */
    double computeStepSize(Problem const& p, SolutionVector const& direction,
      SolutionVector const& x)
    {
      if (useArmijo_)
        return computeArmijoStepSize(p, direction, x);
      else
        return t_init_;
    }

    /**
     * @brief compute Armijo stepsize for update "direction" at position "x"
     *
     * @param direction
     * @param x
     * @return stepsize
     */
    double computeArmijoStepSize(Problem const& p, SolutionVector const& direction,
      SolutionVector const& x)
    {
      double t = t_init_;
      SolutionVector tmp = makeDOFVector(*(probNewton_.globalBasis()));
      Recursive::transform(
        tmp.impl(),
        Operation::compose(Operation::Plus{},
          Operation::compose(Operation::Scale<double>{t}, Operation::Arg<0>{}),
          Operation::Arg<1>{}),
        direction.impl(), x.impl());

      double EnergyAtX = calculateEnergy(x, true);
      double newEnergy = calculateEnergy(tmp, true);
      // - -grad(f)|x * d = -rhs * d
      double directionalEnergy
        = -Recursive::innerProduct(p.rhsVector()->impl(), direction.impl(), 0.);

      info("For t = {} NewEnergy is {} old Energy is {}  and directional Energy is {}", t, newEnergy, EnergyAtX, directionalEnergy);
      bool condition = minimize_ ? (newEnergy > EnergyAtX + gamma_ * t * directionalEnergy)
        : (newEnergy < EnergyAtX + gamma_ * t * directionalEnergy);
      for (std::size_t i = 0; condition && (i < armijoMax_); ++i)
      {
        t = beta_ * t;
        Recursive::transform(
          tmp.impl(),
          Operation::compose(Operation::Plus{},
            Operation::compose(Operation::Scale<double>{t}, Operation::Arg<0>{}),
            Operation::Arg<1>{}),
          direction.impl(), x.impl());
        newEnergy = calculateEnergy(tmp, true);
        directionalEnergy = -Recursive::innerProduct(p.rhsVector()->impl(), direction.impl(), 0.); // TODO this includes Lagrange Multipliers
        info("For t = {} NewEnergy is {} old Energy is {}  and directional Energy is {}", t, newEnergy, EnergyAtX, directionalEnergy);

        condition = minimize_ ? (newEnergy > EnergyAtX + gamma_ * t * directionalEnergy)
          : (newEnergy < EnergyAtX + gamma_ * t * directionalEnergy);
      }

      return t;
    }

    // TODO move norm calculations to utility
    template <class GridFunction>
    double norm(GridFunction const& f, int derivativeOrder = NormOrder)
    {
      return sqrt(norm2(f, derivativeOrder));
    }

    template <class GridFunction>
    double norm2(GridFunction const& f, int derivativeOrder)
    {
      if (derivativeOrder == 0)
        return integrate(sqr(f), gv_);
      else if (derivativeOrder == 1)
      {
        return integrate(sqr(f) + unary_dot(gradientOf(f)), gv_);
      }
      else if (derivativeOrder == 2)
      {
        return integrate(
          sqr(f) + unary_dot(gradientOf(f))
          + MatrixInnerProduct(gradientOf(gradientOf(f)), gradientOf(gradientOf(f))),
          gv_);
      }
      else
      {
        msg("H{}norm not implemented!", derivativeOrder);
        return -1;
      }
    }

    double norm(SolutionVector const& x, int derivativeOrder = NormOrder)
    {
      return sqrt(norm2(x, derivativeOrder));
    }

    double norm2(SolutionVector const& x, int derivativeOrder)
    {
      if (derivativeOrder == 0)
        return integrate(sqr(valueOf(x, phi_)) + sqr(valueOf(x, w_)), gv_);
      else if (derivativeOrder == 1)
      {
        return integrate(sqr(valueOf(x, phi_)) + sqr(valueOf(x, w_))
          + unary_dot(gradientOf(valueOf(x, phi_)))
          + unary_dot(gradientOf(valueOf(x, w_))),
          gv_);
      }
      else if (derivativeOrder == 2)
      {
        return integrate(sqr(valueOf(x, phi_)) + sqr(valueOf(x, w_))
          + unary_dot(gradientOf(valueOf(x, phi_)))
          + unary_dot(gradientOf(valueOf(x, w_)))
          + MatrixInnerProduct(gradientOf(gradientOf(valueOf(x, phi_))),
            gradientOf(gradientOf(valueOf(x, phi_))))
          + MatrixInnerProduct(gradientOf(gradientOf(valueOf(x, w_))),
            gradientOf(gradientOf(valueOf(x, w_)))),
          gv_);
      }
      else
      {
        msg("H{}norm not implemented!", derivativeOrder);
        return -1;
      }
    }

    double gradientNormOfNegativeSolution(SolutionVector& x)
    {
      valueOf(solutionFlow_, phi_) << valueOf(x, phi_);
      Recursive::transform(x.impl(), Operation::Scale<double>{-1.}, x.impl());
      valueOf(solutionFlow_, w_) << valueOf(x, w_);
      auto d = makeDOFVector(*probNewton_.globalBasis());
      explicitEulerUpdate(d);

      return norm(d);
    }

    double gradientNormAt(SolutionVector const& x, int normOrder = NormOrder)
    {
      auto tmp = solutionFlow_;
      Recursive::transform(solutionFlow_.impl(), Operation::Id{}, x.impl());
      auto d = makeDOFVector(*probNewton_.globalBasis());
      explicitEulerUpdate(d);
      solutionFlow_ = tmp;
      return norm(d, normOrder);
    }

    /**
     * @brief check condition for update in globalized Newton Method
     *
     * @param d DOFVector with Newton update
     * @return true if update is accepted, else false
     */
     //
    bool checkUpdate(SolutionVector const& d)
    {
      // TODO float comp
      // TODO use discrete norm? or same norm as for flow?
      auto dNorm
        = std::pow(std::sqrt(integrate(sqr(valueOf(d, phi_)) + sqr(valueOf(d, w_)), gv_)), 2 + p_);
      if (minimize_)
        return (Recursive::innerProduct(probNewton_.rhsVector()->impl(), d.impl(), 0.)
          >= rho_ * dNorm);
      else
        return (-Recursive::innerProduct(probNewton_.rhsVector()->impl(), d.impl(), 0.)
          >= rho_ * dNorm);
    }

    void assemble(Problem& p, bool assembleMatrix = true, bool assembleRhs = true)
    {
      Dune::Timer timer;
      AdaptInfo adaptInfo("problem");
      p.buildAfterAdapt(adaptInfo, Flag{0}, assembleMatrix, assembleRhs);
      if (reAssembleDirac_)
      {
        addDefectOperators(p);
      }
      info("Total assembling took {} seconds", timer.elapsed());
    }

    void solve(Problem& p)
    {
      info("Solving Linear System of Problem {}", p.name());
      p.rhsVector()->finish();
      Dune::Timer timer;
      AdaptInfo adaptInfo("problem");
      p.solve(adaptInfo);
      info("Solving System took {} seconds", timer.elapsed());

    }

    /**
     * @brief compute explicit gradient step and write intro input
     *
     * @param d DOFVector to be overwritten
     */
    void explicitEulerUpdate(SolutionVector& d)
    {
      assemble(probGradientFlow_, false, true);
      solve(probGradientFlow_);
      Recursive::transform(d.impl(), Operation::Id{}, probGradientFlow_.solutionVector()->impl());
    }

    void backup()
    {
      this->backup(Parameters::get<std::string>("NewtonStep->backup->solution").value_or("newton.solution"),
        Parameters::get<std::string>("GradientFlow->backup->solution").value_or("gradientflow.solution"));
    }

    void backup(std::string newtonString, std::string flowString)
    {

      assert(newtonString != "" && flowString != "");

      auto path = getDirectory() + "/backup";
      if (!fs::exists(path) || !fs::is_directory(path))
        fs::create_directories(path);
      std::cout << getDirectory() << "/" << newtonString << std::endl;
      solutionNewton_.backup(path + "/" + newtonString);
      solutionFlow_.backup(path + "/" + flowString);
      // backup(*probGradientFlow_.grid(), Parameters::get<std::string>("NewtonStep->backup->grid").value_or("newton.grid"));
    }

    void restore()
    {
      // probNewton_.restore(INIT_NOTHING);
      // probGradientFlow_.restore(INIT_NOTHING);
      // solutionNewton_ = *probNewton_.solutionVector();
      // solutionFlow_ = *probGradientFlow_.solutionVector();
      std::string newtonFilename = Parameters::get<std::string>("NewtonStep->restore->solution").value_or("newton.solution");
      info("Restore from " + newtonFilename);
      solutionNewton_.resize(sizeInfo(*probNewton_.globalBasis()));
      solutionNewton_.restore(newtonFilename);

      std::string flowFilename = Parameters::get<std::string>("GradientFlow->restore->solution").value_or("gradientflow.solution");
      solutionFlow_.resize(sizeInfo(*probGradientFlow_.globalBasis()));
      solutionFlow_.restore(flowFilename);

    }

    void setInitialSolution(SolutionVector& x);
    void setInitialSolution(SolutionVector& x, std::string type);


    double flipSolution(SolutionVector& x, Dune::FieldVector<double, 2> topRight = {0.5,1.})
    {
      auto tmp2 = makeDOFVector(*probNewton_.globalBasis());
      auto writer = createWriter("flippedSolution", tmp2);
      tmp2 = x;
      writer.write(0);

      // write phi values into solutionFlow
      // create Dof vectors with -1 if in area and 1 else (at w), phi doesnot matter
      auto tmp = makeDOFVector(*probNewton_.globalBasis());
      valueOf(tmp, w_) << makeGridFunction(evalAtQP(getFlippingFunction(topRight)), gv_);

      tmp2 = tmp;
      writer.write(1);
      //multiply with x
      Recursive::transform(x.impl(), Operation::Multiplies{}, x.impl(), tmp.impl());
      // set integral over domain to zero
      valueOf(tmp, u_) << integrate(valueOf(x, u_), gv_, 5);
      Recursive::transform(x.impl(), Operation::Minus{}, x.impl(), tmp.impl());
      tmp2 = x;
      writer.write(2);

      valueOf(solutionFlow_, w_) << valueOf(x, w_);
      tmp2 = solutionFlow_;
      writer.write(3);
      x = solutionFlow_;
      explicitEulerUpdate(tmp);
      return norm(tmp);
    }

    /**
     * @brief Most basic routine this class
     *  Implements the different solution methods (chosen via initfile)
     *  - direct Newton scheme
     *  - globalized Newton scheme
     *  - explicit Gradient flow
     *  - implicit Gradient flow with globalized Newton scheme
     */
    void run()
    {
      checkInitialized();

      solveProblem();
      backup();
      auto gradNorm = gradientNormOfNegativeSolution(solutionNewton_);
      info("Norm of Gradient of negative solution {}", gradNorm);
      // switch to second method
      method_ = secondMethod_;
      if (Parameters::get<bool>(name_ + "->parameterEvolution").value_or(false))
        runParameterEvolution();
      if (Parameters::get<bool>(name_ + "->flip displacement").value_or(false))
      {
        auto upperRight = Parameters::get<Dune::FieldVector<double, 2>>(name_ + "->flip displacement->upper right").value_or(Dune::FieldVector<double, 2>{0.5, 0.5});
        info("Flipping Solution in area between 0,0 and {}", upperRight);
        auto gradNorm = flipSolution(solutionNewton_, upperRight);
        info("Norm of Gradient of flipped solution {}", gradNorm);
        runParameterEvolution("BifurcationFlipped");
      }
    }

    /**
     * @brief Solve for zero (out of Plane) Solution. Here Newton works well
     *
     */
    void findZeroSolution(SolutionVector& x)
    {
      checkInitialized();

      auto actualMethod = method_;
      method_ = localNewton;
      auto gf = makeGridFunction(evalAtQP(getZeroVectorFunction()), gv_);
      valueOf(solutionNewton_, u_) << gf;
      if (!solveNewtonScheme())
        error_exit("Could not find zero solution with Newton");
      method_ = actualMethod;
      x = solutionNewton_;
    }

    void projectDefectDistribution(SolutionVector& x, double scale = 1.)
    {
      projectDefectDistribution();
      Recursive::transform(x.impl(), Operation::Scale<double>{scale},
        probGradientFlow_.solutionVector()->impl());
      if constexpr (Lagrange)
      {
        auto integralPhi = integrate(valueOf(x, phi_), gv_);
        auto integralW = integrate(valueOf(x, w_), gv_);
        auto shift = makeDOFVector(*probGradientFlow_.globalBasis());
        valueOf(shift, u_) << getConstantVectorFunction({integralPhi, integralW});
        Recursive::transform(x.impl(), Operation::Minus{}, x.impl(), shift.impl());
      }
    }

    void projectDefectDistribution()
    {
      info("========================================================\n Projecting defect distribution into initial Solution");
      if (reAssembleDirac_)
      {
        // Assume the bilinear form of probGradientFlow_ is already assembled

        // set rhsVector of ProblemStat to zero
        probGradientFlow_.rhsVector()->resizeZero(sizeInfo(*probGradientFlow_.globalBasis()));
        //Add only the defect approximations to rhs

        addDefectOperators(probGradientFlow_);

        probGradientFlow_.rhsVector()->finish();
        // project rhs into V_h
        AdaptInfo adaptInfo("problem");
        solve(probGradientFlow_);
      }
      else
      {
        auto defectRhs = std::make_shared<typename Problem::SystemVector>(probGradientFlow_.globalBasis());
        auto tmp = probGradientFlow_.rhsVector();
        probGradientFlow_.rhsVector() = defectRhs;
        addDefectOperators(probGradientFlow_);

        probGradientFlow_.rhsVector()->finish();
        // project rhs into V_h
        AdaptInfo adaptInfo("problem");
        solve(probGradientFlow_);

        probGradientFlow_.rhsVector() = tmp;

      }
      info("Norm of Defect distribution is ", norm(*probGradientFlow_.solutionVector()));
      auto writer = createWriter("DefectProjection", *probGradientFlow_.solutionVector());
      writer.write(0);
    }

    void projectPointCloud(SolutionVector& x, std::string filename = "LucasData/pointsWithData.csv", int quadOrder = 5);

    void projectIntoDefectDensity(SolutionVector const& x, int quadOrder);

    void projectContinuumSolution(SolutionVector& x, int quadOrder);

    void artificialForce(std::string suffix = "")
    {

      checkInitialized();
      auto writer = createWriter("ArtificialForce" + suffix, solutionNewton_);
      auto csvWriter
        = getCSVWriter("ArtificialForce" + suffix, {"f", "converged?", "$\\Pi$",
                                           "$||\\delta \\Pi||$", "$||\\phi||$", "$||w||$"});
      std::vector<Dune::FieldVector<double, 2>> positionVector = {{0.,0.5 * ymax}, {0.5 * xmax, 0.5 * ymax},  {xmax, 0.5 * ymax}};
      auto lastSolution = solutionNewton_;
      bool breakOnZero = Parameters::get<bool>(name_ + "->bifurcation->break on zero").value_or(false);
      bool breakOnDivergence = Parameters::get<bool>(name_ + "->bifurcation->break on divergence").value_or(false);

      auto forceTerm = [&f = this->constantForce_, positions = positionVector](auto const& x)
      {
        for (auto const& pos : positions)
          if (lt((x - pos).two_norm(), 10.))
            return f;
        return 0.;
      };
      auto opForceTerm = makeOperator(tag::test{}, forceTerm, 5);
      probNewton_.addVectorOperator(opForceTerm, w_);

      constantForce_ = 0.;
      double fMax = Parameters::get<double>(name_ + "->artificial force->f max").value_or(1.);
      int steps = Parameters::get<int>(name_ + "->artificial force->steps").value_or(10);
      double inc = fMax / steps;

      auto diff = solutionNewton_;

      for (; lt(constantForce_, fMax + 1e-10); constantForce_ += inc)
      {
        info("#######################################################");
        info(" f = {}", constantForce_);

        auto n = solveProblem();
        writer.write(constantForce_);
        auto& x = solutionNewton_;
        info("Parameter, converged, Energy, Norm of Gradient, norm of Phi, norm of W");
        csvWriter.tee({(double) constantForce_, (double) n, calculateEnergy(x),
                       gradientNormAt(x),
                       norm(valueOf(x, phi_)),
                       norm(valueOf(x, w_))});


        if ((breakOnZero && (n < 0)))
          break;
        if (breakOnDivergence && n == 0)
          break;
        else
          continue;

        axPy(diff, -1., lastSolution, solutionNewton_);
        lastSolution = solutionNewton_;

      }
      backup("artificialForce_newton.solution", "artificialForce_newton.solution");
      // reset to last convergent solution
      solutionNewton_ = lastSolution;
      auto writerBack = createWriter("ArtificialForce_back" + suffix, solutionNewton_);

      for (; lt(0. - 1e-10, constantForce_); constantForce_ -= inc)
      {
        info("#######################################################");
        info(" f = {}", constantForce_);

        auto n = solveProblem();
        writerBack.write(constantForce_);
        auto& x = solutionNewton_;
        info("Parameter, converged, Energy, Norm of Gradient, norm of Phi, norm of W");
        csvWriter.tee({(double) constantForce_, (double) n, calculateEnergy(x),
                       gradientNormAt(x),
                       norm(valueOf(x, phi_)),
                       norm(valueOf(x, w_))});
        if ((breakOnZero && (n < 0)) || (breakOnDivergence && n == 0))
          break;
        lastSolution = solutionNewton_;

      }
      solutionNewton_ = lastSolution;

      backup("artificialForce_newton.solution", "artificialForce_newton.solution");
    }


    int loadControl(std::string suffix = "")
    {
      checkInitialized();
      auto writer = createWriter("LoadControl_" + suffix, solutionNewton_);
      lambda_ = Parameters::get<double>(name_ + "->loadControl->initial lambda").value_or(0.1);
      double finalLambda = Parameters::get<double>(name_ + "->loadControl->final lambda").value_or(1.);

      double dLambda
        = Parameters::get<double>(name_ + "->loadControl->increment lambda").value_or(0.1);

      int n = 0;
      bool increasing = Dune::FloatCmp::gt(finalLambda, lambda_) && Dune::FloatCmp::gt(dLambda, 0.);
      bool decreasing = Dune::FloatCmp::lt(finalLambda, lambda_) && Dune::FloatCmp::lt(dLambda, 0.);
      assert(increasing != decreasing);
      auto csvWriter
        = getCSVWriter("LoadControl_" + suffix, {"lambda_", "converged?", "$\\Pi$",
                                           "$||\\delta \\Pi||$", "$||\\phi||$", "$||w||$"});
      for (; increasing ? Dune::FloatCmp::le(lambda_, finalLambda) : Dune::FloatCmp::ge(lambda_, finalLambda); lambda_ += dLambda)
      {
        info("#######################################################");
        info(" Lambda = {}", lambda_);

        n = solveProblem();
        writer.write(lambda_);
        auto& x = solutionNewton_;
        info("Parameter, converged, Energy, Norm of Gradient, norm of Phi, norm of W");
        csvWriter.tee({(double) lambda_, (double) n, calculateEnergy(x),
                       gradientNormAt(x),
                       norm(valueOf(x, phi_)),
                       norm(valueOf(x, w_))});
        if (n <= 0)
        {
          lambda_ = 1.;
          return n;
        }
      }
      lambda_ = 1.;
      return n;
    }

    /**
     * @brief Newton iteration scheme
     *        writes vtks and csv
     * @return int number of steps if scheme converged (negated if zero deflection), 0 if not converged
     */
    int solveNewtonScheme(std::string suffix = "")
    {
      checkInitialized();

      auto writer = createWriter("Newton_" + suffix, solutionNewton_);
      std::vector<std::string> header;
      if (logGradientNorm_)
        if (Lagrange)
          header = {"$i$", "$\\Pi(u_i)$", "$||t \\cdot du||$", "$||\\nabla u_i||$",
                  "$\\int \\phi$", "$\\int w$", "$\\lambda_\\phi$", "$\\lambda_w$"};
        else
          header = {"$i$", "$\\Pi(u_i)$", "$||t \\cdot du||$", "$||\\nabla u_i||$",
                  "$\\int \\phi$", "$\\int w$"};
      else
        if (Lagrange)
          header = {"$i$", "$\\Pi(u_i)$", "$||t \\cdot du||$", "$\\int \\phi$", "$\\int w$", "$\\lambda_\\phi$", "$\\lambda_w$"};
        else
          header = {"$i$", "$\\Pi(u_i)$", "$||t \\cdot du||$", "$\\int \\phi$", "$\\int w$"};

      auto csvWriter = getCSVWriter("Newton_" + suffix, header);

      if (method_ == implicitEuler || method_ == globalNewton)
        solutionNewton_ = solutionFlow_;

      double updateNorm = 0.;
      std::size_t i = 1;
      SolutionVector update = makeDOFVector(*probNewton_.globalBasis());

      AdaptInfo adaptInfo("problem");
      double armijoMin = t_init_ * std::pow(beta_, armijoMax_) + 1e-12;
      bool converged = false;
      writer.write(0);

      if (logGradientNorm_)
      {
        info("-------------------------------------------------\n Projecting Gauteaux Derivative ... ");
        double gradientNorm = gradientNormAt(solutionNewton_);

        info("Current Energy, Norm of update, Norm of Gradient at step ");
        if constexpr (Lagrange)
          csvWriter.tee({(double) 0, calculateEnergy(solutionNewton_), updateNorm,
                          gradientNorm, integrate(valueOf(solutionNewton_, phi_), gv_),
                          integrate(valueOf(solutionNewton_, w_), gv_)
                          , valueOf(solutionNewton_, _1)({0.,0.}),
                          valueOf(solutionNewton_, _2)({0.,0.})});
        else
          csvWriter.tee({(double) 0, calculateEnergy(solutionNewton_), updateNorm,
                gradientNorm, integrate(valueOf(solutionNewton_, phi_), gv_),
                integrate(valueOf(solutionNewton_, w_), gv_)});
        info("-------------------------------------------------");

      }

      for (; i <= newtonMax; ++i)
      {
        info("=============================== Newton step {} ================", i);

        Dune::Timer timer;
        // assemble matrix and vector
        assemble(probNewton_);

        // solve the linear system using the solver given in the initfile
        try
        {
          solve(probNewton_);

          if constexpr (!updateFormulation_)
            Recursive::transform(update.impl(), Operation::Minus{}, solutionNewton_.impl(),
              probNewton_.solutionVector()->impl());
          else
            Recursive::transform(update.impl(), Operation::Id{},
              probNewton_.solutionVector()->impl());
        }
        catch (const std::exception& e)
        {
          warn(e.what());
          warn(" Performing explicit gradient flow step");
          try
          {
            auto tmp = solutionFlow_;
            solutionFlow_ = solutionNewton_;
            explicitEulerUpdate(update);
            solutionFlow_ = tmp;
          }
          catch (const std::exception& e)
          {
            error(e.what());
            error("Aborting Newton Scheme");
            return 0;
          }

        }

        // compute Norm of update
        info("_____________________________________________________________");
        updateNorm = norm(update);
        info("L2 Norm of Newton update after {} steps is {}", i, updateNorm);
        bool updateAccepted = true;


        // TODO use global newton also for implicit flow
        if (method_ == globalNewton)
        {
          updateAccepted = checkUpdate(update);
          if (!updateAccepted)
          {
            info("Newton update is not accepted, moving in gradient direction!");
            explicitEulerUpdate(update);
            updateNorm = norm(update);
            info("Replace Newton update with gradient step of norm {}", updateNorm);
          }
          // TODO Compute stepsize throw error and catch
          double t = computeStepSize(probNewton_, update);
          if (t < armijoMin && updateAccepted) // Don't repeat eulerstep
          {
            warn("No feasible armijo stepsize found");
            explicitEulerUpdate(update);
            updateNorm = norm(update);
            t = computeStepSize(probNewton_, update);
            info("Replace Newton update with gradient step of norm {}", updateNorm);
          }

          Recursive::transform(update.impl(), Operation::Scale<double>{t}, update.impl());
          // Consider scheme converged if actual step is small enough
          updateNorm *= t;
        }

        // update solution, possibly with damping
        if (damping_ > 1e-12)
        {
          if constexpr (updateFormulation_)
            // u_{n+1} = u_{n} + a* du
            axPy(solutionNewton_, damping_, update, solutionNewton_);
          else
            // u_{n+1} = a * u_{n+1} + (1-a) * u_{n}
            axPby(solutionNewton_, damping_, probNewton_.solutionVector(),
              1. - damping_, solutionNewton_);
        }
        else
        {
          if constexpr (updateFormulation_)
            // u_{n+1} = u_{n} + du
            axPy(solutionNewton_, 1., update, solutionNewton_);
          else
          {
            assert(method_ == localNewton);
            // u_{n+1} = u_{n+1}
            solutionNewton_ = *probNewton_.solutionVector();
          }
        }

        info("Newtonstep {} took {}", i, timer.elapsed());
        info("=============================================================");

        // write
        writer.write(i);
        if (logGradientNorm_)
        {
          info("-------------------------------------------------\nProjecting Gauteaux Derivative ... ");
          double gradientNorm = gradientNormAt(solutionNewton_);

          info("Current Energy, Norm of update, Norm of Gradient at step ");
          if constexpr (Lagrange)
            csvWriter.tee({(double) i, calculateEnergy(solutionNewton_), updateNorm,
                            gradientNorm, integrate(valueOf(solutionNewton_, phi_), gv_),
                            integrate(valueOf(solutionNewton_, w_), gv_)
                            , valueOf(solutionNewton_, _1)({0.,0.}),
                            valueOf(solutionNewton_, _2)({0.,0.})});
          else
            csvWriter.tee({(double) i, calculateEnergy(solutionNewton_), updateNorm,
                  gradientNorm, integrate(valueOf(solutionNewton_, phi_), gv_),
                  integrate(valueOf(solutionNewton_, w_), gv_)});
          info("-------------------------------------------------");
        }
        else
        {
          info("Current Energy and L2 Norm of update at step ");
          csvWriter.tee({(double) i, calculateEnergy(solutionNewton_), updateNorm});
        }

        // return if converged
        converged |= (updateNorm < NewtonThreshold_);
        if (converged)
        {
          info("Newton converged after {} steps with L2Norm of Update {}", i, updateNorm);

          // log final gradient norm even if not logged before
          if (!logGradientNorm_ && Environment::infoLevel() >= 2)
          {
            info("-------------------------------------------------\nProjecting Gauteaux Derivative ... ");
            double gradientNorm = gradientNormAt(solutionNewton_);

            info("Gradient Norm = {}", gradientNorm);
          }

          // if constexpr(Lagrange)
          // {
          //   valueOf(solutionNewton_, _1)<<0.;
          //   valueOf(solutionNewton_, _2)<<0.;

          // }

          // print criterion for saddlepoint
          info("DefiniteNess Criterion of Hessian: {}  (>0 => indefinit)"
            , definiteCiterion(valueOf(solutionNewton_, phi_),
              valueOf(solutionNewton_, w_), gv_));

          // solutionFlow_ = solutionNewton_;
          // return negative steps if deflection is zero
          if (integrate(sqr(valueOf(solutionNewton_, w_)), gv_) < 1e-9)
          {
            warn("Newton found Zero Solution");
            return -i;
          }
          else
            return i;
        }
        // break if update is very large
        else if (updateNorm > 1e9 || std::isnan(updateNorm))
          break;

      } // end Newton
      warn("Newton did not converge! L2 Norm of last update: {} ", updateNorm);
      return 0;
    }

    void zeroLagrangeMultipliers(SolutionVector& x)
    {
      if constexpr (Lagrange)
      {
        valueOf(x, makeTreePath(_1)) << 0.;
        valueOf(x, makeTreePath(_2)) << 0.;
      }
    }

    int solveGAD(std::string suffix = "")
    {
      Dune::Timer flowTimer;
      checkInitialized();
      // Gradient Flow
      double L2DiffFlow = 0.;
      auto flowWriter = createWriter("Flow_" + suffix, solutionFlow_);
      auto csvWriter = getCSVWriter("energyFlow", {"$t$", "$\\Pi$", "$||du||$", "newtonsteps"});
      // Write initial Value
      flowWriter.write(0.);
      double t = tau_;
      std::size_t j = 0;
      // findZeroSolution(solutionFlow_);
      // TODO think of better initial values of v
      auto v = makeDOFVector(*probGradientFlow_.globalBasis());
      setInitialSolution(v);
      zeroLagrangeMultipliers(v);
      auto vnorm = std::sqrt(vmv(*probGradientFlow_.systemMatrix(), v));
      info("Norm of initial V is {}, vPv is {}", norm(v), vnorm);
      Recursive::transform(v.impl(), Operation::Scale<double>{1. / vnorm}, v.impl());
      auto& x = solutionFlow_;
      axPy(x, 1., v, x);
      auto vWriter = createWriter("GentlestAscend_" + suffix, v);
      vWriter.write(0.);

      bool solveForV = true;
      if (solveForV)
      {
        // assemble Hessian at x
        solutionNewton_ = x;
        assemble(probNewton_, true, false);
        // compute gentlest ascend descent direction
        // temporary Vectors to write into
        // zeroLagrangeMultipliers(v);
        auto HessianV = v;
        auto ProjV = v;
        for (; t < 1; t += tau_, j++)
        {
          // normalized vector matrix vector product
          auto factor = vmv(*probNewton_.systemMatrix(), v);
          auto scaling = vmv(*probGradientFlow_.systemMatrix(), v);
          info("Norm2 of v is {}", scaling);
          factor /= scaling;
          info("Factor in V equation: {}", factor);
          // fill temporary Vectors
          mv(*probNewton_.systemMatrix(), v, HessianV);
          mv(*probGradientFlow_.systemMatrix(), v, ProjV);

          // add everything to rhs of Gradient flow problem
          axPby(*probGradientFlow_.rhsVector(), factor, ProjV, -1., HessianV);

          // apply explicit Euler step to v
          solve(probGradientFlow_);
          axPy(v, tau_, *probGradientFlow_.solutionVector(), v);
          auto updateNorm = norm(*probGradientFlow_.solutionVector());
          info("time: {}; norm of v; {}; norm of v update: {}", t, norm(v), updateNorm);
          vWriter.write(t);
          if (updateNorm < 1e-8)
            break;
        }
      }

      for (; t < tMax_; t += tau_, j++)
      {
        Dune::Timer timer;

        // assemble Hessian at x
        solutionNewton_ = x;
        assemble(probNewton_, true, false);


        // compute gentlest ascend descent direction
        // temporary Vectors to write into
        // zeroLagrangeMultipliers(v);
        auto HessianV = v;
        auto ProjV = v;
        // normalized vector matrix vector product
        auto factor = vmv(*probNewton_.systemMatrix(), v);
        auto scaling = vmv(*probGradientFlow_.systemMatrix(), v);
        info("Norm2 of v is {}", scaling);
        factor /= scaling;
        info("Factor in V equation: {}", factor);
        // fill temporary Vectors
        mv(*probNewton_.systemMatrix(), v, HessianV);
        mv(*probGradientFlow_.systemMatrix(), v, ProjV);

        // add everything to rhs of Gradient flow problem
        axPby(*probGradientFlow_.rhsVector(), factor, ProjV, -1., HessianV);

        // apply explicit Euler step to v
        solve(probGradientFlow_);
        axPy(v, tau_, *probGradientFlow_.solutionVector(), v);
        info("time: {}; norm of v; {}; norm of v update: {}", t, norm(v), norm(*probGradientFlow_.solutionVector()));
        vWriter.write(t);

        // Now v is an approximation of the eigenvector to the smallest eigenvalue
        // We apply a explicit step of a gradient descend, where v is turned into an ascend

        // first assemble negative gradient
        assemble(probGradientFlow_, false, true);
        // add v's projection into gradient twice
        // normalized directional derivative
        // zeroLagrangeMultipliers(v);
        factor = -2. * Recursive::innerProduct(probGradientFlow_.rhsVector()->impl(), v.impl(), 0.);
        factor /= vmv(*probGradientFlow_.systemMatrix(), v);
        info("Factor in X equation: {}", factor);

        // fill temporary Vector
        // note that this is the second time we use this matrix. Potiential sign changes cancel thus out
        mv(*probGradientFlow_.systemMatrix(), v, ProjV);
        axPy(*probGradientFlow_.rhsVector(), factor, ProjV, *probGradientFlow_.rhsVector());

        // apply Euler step
        solve(probGradientFlow_);
        axPy(x, tau_, *probGradientFlow_.solutionVector(), x);

        // Write
        L2DiffFlow = norm(*probGradientFlow_.solutionVector());
        info("Time; Energy; L2 Norm of update ");
        double energy = calculateEnergy(solutionFlow_);
        csvWriter.tee({t, energy, L2DiffFlow});
        if (std::isnan(energy) || std::isnan(L2DiffFlow))
        {
          warn("Gradient Flow diverged!");
          return 0;
        }

        info("=============================================================");
        flowWriter.write(t);
        info("Timestep took {}", timer.elapsed());
        if (L2DiffFlow < NewtonThreshold_)
        {
          info("Gradient Flow converged! Norm of last update: {}", L2DiffFlow);
          break;
        }
        else if (switchToNewton_ && L2DiffFlow < switchThreshold_)
        {
          info("Switching to Newton as norm of update is {}", L2DiffFlow);
          solutionNewton_ = solutionFlow_;
          solveNewtonScheme("afterGradientFlow");
        }
      } // end flow
      if ((L2DiffFlow > NewtonThreshold_ * 100.))
      {
        warn("Gradient Flow did not converge! Norm of last update: ", L2DiffFlow);
        return 0;
      }
      else
      {
        info("Gradient flow took {}  seconds", flowTimer.elapsed());
        return j;
      }
    }

    int solveFlow(std::string suffix = "")
    {
      Dune::Timer flowTimer;
      checkInitialized();
      // Gradient Flow
      double L2DiffFlow = 0.;
      auto flowWriter = createWriter("Flow_" + suffix, solutionFlow_);
      auto csvWriter = getCSVWriter("energyFlow", {"$t$", "$\\Pi$", "$||du||$", "newtonsteps"});
      // Write initial Value
      flowWriter.write(0.);
      double t = 0.;
      std::vector<SolutionVector> images;
      std::vector<SolutionVector> tangentials;
      if (method_ == stringMethod)
      {
        findZeroSolution(solutionFlow_);
        auto stringWriter = createWriter("initialString_", solutionFlow_);

        auto update = makeDOFVector(*probGradientFlow_.globalBasis());
        Recursive::transform(update.impl(), Operation::Minus{}, solutionFlow_.impl(),
          solutionNewton_.impl());
        for (std::size_t i = 0; i < nString_; ++i)
        {
          images.push_back(makeDOFVector(*probGradientFlow_.globalBasis()));

          tangentials.push_back(makeDOFVector(*probGradientFlow_.globalBasis()));

          // Initialize string image and tangentials
          if (i == 0)
          {
            images[i] = solutionNewton_;
          }
          else
          {
            // interpolate images linearly between 0 (solNewton) and initialGuess(solFlow)
            Recursive::transform(
              images[i].impl(),
              Operation::compose(
                Operation::Plus{},
                Operation::compose(Operation::Scale<double>{((double) i) / nString_},
                  Operation::Arg<1>{}),
                Operation::Arg<0>{}),
              solutionNewton_.impl(), update.impl());
            if (useTangentials_)
            {
              // initialize tangentials
              Recursive::transform(tangentials[i].impl(), Operation::Minus{}, images[i].impl(),
                images[i - 1].impl());
              double normTangential = norm(tangentials[i]);
              info("Norm of initial tangential: {}", normTangential);
              Recursive::transform(tangentials[i].impl(),
                Operation::Scale<double>{1. / normTangential},
                tangentials[i].impl());
            }
          }
          info("Energy of image {} is {}", i, calculateEnergy(images[i]));
        }
      }
      std::size_t j = 0;
      for (; t < tMax_; t += tau_, j++)
      {
        Dune::Timer timer;
        // TODO add convergence criterion for gradient flows
        if (method_ == explicitEuler)
        {
          // use Newton solution to save memory
          auto& update = solutionNewton_;
          // compute gradient descent direction
          explicitEulerUpdate(update);
          // compute stepsize
          tau_ = computeStepSize(probGradientFlow_, update, solutionFlow_);
          // TODO reuse computed new Energy
          //  apply update
          Recursive::transform(solutionFlow_.impl(),
            Operation::compose(Operation::Plus{},
              Operation::compose(Operation::Scale<double>{tau_},
                Operation::Arg<0>{}),
              Operation::Arg<1>{}),
            update.impl(), solutionFlow_.impl());
          // Write
          L2DiffFlow = norm(update);
          info("Current Energy and L2 Norm of gradient at time ");
          double energy = calculateEnergy(solutionFlow_);
          csvWriter.tee({t, energy, L2DiffFlow});
          if (std::isnan(energy) || std::isnan(L2DiffFlow))
          {
            warn("Gradient Flow diverged!");
            return 0;
          }

          info("=============================================================");
          flowWriter.write(t);
        }
        else if (method_ == stringMethod)
        {
          auto stringWriter = createWriter("String_" + std::to_string(t), solutionFlow_);

          // TODO set Tau
          for (std::size_t i = 1; i < nString_; ++i)
          {
            solutionFlow_ = images[i];
            solutionNewton_ = images[i];

            assemble(probNewton_, false, true);

            if (useTangentials_)
            {
              // calculate functions for rhs
              double factor = -Recursive::innerProduct(probNewton_.rhsVector()->impl(),
                tangentials[i].impl(), 0.);
              // final image has different boundary condition
              if (nString_ == i)
                Recursive::transform(solutionNewton_.impl(),
                  Operation::Scale<double>{parameterString_* factor},
                  tangentials[i].impl());
              else
                Recursive::transform(solutionNewton_.impl(),
                  Operation::Scale<double>{factor}, tangentials[i].impl());
            }
            assemble(probGradientFlow_, false, true);

            // project rhs into V_h
            AdaptInfo adaptInfo("problem");
            solve(probGradientFlow_);
            // calculate new image
            Recursive::transform(
              images[i].impl(),
              Operation::compose(
                Operation::Plus{},
                Operation::compose(Operation::Scale<double>{tau_}, Operation::Arg<0>{}),
                Operation::Arg<1>{}),
              probGradientFlow_.solutionVector()->impl(), images[i].impl());
            // TODO add nan check
            if (useTangentials_)
            {
              // calculate new tangentials
              Recursive::transform(tangentials[i].impl(), Operation::Minus{}, images[i].impl(),
                images[i - 1].impl());
              // normalize later
            }

            solutionFlow_ = images[i];
            stringWriter.write(i);
            info("Energy of image {} is {}", i, calculateEnergy(images[i]));
          } // end image loop

          // Check if Newton converges
          int newtonSteps = -1;
          int newtonCheck = Parameters::get<int>(name_ + "->string->check newton").value_or(5);
          bool continueFlow = true;
          if (newtonCheck && j % newtonCheck == 0)
          {
            solutionNewton_ = images[nString_ - 1];
            newtonSteps = solveNewtonScheme();
            if (newtonSteps != 0)
            {

              if (integrate(sqr(valueOf(solutionNewton_, w_)), gv_) < 1e-9)
              {
                warn("Newton found Zero Solution");
                newtonSteps *= -1;
              }
              else
              {
                continueFlow = false;
              }
            }
          }
          // Write
          auto const& update = *probGradientFlow_.solutionVector();
          L2DiffFlow = sqrt(integrate(sqr(valueOf(update, phi_)) + sqr(valueOf(update, w_)), gv_));
          info("Current Energy and L2 Norm of gradient at time ");
          double energy = calculateEnergy(images[nString_ - 1]);
          csvWriter.tee({t, energy, L2DiffFlow, (double) newtonSteps});
          if (std::isnan(energy) || std::isnan(L2DiffFlow))
          {
            info("Gradient Flow diverged!");
            return 0;
          }

          info("=============================================================");
          flowWriter.write(t);
          if (!continueFlow)
          {
            info("String method took {} seconds", flowTimer.elapsed());
            return j;
          }
          // check monotonic constraint and truncate
          double prevEnergy = calculateEnergy(images[0]);
          int newN = -1;
          if (Parameters::get<bool>(name_ + "->string->truncate").value_or(true))
          {
            // truncate before (!) first maximum is attained
            for (std::size_t i = 1; i < nString_; ++i)
            {
              double energy = calculateEnergy(images[i]);
              if (minimize_ ? energy > prevEnergy : energy < prevEnergy)
                prevEnergy = energy;
              else
                newN = i;
            }
            if (newN == -1) // keep entire string if no maximum
              newN = nString_;
            else if (newN == 0)
              error_exit("String attained its maximum at first image!");
          }
          else
          {
            newN = nString_;
          }

          // Reparametrisation
          if (Parameters::get<bool>(name_ + "->string->reparametrize").value_or(true))
          {
            Dune::Timer reparamTimer;
            test_exit(useTangentials_,
              "Reparametrization only implemented when tangentials are calculated!");
            std::vector<double> distances;
            std::vector<SolutionVector> newImages;

            auto diff = makeDOFVector(*probNewton_.globalBasis());
            for (int i = 1; i < newN; ++i)
            {
              Recursive::transform(diff.impl(), Operation::Minus{}, images[i].impl(),
                images[i - 1].impl());
              distances.push_back(norm(diff));
            }
            auto length = std::accumulate(distances.begin(), distances.end(), 0.);
            // redistribute by linear interpolation
            // TODO think of improvement!
            auto step = length / nString_;
            double currentLength = 0.;
            // first images does not change
            newImages.push_back(images[0]);

            for (std::size_t i = 1; i < nString_; ++i)
            {
              newImages.push_back(makeDOFVector(*probGradientFlow_.globalBasis()));

              auto pos = step * i;
              int k = 0;
              // find j such that pos is between pos(j) and pos(j + 1)
              for (; k < newN; ++k)
              {
                if (pos > currentLength && pos < currentLength + step)
                {
                  info("interpolating between image {} and {}", k, k + 1);
                  break;
                }
                else
                {
                  currentLength += step;
                }
              }
              // linear interpolation
              //  tangential j points from image j-1 to image j and is not! normalized
              // newImages[i] = images[j] + (pos - currentLength) * tangentials[j + 1];
              Recursive::transform(
                newImages[i].impl(),
                Operation::compose(
                  Operation::Plus{}, Operation::Arg<0>{},
                  Operation::compose(Operation::Scale<double>{pos - currentLength},
                    Operation::Arg<1>{})),
                images[k].impl(), tangentials[k].impl());
            }
            images = newImages;
            // recalculate tangentials
            for (std::size_t i = 1; i < nString_; ++i)
              Recursive::transform(tangentials[i].impl(), Operation::Minus{}, images[i].impl(),
                images[i - 1].impl());
            info("Reparametrization took {} seconds", reparamTimer.elapsed());
          }
          // Normalize because we haven't done this before
          if (useTangentials_)
            for (std::size_t i = 1; i < nString_; ++i)
            {
              double normTangential = norm(tangentials[i]);
              Recursive::transform(tangentials[i].impl(),
                Operation::Scale<double>{1. / normTangential},
                tangentials[i].impl());
            }
        } // end if stringmethod
        // method is either local or global newton or implicit gradient flow (i.e. with newton)
        else
        {
          int newtonConverged = solveNewtonScheme(suffix);
          // repeat Newton scheme if not converged
          if (!newtonConverged)
          {
            // Do not update solution, etc.
            // Correct time
            t -= tau_;
            // Decrease tau_ if Newton did not converge
            tau_ /= 2.;
            continue;
          }

          // if newton converged update flow solution

          L2DiffFlow = sqrt(integrate(
            sqr(valueOf(solutionFlow_, phi_) - valueOf(solutionNewton_, phi_))
            + sqr(valueOf(solutionFlow_, w_) - valueOf(solutionNewton_, w_)),
            gv_));
          double energy = calculateEnergy(solutionNewton_);
          info("Current Energy and L2 Norm of update at time ");

          csvWriter.tee({t, energy, L2DiffFlow});
          if (std::isnan(energy) || std::isnan(L2DiffFlow))
          {
            warn("Gradient Flow diverged!");
            return 0;
          }
          // update Flow solution
          Recursive::transform(solutionFlow_.impl(), Operation::Id{},
            solutionNewton_.impl());

          // Write
          flowWriter.write(t);

          // break if converged
          if (L2DiffFlow < NewtonThreshold_ * 100.)
          {
            info("Gradient flow converged at {} with L2Norm of Update {} \n Gradient flow took {} seconds"
              , t, L2DiffFlow, flowTimer.elapsed());
            return j;
          }

          tau_ *= 1.1; // increase tau_
        }
        info("Timestep took {}", timer.elapsed());
        if (L2DiffFlow < NewtonThreshold_)
        {
          info("Gradient Flow converged! Norm of last update: {}", L2DiffFlow);
        }
        else if (switchToNewton_ && L2DiffFlow < switchThreshold_)
        {
          info("Switching to Newton as norm of update is {}", L2DiffFlow);
          solutionNewton_ = solutionFlow_;
          solveNewtonScheme("afterGradientFlow");
        }
      } // end flow
      if ((L2DiffFlow > NewtonThreshold_ * 100.))
      {
        warn("Gradient Flow did not converge! Norm of last update: ", L2DiffFlow);
        return 0;
      }
      else
      {
        info("Gradient flow took {}  seconds", flowTimer.elapsed());
        return j;
      }
    }

    int solveProblem(std::string suffix = "")
    {
      if (method_ == localNewton)
        return solveNewtonScheme(suffix);
      else if (method_ == gentlestAscend)
        return solveGAD(suffix);
      else
        return solveFlow(suffix);
    }

    template <class T>
    void runParameterSearch(T& parameter, T start, T stop, int steps,
      std::string parameterName = "parameter")
    {
      if (method_ == globalNewton || method_ == localNewton)
        runParameterSearch(parameter, start, stop, steps, solutionNewton_, parameterName);
      else
        runParameterSearch(parameter, start, stop, steps, solutionFlow_, parameterName);
    }


    template <class T>
    void runParameterSearch(T& parameter, T start, T stop, int steps, SolutionVector& x,
      std::string parameterName = "parameter")
    {
      checkInitialized();
      auto parameterWriter = createWriter("parameterSearch", x);
      parameterWriter.write(start);
      double parameterIncrease = (stop - start) / steps;
      auto csvWriter
        = getCSVWriter("parameterSearch", {parameterName, "converged?", "$\\Pi$",
                                           "$||\\delta \\Pi||$", "$||\\phi||$", "$||w||$", "$\\int[w,w]$",  "$||[w,w]||_0$"});
      parameter = start;
      if (Parameters::get<bool>(name_ + "->search->artificial force").value_or(false))
      {
        std::vector<Dune::FieldVector<double, 2>> positionVector = {{0.,0.5 * ymax}, {0.5 * xmax, 0.5 * ymax},  {xmax, 0.5 * ymax}};

        constantForce_ = Parameters::get<double>(name_ + "search->artificial force->value").value_or(0.0001);
        auto forceTerm = [&f = this->constantForce_, positions = positionVector](auto const& x)
        {
          for (auto const& pos : positions)
            if (lt((x - pos).two_norm(), 10.))
              return f;
          return 0.;
        };
        auto opForceTerm = makeOperator(tag::test{}, forceTerm, 5);
        probNewton_.addVectorOperator(opForceTerm, w_);
      }
      // increase or decrease bifurcation parameter
      for (; (parameter < stop); parameter += parameterIncrease)
      {
        setInitialSolution(x);

        int converged = 0;
        if (Parameters::get<bool>(name_ + "->search->loadControl").value_or(false))
          converged = loadControl(std::to_string(parameter));
        else
          converged = solveProblem();

        // if (method_ == globalNewton || method_ == localNewton)
          // solutionFlow_ = solutionNewton_;
        info("Parameter, converged, Energy, Norm of Gradient, norm of Phi, norm of W, disp. Incomp, disp Incomp Norm");
        csvWriter.tee({(double) parameter, (double) converged, calculateEnergy(x),
                       gradientNormAt(x),
                       norm(valueOf(x, phi_)),
                       norm(valueOf(x, w_)),
                        displacementIncompatibility(valueOf(x,w_), gv_),
                        displacementIncompatibilityNorm(valueOf(x,w_), gv_)
          });
        parameterWriter.write(parameter);
        if (converged > 0)
        {
          backup("Newton_Search_" + std::to_string(parameter), "Flow_Search_" + std::to_string(parameter));
          if (Parameters::get<bool>(name_ + "->search->bifurcation").value_or(false))
          {
            auto E = E_;
            runParameterEvolution("Bifurcation_" + std::to_string(parameter));
            E_ = E;
          }
          else if (Parameters::get<bool>(name_ + "->search->bifurcation with loadcontrol").value_or(false))
          {
            auto E = E_;
            runParameterEvolutionWithLoadControl("BifurcationWithLoadControl_" + std::to_string(parameter));
            E_ = E;
          }
          if (Parameters::get<bool>(name_ + "->search->evolve D").value_or(false))
          {
            auto D = D_;
            runParameterEvolution_D("Bifurcation_" + std::to_string(parameter));
            D_ = D;
          }
        }

      } // end parameter loop
    }

    void runParameterSearch_Scaling(double start, double stop, int steps)
    {
      runParameterSearch<double>(this->scale_, start, stop, steps, "scaling");
    }

    void runParameterSearch_E(double start, double stop, int steps)
    {
      runParameterSearch<double>(E_, start, stop, steps, "E");
    }

    void runParameterSearch_D(double start, double stop, int steps)
    {
      runParameterSearch<double>(D_, start, stop, steps, "D");
    }


    void runParameterEvolution(std::string title = "Bifurcation")
    {
      checkInitialized();
      auto parameterWriter = createWriter(title, solutionFlow_);
      parameterWriter.write(0.);
      auto csvWriter = getCSVWriter(title, {"$E/D$","$\\Pi$","$\\phi(0.5,0.5)$", "$w(0.5,0.5)$", "$||\\phi||$", "$||w||$"});
      bool breakOnZero = Parameters::get<bool>(name_ + "->bifurcation->break on zero").value_or(false);
      bool breakOnDivergence = Parameters::get<bool>(name_ + "->bifurcation->break on divergence").value_or(false);

      // increase or decrease bifurcation parameter
      for (; increasing_ ? (E_ < EEnd_) : (E_ > EEnd_); E_ = E_ * EFactor_ + EShift_)
      {
        double energy = calculateEnergy(solutionFlow_);
        if (std::isnan(energy))
        {
          throw std::runtime_error("initial energy is nan");
        }
        info("Initial Energy for Gamma = {} is {}", E_ / D_, energy);


        int converged = solveProblem();

        if (method_ == globalNewton || method_ == localNewton)
          solutionFlow_ = solutionNewton_;

        double Energy = calculateEnergy(solutionFlow_);
        info("Final energy for Gamma  = {} is {}", E_ / D_, calculateEnergy(solutionFlow_));
        csvWriter.tee({E_ / D_, Energy, valueOf(solutionFlow_,phi_)({0.5 * xmax,0.5 * ymax}),
                        valueOf(solutionFlow_,w_)({0.5 * xmax,0.5 * ymax}), norm(valueOf(solutionFlow_,phi_)),
                        norm(valueOf(solutionFlow_,w_))});

        parameterWriter.write(E_ / D_);

        if ((breakOnZero && (converged < 0)) || (breakOnDivergence && converged == 0))
          break;
      } // end parameter loop
    }

    void runParameterEvolution_D(std::string title = "Evolve D")
    {
      checkInitialized();
      auto parameterWriter = createWriter(title, solutionFlow_);
      parameterWriter.write(0.);
      auto csvWriter = getCSVWriter(title, {"$D$","$h$", "$\\Pi$","$\\phi(0.5,0.5)$", "$w(0.5,0.5)$", "$||\\phi||$", "$||w||$"});
      bool breakOnZero = Parameters::get<bool>(name_ + "->bifurcation->break on zero").value_or(false);
      bool breakOnDivergence = Parameters::get<bool>(name_ + "->bifurcation->break on divergence").value_or(false);
      double DEnd = Parameters::get<double>(name_ + "->evolve D->finalD").value_or(1.);
      int steps = Parameters::get<int>(name_ + "->evolve D->steps").value_or(10);

      info("Evolve Bending modulus D from {} to {}", D_, DEnd);
      bool increasing = lt(D_, DEnd);
      double DShift = (DEnd - D_) / steps;
      double E3d = E_ * 0.9375;
      // increase or decrease bifurcation parameter
      for (; increasing ? lt(D_, DEnd) : lt(DEnd, D_); D_ = D_ + DShift)
      {
        double h = std::pow(12 * (1 - Nu_ * Nu_) * D_ / E3d, 1. / 3.);

        double energy = calculateEnergy(solutionFlow_);
        if (std::isnan(energy))
        {
          throw std::runtime_error("initial energy is nan");
        }
        info("Initial Energy for D = {} is {}", D_, energy);


        int converged = solveProblem();

        if (method_ == globalNewton || method_ == localNewton)
          solutionFlow_ = solutionNewton_;

        double Energy = calculateEnergy(solutionFlow_);
        info("Final energy for D  = {} is {}", D_, calculateEnergy(solutionFlow_));
        csvWriter.tee({D_ ,h, Energy, valueOf(solutionFlow_,phi_)({0.5 * xmax,0.5 * ymax}),
                        valueOf(solutionFlow_,w_)({0.5 * xmax,0.5 * ymax}), norm(valueOf(solutionFlow_,phi_)),
                        norm(valueOf(solutionFlow_,w_))});

        parameterWriter.write(D_);

        if ((breakOnZero && (converged < 0)) || (breakOnDivergence && converged == 0))
          break;
      } // end parameter loop
      backup("Newton_D=" + std::to_string(D_) + ".solution", "Flow_D=" + std::to_string(D_) + ".solution");
    }

    void runParameterEvolutionWithLoadControl(std::string title = "BifurcationWithLoadControl")
    {
      checkInitialized();
      auto parameterWriter = createWriter(title, solutionFlow_);
      parameterWriter.write(0.);
      auto csvWriter = getCSVWriter(title, {"$E/D$", "$\\lambda$","$\\Pi$","$\\phi(0.5,0.5)$", "$w(0.5,0.5)$", "$||\\phi||$", "$||w||$"});
      bool breakOnZero = Parameters::get<bool>(name_ + "->bifurcation->break on zero").value_or(false);
      bool breakOnDivergence = Parameters::get<bool>(name_ + "->bifurcation->break on divergence").value_or(false);

      lambda_ = Parameters::get<double>(name_ + "->loadControl->initial lambda").value_or(0.1);
      double finalLambda = Parameters::get<double>(name_ + "->loadControl->final lambda").value_or(1.);

      double dLambda
        = Parameters::get<double>(name_ + "->loadControl->increment lambda").value_or(0.1);
      // div with remainder is intended
      int Esteps = std::log(EEnd_ / E_) / std::log(EFactor_);
      int lambdaSteps = (finalLambda - lambda_) / dLambda;
      int updateLambda = Esteps / lambdaSteps;
      if (updateLambda <= 0)
        updateLambda = 1;
      int j = 0;
      // increase or decrease bifurcation parameter
      for (; increasing_ ? (E_ < EEnd_) : (E_ > EEnd_); E_ = E_ * EFactor_ + EShift_, j++)
      {
        double energy = calculateEnergy(solutionFlow_);
        if (std::isnan(energy))
        {
          throw std::runtime_error("initial energy is nan");
        }
        info("Initial Energy for Gamma = {} is {}", E_ / D_, energy);


        int converged = solveProblem();

        if (method_ == globalNewton || method_ == localNewton)
          solutionFlow_ = solutionNewton_;

        double Energy = calculateEnergy(solutionFlow_);
        info("Final energy for Gamma  = {} is {}", E_ / D_, calculateEnergy(solutionFlow_));
        csvWriter.tee({E_ / D_, lambda_, Energy, valueOf(solutionFlow_,phi_)({0.5 * xmax,0.5 * ymax}),
                        valueOf(solutionFlow_,w_)({0.5 * xmax,0.5 * ymax}), norm(valueOf(solutionFlow_,phi_)),
                        norm(valueOf(solutionFlow_,w_))});

        parameterWriter.write(E_ / D_);

        if ((breakOnZero && (converged < 0)) || (breakOnDivergence && converged == 0))
          break;

        // update Lambda
        if ((lt(dLambda, 0.) ? lt(finalLambda, lambda_) : lt(lambda_, finalLambda)) && j == updateLambda)
        {
          j = 0;
          lambda_ += dLambda;
        }
      } // end parameter loop
      // Calculate solution with exact values
      E_ = EEnd_;
      lambda_ = finalLambda;
      int converged = solveProblem();

      if (method_ == globalNewton || method_ == localNewton)
        solutionFlow_ = solutionNewton_;

      double Energy = calculateEnergy(solutionFlow_);
      info("Final energy for Gamma  = {} is {}", E_ / D_, calculateEnergy(solutionFlow_));
      csvWriter.tee({E_ / D_, lambda_, Energy, valueOf(solutionFlow_,phi_)({0.5 * xmax,0.5 * ymax}),
                      valueOf(solutionFlow_,w_)({0.5 * xmax,0.5 * ymax}), norm(valueOf(solutionFlow_,phi_)),
                      norm(valueOf(solutionFlow_,w_))});

      parameterWriter.write(E_ / D_);
      info("performing backup");
      this->backup();

      if (Parameters::get<bool>(name_ + "->bifurcation with loadcontrol->bifurcate back").value_or(false))
      {
        Parameters::get<double>(name_ + "->initialE", EEnd_);
        EFactor_ = 1 / EFactor_;
        increasing_ = !increasing_;
        runParameterEvolution("bifurcate Back");
        EFactor_ = 1 / EFactor_;
        Parameters::get<double>(name_ + "->finalE", EEnd_);
        increasing_ = !increasing_;

      }

    }

    void searchForBifurcation()
    {
      int initialN = Parameters::get<int>(name_ + "->vary thickness->initial n").value_or(0);
      int finalN = Parameters::get<int>(name_ + "->vary thickness->final n").value_or(6);
      if (initialN < finalN)
      {
        variateThickness(); // no search here we just look for the solution upstream
        // now set parameters to search downstream for bifuraction
        Parameters::set(name_ + "->vary thickness->initial n", std::to_string(finalN));
        Parameters::set(name_ + "->vary thickness->final n", std::to_string(initialN));
      }
      variateThickness("Bifurcation Search", true);
    }


    /**
     * @brief Starting from an previously found solution, vary the thickness while keeping 3d Youngs modulus E_3d constant.
     *        Precisely, we set D = 2^n, calculate the thickness h , and set E_2d = E_3d * h.
     *        The range of n is specified via initfile
     *
     * @param title prefix for files to be saved
     * @param searchBifurcation boolean indicating whether and additional search for other solutions is performed, when some criterion is met
     */
    void variateThickness(std::string title = "variateThickness", bool searchBifurcation = false)
    {
      checkInitialized();
      auto initialN = Parameters::get<int>(name_ + "->vary thickness->initial n").value_or(0);
      auto finalN = Parameters::get<int>(name_ + "->vary thickness->final n").value_or(6);
      auto intermediateSteps = Parameters::get<int>(name_ + "->vary thickness->intermediate steps").value_or(5);
      auto E3d = Parameters::get<double>(name_ + "->vary thickness->E3d").value_or(0.22729);
      assert(intermediateSteps > 0);
      auto parameterWriter = createWriter(title, solutionNewton_);
      parameterWriter.write(0.);

      auto csvWriter = getCSVWriter(title, {"$n$", "$h$","converged?", "$\\Pi$","$\\phi(defect)$",
                      "$w(defect)$", "$||\\phi||$", "$||w||$", "bifurcation estimate",
                      "second bifurcation estimate", "definite Criterion"});
      bool breakOnZero = Parameters::get<bool>(name_ + "->bifurcation->break on zero").value_or(false);
      bool breakOnDivergence = Parameters::get<bool>(name_ + "->bifurcation->break on divergence").value_or(false);
      double DOld = D_;
      // increase or decrease bifurcation parameter
      auto dn = finalN - initialN > 0 ? 1 : -1;
      auto previous = solutionNewton_;
      auto prePrevious = solutionNewton_;
      for (int n = initialN; n != finalN; n += dn)
        for (int i = 0; i < intermediateSteps; ++i)
        {
          D_ = std::pow(2., n + dn * i / (double) intermediateSteps);
          double h = std::pow(12 * (1 - Nu_ * Nu_) * D_ / E3d, 1. / 3.);
          E_ = E3d * h;
          double energy = calculateEnergy(solutionNewton_);
          if (std::isnan(energy))
          {
            throw std::runtime_error("initial energy is nan");
          }
          info("Initial Energy for thickness h  = {} and D = {} is {}", h, D_, energy);


          int converged = solveProblem();

          double Energy = calculateEnergy(solutionNewton_);
          info("Final energy for thickness h  = {} and D = {} is {}", h, D_, Energy);
          double bifcheck = checkBifurcation2(solutionNewton_, previous, prePrevious);
          csvWriter.tee({(double) n,h, (double) converged, Energy, valueOf(solutionNewton_,phi_)(dislocations_[0].position()),
                          valueOf(solutionNewton_,w_)(dislocations_[0].position()), norm(valueOf(solutionNewton_,phi_)),
                          norm(valueOf(solutionNewton_,w_)), checkBifurcation(solutionNewton_, previous, prePrevious),
                          bifcheck, definiteCiterion(valueOf(solutionNewton_, phi_), valueOf(solutionNewton_, w_), gv_)});

          parameterWriter.write(h);
          double tolerance = 0.1;
          if (searchBifurcation && bifcheck > tolerance && n * intermediateSteps + i >= 2)
          {
            std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++\n";
            std::cout << "Starting search for Bifurcation \n";
            auto currentSolution = solutionNewton_;
            auto extrapolation = solutionNewton_;
            axPby(extrapolation, 2., previous, -1., prePrevious);
            solutionNewton_ = extrapolation;
            int isSolution = solveProblem("Extrapolation at possible Bifurcation");
            if (isSolution == 0)
              std::cout << "Extrapolation did not provide a solution\n ";
            else{
              auto diff = currentSolution;
              axPy(diff, -1., solutionNewton_, currentSolution);
              if (norm(diff) < 1e-6)
                std::cout << "Extrapolation did not provide a new solution\n";
              else
              {
                std::cout << "!!!!!   Extrapolation did provide a new solution   !!!!";
                extrapolation = solutionNewton_;
                Parameters::set<int>(name_ + "->vary thickness->initial n", n);
                variateThickness("extrapolatedSolutionPath");
              }
              backup("extrapolatedSolutionNewton.solution", "redundant");
              // Now guess other side of bifurcation

              axPby(solutionNewton_, 2., extrapolation, -1., currentSolution);
              int guessConverged = solveProblem("bifurcationGuess");
              if (guessConverged > 0)
              {
                axPy(diff, -1., solutionNewton_, currentSolution);
                if (norm(diff) < 1e-6)
                  std::cout << " Guess did not yield a new solution\n";
                else
                {
                  backup("guessedSolutionNewton.solution", "redundant");
                  std::cout << "!!!!! guessed a bifuraction !!!!!!\n";
                  createWriter("GuessedSolution", solutionNewton_).write(h);
                  createWriter("Difference", diff).write(h);

                  Parameters::set(name_ + "->vary thickness->initial n", std::to_string(n));
                  variateThickness("newSolutionPath");

                }

              }


            }
          }
          if ((breakOnZero && (converged < 0)) || (breakOnDivergence && converged == 0))
          {
            solutionNewton_ = previous;
            solutionFlow_ = previous;
            backup("newtonLast.solution", "flowLast.solution");
            break;
          }
          else
          {
            prePrevious = previous;
            previous = solutionNewton_;
          }

        } // end parameter loop

    }

    /**
     *  \brief check if new solution is on one line with the two last solutions
     *
    *   \return  ||next - 2. * previous + preprevious||
    */
    double checkBifurcation(SolutionVector const& next, SolutionVector const& previous, SolutionVector const& preprevious)
    {
      SolutionVector diff = next;
      axPy(diff, -2., previous, preprevious);
      axPy(diff, 1., diff, next);
      return norm(diff);
    }
    /**
     *  \brief check if new solution is on one line with the two last solutions
     *
    *   \return  [next - 2. * previous + preprevious]_w (defect)
    */
    double checkBifurcation2(SolutionVector const& next, SolutionVector const& previous, SolutionVector const& preprevious)
    {
      SolutionVector diff = next;
      axPy(diff, -2., previous, preprevious);
      axPy(diff, 1., diff, next);
      return std::abs(valueOf(diff, w_)(dislocations_[0].position()));
    }

    void rotateDefect(std::string title = "rotate Defect")
    {
      checkInitialized();
      auto parameterWriter = createWriter(title, solutionNewton_);
      parameterWriter.write(0.);
      auto csvWriter = getCSVWriter(title, {"rotation","$\\Pi$","$\\phi(defect)$", "$w(defect)$", "$||\\phi||$", "$||w||$"});
      bool breakOnZero = Parameters::get<bool>(name_ + "->bifurcation->break on zero").value_or(false);
      bool breakOnDivergence = Parameters::get<bool>(name_ + "->bifurcation->break on divergence").value_or(false);

      auto finalRot = Parameters::get<double>(name_ + "->rotate defect->rotation").value();

      // increase or decrease bifurcation parameter
      int n = Parameters::get<int>(name_ + "->rotate defect->steps").value_or(10);

      double rot = finalRot * M_PI * 2 / n;
      for (int i = 0; i < n; ++i)
      {
        for (int j = 0; j < dislocation_; ++j)
        {
          if (Parameters::get<bool>(name_ + "->dislocation" + "[" + std::to_string(j) + "]->rotate").value_or(false))
            dislocations_[j].rotate(rot);
        }

        double energy = calculateEnergy(solutionNewton_);
        if (std::isnan(energy))
        {
          throw std::runtime_error("initial energy is nan");
        }
        info("Initial Energy for Rotation = {} is {}", i * rot, energy);


        int converged = solveProblem();


        double Energy = calculateEnergy(solutionNewton_);
        info("Final energy for Rotation  = {} is {}", i * rot, calculateEnergy(solutionNewton_));
        csvWriter.tee({i * rot, Energy, valueOf(solutionNewton_,phi_)({0.5 * xmax,0.5 * ymax}),
                        valueOf(solutionNewton_,w_)({0.5 * xmax,0.5 * ymax}), norm(valueOf(solutionNewton_,phi_)),
                        norm(valueOf(solutionNewton_,w_))});

        parameterWriter.write(i * rot);

        if ((breakOnZero && (converged < 0)) || (breakOnDivergence && converged == 0))
          break;
      } // end parameter loop
    }

    void moveDefect(std::string title = "move Defect")
    {
      checkInitialized();
      auto parameterWriter = createWriter(title, solutionNewton_);
      parameterWriter.write(0.);
      auto csvWriter = getCSVWriter(title, {"$shift$","$\\Pi$","$\\phi(defect)$", "$w(defect)$", "$||\\phi||$", "$||w||$"});
      bool breakOnZero = Parameters::get<bool>(name_ + "->bifurcation->break on zero").value_or(false);
      bool breakOnDivergence = Parameters::get<bool>(name_ + "->bifurcation->break on divergence").value_or(false);

      auto finalShift = Parameters::get<Dune::FieldVector<double, 2>>(name_ + "->move defect->final shift").value();
      finalShift[0] *= xmax;
      finalShift[1] *= ymax;
      finalShift -= absoluteShift_;

      // increase or decrease bifurcation parameter
      int n = Parameters::get<int>(name_ + "->move defect->steps").value_or(10);
      auto shift = finalShift * (1. / n);
      for (int i = 0; i < n + 1; ++i)
      {
        double energy = calculateEnergy(solutionNewton_);
        if (std::isnan(energy))
        {
          throw std::runtime_error("initial energy is nan");
        }
        info("Initial Energy for Shift = {} is {}", (double) i / n, energy);


        int converged = solveProblem();


        double Energy = calculateEnergy(solutionNewton_);
        info("Final energy for Shift = {} is {}", (double) i / n, calculateEnergy(solutionNewton_));
        csvWriter.tee({(double) i / n, Energy, valueOf(solutionNewton_,phi_)({0.5 * xmax,0.5 * ymax}),
                        valueOf(solutionNewton_,w_)({0.5 * xmax,0.5 * ymax}), norm(valueOf(solutionNewton_,phi_)),
                        norm(valueOf(solutionNewton_,w_))});

        parameterWriter.write((double) i / n);

        if ((breakOnZero && (converged < 0)) || (breakOnDivergence && converged == 0))
          break;

        for (int j = 0; j < dislocation_; ++j)
        {
          if (Parameters::get<bool>(name_ + "->dislocation" + "[" + std::to_string(j) + "]->move").value_or(false))
            dislocations_[j].move(shift);
        }
      } // end parameter loop

      double energy = calculateEnergy(solutionNewton_);
      if (std::isnan(energy))
      {
        throw std::runtime_error("initial energy is nan");
      }
      info("Initial Energy for Shift = {} is ", 1., energy);
      int converged = solveProblem();


      double Energy = calculateEnergy(solutionNewton_);
      info("Final energy for Shift = {} is {}", 1., calculateEnergy(solutionNewton_));
      csvWriter.tee({1., Energy, valueOf(solutionNewton_,phi_)({0.5 * xmax,0.5 * ymax}),
                      valueOf(solutionNewton_,w_)({0.5 * xmax,0.5 * ymax}), norm(valueOf(solutionNewton_,phi_)),
                      norm(valueOf(solutionNewton_,w_))});

      parameterWriter.write(1.);
    }

    void solveForRefinementLevels()
    {
      checkInitialized();
      auto const& u_h = solutionNewton_;

      auto writer = getCSVWriter("convergence", {"refinement", "$||u-u_h||$"});
      bool compareToContinuum = Parameters::get<bool>(name_ + "->compareToContinuum").value_or(false);

      auto csvWriter = getCSVWriter("convergence", {"refinement", "$h$", "$||\\phi-\\phi_h||_\\Omega$", "$||\\phi-\\phi_h||_{\\Omega /defect}$"});
      if (Lagrange) csvWriter = getCSVWriter("convergence", {"refinement", "$h$", "$||\\phi-\\phi_h||_\\Omega$", "$||\\phi-\\phi_h||_{\\Omega /defect}$","$\\lambda_\\phi$","$\\lambda_w$", "$\\int_\\Omega w$", "$\\int_\\Omega \\phi$"});


      int periodicity = Parameters::get<double>(name_ + "->evaluation->periodicity level").value_or(1);
      info("periodicity for continuum solution: {}", periodicity);
      auto sigma = evalAtQP(getContinuumStress(dislocations_, E_, periodicity));
      auto chi = getContinuumAiry(dislocations_, E_, periodicity);

      auto airySolution = makeGridFunction(chi, gv_);
      auto stressSolution = makeGridFunction(sigma, gv_);

      auto hessianAiry = makeGridFunction(gradientOf(gradientOf(valueOf(u_h, phi_))), gv_);
      auto stress = makeGridFunction(invokeAtQP(
        [&](auto const& mat) -> Dune::FieldVector<double, 3>
      {
        return {{Dune::MatVec::as_matrix(mat)[1][1],
                Dune::MatVec::as_matrix(mat)[0][0],
                -Dune::MatVec::as_matrix(mat)[0][1]}};
      }, hessianAiry), gv_);

      double cutOffRadius = Parameters::get<double>(name_ + "->evaluation->cut off radius").value_or(100);//(xmax*xmax+ ymax*ymax)/1e5
      auto tmp = getDefectIndicatorFunction(dislocations_, cutOffRadius);
      auto indicator = invokeAtQP(Operation::Minus{}, 1., tmp);

      auto airyAverage = 0.;
      Dune::FieldVector<double, 3> stressAverage = {0.,0.,0.};
      if constexpr (Lagrange)
      {
        airyAverage = integrate(airySolution * indicator, gv_, 10) / integrate(indicator, gv_, 10);
        stressAverage = integrate(stressSolution * indicator, gv_, 10) / integrate(indicator, gv_, 10);
      }
      auto airyDifference = makeGridFunction(valueOf(u_h, phi_) - airySolution + airyAverage, gv_);

      auto stressDifference = makeGridFunction(stress - stressSolution + stressAverage, gv_);


      auto vtkWriter = createWriter("flatSolution", solutionNewton_);
      vtkWriter.addVertexData(makeGridFunction(stressSolution, gv_), Dune::VTK::FieldInfo("analyticalStress", Dune::VTK::FieldInfo::Type::vector, 3));
      vtkWriter.addVertexData(stressDifference, Dune::VTK::FieldInfo("stressDifference", Dune::VTK::FieldInfo::Type::vector, 3));
      vtkWriter.addVertexData(makeGridFunction(stressDifference * indicator, gv_), Dune::VTK::FieldInfo("stressDifferenceWithoutDefectAreas", Dune::VTK::FieldInfo::Type::vector, 3));


      // for loop for uniform iterations
      for (std::size_t refine = 1; refine <= maxRefinement_; ++refine)
      {
        info("#######################  Refinement Level {}  #############################", refine);
        globalRefine(1);
        auto h = getMeshWidth(gv_);
        setInitialSolution(solutionNewton_);

        solveProblem();
        if (method_ == globalNewton || method_ == localNewton)
          solutionFlow_ = solutionNewton_;
        if (manufactured_)
        {
          auto u = get_Manufactured_Solution();

          auto u_avg = integrate(u, gv_, 10);
          auto solution = makeGridFunction(u, gv_);

          info("L2 difference to analytical solution: ");
          writer.tee({(double) refine, sqrt(integrate(unary_dot(valueOf(u_h, u_) - solution + u_avg), gv_, 10))});
        }
        else if (compareToContinuum)
        {
          stressAverage = integrate(stressSolution, gv_, 10) / area_;

          info("refine; h; L2 difference to analytical solution; Without defect neighborhoods<; Lagrange multiplier 1; 2; Lagrange condition 1; 2>");
          std::vector<double> values;
          if constexpr(Lagrange)
            values = {(double) refine, h,
            sqrt(integrate(unary_dot(stressDifference), gv_, 10)),
            sqrt(integrate(unary_dot(stressDifference) * indicator, gv_, 10)),
            probNewton_.solution(Dune::Indices::_1)(Dune::FieldVector<double, 2>{1., 1.}),
            probNewton_.solution(Dune::Indices::_2)(Dune::FieldVector<double, 2>{1., 1.}),
            integrate(probNewton_.solution(phi_), gv_,5),
            integrate(probNewton_.solution(w_), gv_,5)};
          else
          {
            values = {(double) refine, h,
              sqrt(integrate(unary_dot(stressDifference), gv_, 10)),
              sqrt(integrate(unary_dot(stressDifference) * indicator, gv_, 10))};
          }
          csvWriter.tee(values);
          vtkWriter.write(double(refine));
        }
      }
    }

  public:
    // class members
    static constexpr bool updateFormulation_ = true;
    //// instance members
    /// Problem
    Environment& env_;
    std::string name_;
    GridView gv_;
    Problem probNewton_;
    Problem probGradientFlow_;
    TP phi_;
    TP w_;
    TYPEOF(pop_back(std::declval<TP>())) u_;
    double area_;
    GradientFlow method_;
    GradientFlow secondMethod_;
    // SolutionVectors
    SolutionVector solutionNewton_;
    SolutionVector solutionFlow_;

    std::string bc_;
    //// Parameters
    double xmax = 1.;
    double ymax = 1.;
    Dune::FieldVector<double, 2> shift_ = {0.,0.};
    Dune::FieldVector<double, 2> absoluteShift_ = {0.,0.};

    /// Refinement
    std::size_t initialRefinement_ = 1;
    std::size_t maxRefinement_ = 1;
    /// Globalized Newton scheme
    std::size_t newtonMax = 20;
    double NewtonThreshold_ = 1e-9;
    double p_ = 1.;
    double rho_ = 0.5;
    // armijo scheme
    bool useArmijo_ = false;
    double beta_ = 0.5;
    double t_init_ = 1.;
    std::size_t armijoMax_ = 15;
    double gamma_ = 0.01;
    double damping_ = 0.;

    /// GradientFlow
    bool minimize_ = true;
    bool switchToNewton_ = false;
    double switchThreshold_ = 1e-4;

    // String method
    std::size_t nString_ = 10;
    double parameterString_ = 2.;
    bool useTangentials_ = true;
    bool breakString_ = true;

    double tau_ = 0.01;
    double tMax_ = 100.;
    /// mechanical
    double D_ = 0.01;
    double EStart_ = 15;
    double EEnd_ = .01;
    double EFactor_ = 0.98;
    double EShift_ = 0.;
    double Nu_ = 0.3;
    bool increasing_ = true;

    double E_ = 15;
    // calculate norm of gradient every step
    bool logGradientNorm_ = false;

    bool logEnergy_ = true;

    // use manufactured rhs
    bool manufactured_;
    /// right hand sides
    double lambda_ = 1.;
    // force term
    bool useForce_ = false;
    double constantForce_ = 1.;

    // Parameters for defect distributions
    bool loadDefectsFromFile_ = false;
    bool approxDiracByGaussian_ = true;
    double gaussianSigma_ = 0.1;
    // initial guess
    double scale_ = 0.5;
    // dislocations
    int dislocation_ = 0;
    std::vector<Dislocation> dislocations_;
    // disclinations
    int disclination_ = 0;
    std::vector<Disclination> disclinations_;
    bool reAssembleDirac_ = false;

    bool isInit_ = false;
    bool operatorsSet_ = false;
  };

  // deduction guides
  template<class Traits, class TP>
  VonKarmanProblem(Environment&, ProblemStat<Traits>, TP, TP, std::string,
    std::string)
    -> VonKarmanProblem<Traits, TP>;

  template <class Traits, class Index>
  VonKarmanProblem(Environment&, ProblemStat<Traits>, Index, std::string, std::string)
    -> VonKarmanProblem<Traits, TYPEOF(makeTreePath(std::declval<Index>(), 0))>;

  template<class Traits>
  VonKarmanProblem(Environment&, ProblemStat<Traits>, std::string,
    std::string)
    -> VonKarmanProblem<Traits, TYPEOF(makeTreePath(0))>;
} // namespace AMDiS::VonKarman
#include "AmdisVonKarman.inc.hpp"
