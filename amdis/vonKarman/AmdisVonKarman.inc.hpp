#pragma once

#include <amdis/AMDiS.hpp>
#include <amdis/vonKarman/utility.hpp>
#include <amdis/vonKarman/kdtree.hh>
#include <amdis/localoperators/FourthOrderIncompatibilityOperator.hpp>
#include <amdis/localoperators/FourthOrderLaplaceTestLaplaceTrial.hpp>
#include <dune/functions/functionspacebases/scalarbasis.hh>
#include <dune/periodic/periodicgrid.hh>
#include <amdis/C1Elements.hpp>
#include <amdis/localoperators/FirstOrderNormalGradTest.hpp>
using namespace AMDiS;

namespace AMDiS::VonKarman{

  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::readParameters()
  {

    // Parameters for loops
    bc_ = Parameters::get<std::string>(name_ + "->boundary condition").value_or("invalid");

    Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});
    xmax = upperRight[0];
    ymax = upperRight[1];

    shift_ = Parameters::get<Dune::FieldVector<double, 2>>(name_ + "->shift data").value_or(Dune::FieldVector<double, 2>{0., 0.});
    absoluteShift_ = shift_;
    absoluteShift_[0] *= upperRight[0];
    absoluteShift_[1] *= upperRight[1];

    Parameters::get(name_ + "->maxRefinement", maxRefinement_);
    Parameters::get(name_ + "->newton->maxIteration", newtonMax);
    Parameters::get(name_ + "->D", D_);
    Parameters::get(name_ + "->initialE", EStart_);
    E_ = EStart_;
    Parameters::get(name_ + "->finalE", EEnd_);
    Parameters::get(name_ + "->factorE", EFactor_);
    Parameters::get(name_ + "->shiftE", EShift_);
    Parameters::get(name_ + "->poissonsRatio", Nu_);
    Parameters::get(name_ + "->newton->damping", damping_);
    Parameters::get(name_ + "->newton->threshold", NewtonThreshold_);
    Parameters::get(name_ + "->newton->rho", rho_);
    Parameters::get(name_ + "->newton->p", p_);
    Parameters::get(name_ + "->newton->beta", beta_);
    Parameters::get(name_ + "->newton->gamma", gamma_);
    Parameters::get(name_ + "->newton->log gradient", logGradientNorm_);
    Parameters::get(name_ + "->log energy", logEnergy_);

    std::string method;
    // First method for parameter evolution
    Parameters::get(name_ + "->method", method);
    {
      if (method == "newton")
      {
        method_ = localNewton;
      }
      else if (method == "globalizedNewton")
      {
        method_ = globalNewton;
      }
      else if (method == "explicit")
      {
        method_ = explicitEuler;
      }
      else if (method == "implicit")
      {
        method_ = implicitEuler;
      }
      else if (method == "string")
      {
        method_ = stringMethod;
      }
      else if (method == "gentlest ascend" || method == "gad")
      {
        method_ = gentlestAscend;
      }
      else
      {
        error_exit("method has to be specified!");
      }
      // second method of Parameter evolution

      Parameters::get(name_ + "->second method", method);
      if (method == "newton")
      {
        secondMethod_ = localNewton;
      }
      else if (method == "globalizedNewton")
      {
        secondMethod_ = globalNewton;
      }
      else if (method == "explicit")
      {
        secondMethod_ = explicitEuler;
      }
      else if (method == "implicit")
      {
        secondMethod_ = implicitEuler;
      }
      else if (method == "gentlest ascend")
      {
        secondMethod_ = gentlestAscend;
      }
    }
    std::string stepSizeRule = "";
    Parameters::get(name_ + "->stepsizerule", stepSizeRule);
    if (stepSizeRule == "armijo")
      useArmijo_ = true;
    // TODO implement wolfe-Powell

    increasing_ = Dune::FloatCmp::gt(EFactor_, 1.);
    test_exit(increasing_ ? (EStart_ < EEnd_) : (EStart_ > EEnd_),
      "Configuration of 'initialE', 'finalE' and 'factorE' is not valid");

    // string method
    Parameters::get(name_ + "->string->images", nString_);
    Parameters::get(name_ + "->string->boundary parameter", parameterString_);
    Parameters::get(name_ + "->string->use tangentials", useTangentials_);
    Parameters::get(name_ + "->string->break: true", breakString_);
    // gradientflow parameters
    Parameters::get(name_ + "->flow->minimize", minimize_);
    Parameters::get(name_ + "->flow->tau", tau_);
    t_init_ = tau_;
    Parameters::get(name_ + "->flow->tMax", tMax_);

    Parameters::get(name_ + "->manufactured Solution", manufactured_);

    Parameters::get(name_ + "->flow->switch to newton", switchToNewton_);
    Parameters::get(name_ + "->flow->switch threshold", switchThreshold_);

    // force term
    Parameters::get(name_ + "->force", useForce_);
    // Parameters for defect distributions
    Parameters::get(name_ + "->load defects", loadDefectsFromFile_);
    Parameters::get(name_ + "->gaussian approximation", approxDiracByGaussian_);
    Parameters::get(name_ + "->gaussian->sigma", gaussianSigma_);
    // initial guess
    Parameters::get(name_ + "->initial Solution->scale", scale_);

    createDefects();

  }

  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::createDefects()
  {
    bool relativeDefectPositions = Parameters::get<bool>(name_ + "->defects->relative position").value_or(false);
    dislocations_.clear();
    disclinations_.clear();
    // always add defects to make refineAroundDefects() work
    // loadDefectsFromFile_ = Parameters::get<bool>(name_ + "->load defects").value_or(false);
    // if (loadDefectsFromFile_)
    //   return;
    // dislocations
    Parameters::get(name_ + "->dislocation", dislocation_);
    if (dislocation_)
      for (int i = 0; i < dislocation_; ++i)
        dislocations_.push_back(createDislocation(name_ + "->dislocation", i, relativeDefectPositions, absoluteShift_));
    // disclinations
    Parameters::get(name_ + "->disclination", disclination_);
    if (disclination_)
      for (int i = 0; i < disclination_; ++i)
        disclinations_.push_back(createDisclination(name_ + "->disclination", i, relativeDefectPositions, absoluteShift_));
  }



  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::refineAroundDefects()
  {
    auto refinements = Parameters::get<int>(name_ + "->local refinements").value_or(0);
    auto distance = Parameters::get<double>(name_ + "->local refinements->distance").value_or(1.);


    // fill vector with defect positions
    std::vector<Dune::FieldVector<double, 2>> coordinates;
    for (auto const& defect : dislocations_)
      coordinates.push_back(defect.position());
    for (auto const& defect : disclinations_)
      coordinates.push_back(defect.position());
    // refine locally 'refinements' times
    for (int i = 0; i < refinements; ++i)
    {
      for (auto const& e : elements(gv_))
      {
        for (auto const& p : coordinates)
          if ((e.geometry().center() - p).two_norm2() < distance * distance)
            probNewton_.grid()->mark(1, e);
      }
      AdaptInfo a{"hans"};
      probNewton_.adaptGrid(a);
    }
  }

  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::addGrowthOperators(Problem& p, int quadOrder)
  {
    std::string strainType = Parameters::get<std::string>(name_+"->growth->strain").value_or("none");
    std::string curvatureType = Parameters::get<std::string>(name_ + "->growth->curvature").value_or("none");
    // e(x,y) = ((e_xx(y),0),(0,0)) => curlTcurl e = e_xx,yy
    //e_xx(y) = b(y/ymax)^n
    if (strainType == "none")
      warn("No growth strain specified!");
    else if(strainType == "powerLaw")
    {
      auto beta = Parameters::get<double>(name_+"->growth->strain->powerLaw->beta").value_or(1.);
      auto n = Parameters::get<double>(name_ + "->growth->strain->powerLaw->exponent").value_or(1);
      auto max = Parameters::get<Dune::FieldVector<double,2>>("mesh->upper right").value_or(Dune::FieldVector<double,2>{1.,1.});
      //
      auto f = [=](auto const& x){
        auto val =  beta * n * (n-1) / max[1] / max[1] * std::pow(x[1]/max[1], n-2);
        // std::cout<<"beta: "<<beta<<" n: "<<n<<" ymax: "<<max[1]<<" value: "<<val<<std::endl;
        return val;
        };

      auto op = makeOperator(tag::test{}, -1*evalAtQP(f), quadOrder);
      p.addVectorOperator(op, phi_);
    }
    else
      error_exit("Invalid strain growth tensor");

    if(curvatureType == "none")
      warn("No curvature growth specified");
    else
      error_exit("Invalid Curvature Growth tensor!");




  }

  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::addDefectOperators(Problem& p, int quadOrder)
  {
    if (loadDefectsFromFile_)
    {
      info("Loading Defect distribution from file");
      std::string filename = Parameters::get<std::string>(name_ + "->load defects->filename").value_or("LucasData/burgerDensity.csv");
      auto defectPointCloud = -1. * evalAtQP(Dune::pointCloudFromCSV<double, 2, Dune::FieldVector<double, 2>>
        (filename, {"x","y"}, {"Bx","By"}));
      // TODO eventually add lambda for load control
      auto opDefectTerm = makeOperator(
        tag::normal_grad_test{}, defectPointCloud, quadOrder);
      p.addVectorOperator(opDefectTerm, phi_);
      reAssembleDirac_ = false;

      auto writer = getVTKWriter("rhs", gv_);
      writer.addVertexData(
        makeGridFunction(defectPointCloud, gv_),
        Dune::VTK::FieldInfo("rhs", Dune::VTK::FieldInfo::Type::scalar, 1));
      writer.write(0.);
    }
    else
    {
      // right hand side of dislocation problem
      if (dislocation_)
      {
        if (approxDiracByGaussian_)
        {
          for (auto const& dislocation : dislocations_)
          {

            auto dislocationTerm = VonKarman::Gaussian<double, 2, 1>(
              dislocation.position_, gaussianSigma_); // Gradient of Gaussian!
            Dune::FieldVector<double, 2> normalBurger = {-dislocation.burger_[1], dislocation.burger_[0]};
            auto opDislocationTerm = makeOperator(
              tag::test{},
              evalAtQP(dislocationTerm) *
              [b = normalBurger, &l = this->lambda_](auto const& x) { return b * l; },
              quadOrder);
            p.addVectorOperator(opDislocationTerm, phi_);

            auto writer = getVTKWriter("rhs", gv_);
            writer.addVertexData(
              makeGridFunction(evalAtQP(dislocationTerm) * normalBurger, gv_),
              Dune::VTK::FieldInfo("rhs", Dune::VTK::FieldInfo::Type::scalar, 1));
            writer.write(0.);
          }
          reAssembleDirac_ = false;
        }
        else
        {
          if (reAssembleDirac_)
          {
            for (auto const& disl : dislocations_)
            {
              auto normalBurger = Dune::FieldVector<double, 2>{disl.burger_[1], -disl.burger_[0]}  * lambda_;
              addDiracRhs<1>(*p.globalBasis(), *p.rhsVector(), disl.position_, normalBurger,
                phi_);
            }
          }
          reAssembleDirac_ = true;
        }
      }

      // right hand side of disclination problem
      if (disclination_)
      {
        if (approxDiracByGaussian_)
        {
          for (auto const& disc : disclinations_)
          {
            auto disclinationTerm = VonKarman::Gaussian<double, 2, 0>(disc.position_, gaussianSigma_);
            auto opDisclinationTerm = makeOperator(
              tag::test{},
              evalAtQP(disclinationTerm)*
              [&s = disc.strength_, &l = this->lambda_](auto const& x)
            { return -s * l; },
              quadOrder);
            p.addVectorOperator(opDisclinationTerm, phi_);
            reAssembleDirac_ = false;
          }
        }
        else
        {
          if (reAssembleDirac_)
          {
            for (auto const& disc : disclinations_)
            {
              addDiracRhs<0>(*p.globalBasis(), *p.rhsVector(), disc.position_,
                -disc.strength_ * lambda_, phi_);
            }
          }
          reAssembleDirac_ = true;
        }
      }
    } // else loadDefectsFromFile_

    if (Parameters::get<bool>(name_+"->growth").value_or(false))
      addGrowthOperators(p,quadOrder);

  }

  template<class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::addForceAndDefectOperators(int quadOrder)
  {

    if (manufactured_)
    {
      auto forceTerm = get_Manufactured_Force(D_);
      auto opForceTerm = makeOperator(tag::test{}, forceTerm, quadOrder);
      probNewton_.addVectorOperator(opForceTerm, w_);
      probGradientFlow_.addVectorOperator(opForceTerm, w_);

      // manufactured defect distribution
      auto defectTerm = get_Manufactured_Defect(E_);
      auto opDefectTerm = makeOperator(tag::test{}, defectTerm, quadOrder);
      probNewton_.addVectorOperator(opDefectTerm, phi_);
      probGradientFlow_.addVectorOperator(opDefectTerm, phi_);

    }
    else
    {
      // right hand side of standart problem
      if (useForce_)
      {
        auto forceTerm = [f = this->constantForce_, &lambda = this->lambda_](auto const& x)
        { return f * lambda; };
        auto opForceTerm = makeOperator(tag::test{}, forceTerm, quadOrder);
        probNewton_.addVectorOperator(opForceTerm, w_);
        probGradientFlow_.addVectorOperator(opForceTerm, w_);
      }
      addDefectOperators(probNewton_, quadOrder);
      addDefectOperators(probGradientFlow_, quadOrder);
    }
  }


  /**
   * @brief Takes a csv file with columns for stress components and displacement. Then solves for a
   * corresponding fe Function phi, w such that phi is an airys stress function for the stress and w approximates the displacment
   * Always creates a periodic Problem!
   * @tparam Traits
   * @tparam TP
   * @param x SolutionVector for result to be stored in
   * @param filename
   * @param quadOrder
   */
  template<class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::projectPointCloud(SolutionVector& x, std::string filename, int quadOrder)
  {

    auto stressPointCloud = evalAtQP(Dune::pointCloudFromCSV<double, 2, Dune::FieldMatrix<double, 2, 2>>
      (filename, {"x","y"}, {"stress_xx", "stress_xy","stress_xy","stress_yy"}));
    auto displacementPointCloud = evalAtQP(Dune::pointCloudFromCSV<double, 2, double>
      (filename, {"x","y"}, {"displacement"}));

    auto writer = getVTKWriter("displacementPointCloud", gv_);

    writer.addVertexData(
      makeGridFunction(displacementPointCloud, gv_),
      Dune::VTK::FieldInfo("displacement", Dune::VTK::FieldInfo::Type::scalar, 1));

    auto stress = makeGridFunction(invokeAtQP(
      [&](auto const& mat) -> Dune::FieldVector<double, 3>
    {
      return {{Dune::MatVec::as_matrix(mat)[0][0],
               Dune::MatVec::as_matrix(mat)[1][1],
               Dune::MatVec::as_matrix(mat)[0][1]}};
    },
      makeGridFunction(stressPointCloud, gv_)), gv_);

    writer.addVertexData(stress,
      Dune::VTK::FieldInfo("stress", Dune::VTK::FieldInfo::Type::vector, 3));

    auto continuumStress = makeGridFunction(evalAtQP(getContinuumStress(dislocations_, E_)), gv_);
    writer.addVertexData(continuumStress, Dune::VTK::FieldInfo("ContinuumStress", Dune::VTK::FieldInfo::Type::vector, 3));


    if constexpr(Lagrange)
    {
      Problem projectionProblem("Projection", probNewton_.grid());
      projectionProblem.initialize(INIT_SYSTEM, &probNewton_, INIT_ALL & ~INIT_FILEWRITER & ~INIT_SYSTEM);

      projectionProblem.addMatrixOperator(makeOperator(tag::IncompatibilityOperator{}, 1., quadOrder), phi_, phi_);
      projectionProblem.addVectorOperator(makeOperator(tag::IncompatibilityOperator{}, stressPointCloud, quadOrder), phi_);

      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1., quadOrder), w_, w_);
      projectionProblem.addVectorOperator(makeOperator(tag::test{}, displacementPointCloud, quadOrder), w_);

      auto lambdaPhi = makeTreePath(_1);
      auto lambdaW = makeTreePath(_2);
      // LHS
      // function update testet with multiplier variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), lambdaPhi, phi_);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), lambdaW, w_);
      // multiplier update testet with function variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), phi_, lambdaPhi);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), w_, lambdaW);


      assemble(projectionProblem);
      solve(projectionProblem);
      x = *projectionProblem.solutionVector();
    }
    else // Not Lagrange, but point cloud is periodic
    {
      using namespace Dune::Functions::BasisFactory;
      auto periodicGridptr = std::make_unique<Dune::Periodic::PeriodicGrid<TYPEOF(*probNewton_.grid()->hostGrid())>>(probNewton_.grid()->hostGrid(), Dune::Periodic::torus());
      ProblemStat projectionProblem("Projection", *periodicGridptr, composite(power<2>(argyris(), flatLexicographic()), scalar(), scalar(), flatLexicographic()));
      projectionProblem.initialize(INIT_ALL & ~INIT_FILEWRITER);

      auto phi = makeTreePath(_0, 0);
      auto w = makeTreePath(_0, 1);
      projectionProblem.addMatrixOperator(makeOperator(tag::IncompatibilityOperator{}, 1., quadOrder), phi, phi);
      projectionProblem.addVectorOperator(makeOperator(tag::IncompatibilityOperator{}, stressPointCloud, quadOrder), phi);

      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1., quadOrder), w, w);
      projectionProblem.addVectorOperator(makeOperator(tag::test{}, displacementPointCloud, quadOrder), w);



      auto lambdaPhi = makeTreePath(_1);
      auto lambdaW = makeTreePath(_2);
      // LHS
      // function update testet with multiplier variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), lambdaPhi, phi);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), lambdaW, w);
      // multiplier update testet with function variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), phi, lambdaPhi);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), w, lambdaW);



      AdaptInfo adaptInfo("problem");
      projectionProblem.buildAfterAdapt(adaptInfo, Flag{0}, true,true);
      projectionProblem.solve(adaptInfo);
      valueOf(x)<< projectionProblem.solution(_0);


    }
    auto hessianAiry = makeGridFunction(gradientOf(gradientOf(valueOf(x, phi_))), gv_);
    auto stressP = makeGridFunction(invokeAtQP(
      [&](auto const& mat) -> Dune::FieldVector<double, 3>
    {
      return {{Dune::MatVec::as_matrix(mat)[1][1],
               Dune::MatVec::as_matrix(mat)[0][0],
               -Dune::MatVec::as_matrix(mat)[0][1]}};
    },
      hessianAiry), gv_);

    writer.addVertexData(valueOf(x, phi_),
      Dune::VTK::FieldInfo("airyProjection", Dune::VTK::FieldInfo::Type::scalar, 1));
    writer.addVertexData(
      valueOf(x, w_),
      Dune::VTK::FieldInfo("displacementProjection", Dune::VTK::FieldInfo::Type::scalar, 1));
    writer.addVertexData(stressP,
      Dune::VTK::FieldInfo("stressProjection", Dune::VTK::FieldInfo::Type::vector, 3));
    writer.write(0.);


    // projectIntoDefectDensity(x, quadOrder);
  }


  /**
   * @brief Takes a csv file with columns for stress components and displacement. Then solves for a
   * corresponding fe Function phi, w such that phi is an airys stress function for the stress and w approximates the displacment
   * Always creates a periodic Problem!
   * @tparam Traits
   * @tparam TP
   * @param x SolutionVector for result to be stored in
   * @param filename
   * @param quadOrder
   */
  template<class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::projectContinuumSolution(SolutionVector& x, int quadOrder)
  {
    auto writer = getVTKWriter("projectionOfContinuumSolution", gv_);

    int periodicity = Parameters::get<double>(name_ + "->evaluation->periodicity level").value_or(1);

    auto stressVector = getContinuumStress(dislocations_, E_, periodicity);
    auto stressMatrix = [&v = stressVector](auto const& x)->Dune::FieldMatrix<double,2,2>{
      auto const& vec = v(x);
      return {{vec[0], vec[2]},{vec[2], vec[1]}};
    };
    auto stress = makeGridFunction(evalAtQP(stressMatrix), gv_);
    auto displacement = makeGridFunction(evalAtQP(getZeroFunction()), gv_);

    if constexpr (Lagrange)
    {
      Problem projectionProblem("Projection", probNewton_.grid());
      projectionProblem.initialize(INIT_SYSTEM, &probNewton_, INIT_ALL & ~INIT_FILEWRITER & ~INIT_SYSTEM);

      projectionProblem.addMatrixOperator(makeOperator(tag::IncompatibilityOperator{}, 1., quadOrder), phi_, phi_);
      projectionProblem.addVectorOperator(makeOperator(tag::IncompatibilityOperator{}, stress, quadOrder), phi_);

      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1., quadOrder), w_, w_);
      projectionProblem.addVectorOperator(makeOperator(tag::test{}, displacement, quadOrder), w_);

      auto lambdaPhi = makeTreePath(_1);
      auto lambdaW = makeTreePath(_2);
      // LHS
      // function update testet with multiplier variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1./area_), lambdaPhi, phi_);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1./area_), lambdaW, w_);
      // multiplier update testet with function variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1./area_), phi_, lambdaPhi);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1./area_), w_, lambdaW);


      assemble(projectionProblem);
      solve(projectionProblem);
      x = *projectionProblem.solutionVector();
    }
    else // Not Lagrange, but point cloud is periodic
    {
      using namespace Dune::Functions::BasisFactory;
      auto periodicGridptr = std::make_unique<Dune::Periodic::PeriodicGrid<TYPEOF(*probNewton_.grid()->hostGrid())>>(probNewton_.grid()->hostGrid(), Dune::Periodic::torus());
      ProblemStat projectionProblem("Projection", *periodicGridptr, composite(power<2>(argyris(), flatLexicographic()), scalar(), scalar(), flatLexicographic()));
      projectionProblem.initialize(INIT_ALL & ~INIT_FILEWRITER);

      auto phi = makeTreePath(_0, 0);
      auto w = makeTreePath(_0, 1);
      projectionProblem.addMatrixOperator(makeOperator(tag::IncompatibilityOperator{}, 1., quadOrder), phi, phi);
      projectionProblem.addVectorOperator(makeOperator(tag::IncompatibilityOperator{}, stress, quadOrder), phi);

      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1., quadOrder), w, w);
      projectionProblem.addVectorOperator(makeOperator(tag::test{}, displacement, quadOrder), w);



      auto lambdaPhi = makeTreePath(_1);
      auto lambdaW = makeTreePath(_2);
      // LHS
      // function update testet with multiplier variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / area_), lambdaPhi, phi);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / area_), lambdaW, w);
      // multiplier update testet with function variation
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / area_), phi, lambdaPhi);
      projectionProblem.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / area_), w, lambdaW);



      AdaptInfo adaptInfo("problem");
      projectionProblem.buildAfterAdapt(adaptInfo, Flag{0}, true, true);
      projectionProblem.solve(adaptInfo);
      valueOf(x) << projectionProblem.solution(_0);


    }
    // In both cases x has been written to
    // write files
    auto hessianAiry = makeGridFunction(gradientOf(gradientOf(valueOf(x, phi_))), gv_);
    auto stressP = makeGridFunction(invokeAtQP(
      [&](auto const& mat) -> Dune::FieldVector<double, 3>
    {
      return {{Dune::MatVec::as_matrix(mat)[1][1],
               Dune::MatVec::as_matrix(mat)[0][0],
               -Dune::MatVec::as_matrix(mat)[0][1]}};
    },
      hessianAiry), gv_);

    writer.addVertexData(valueOf(x, phi_),
      Dune::VTK::FieldInfo("airyProjection", Dune::VTK::FieldInfo::Type::scalar, 1));
    writer.addVertexData(
      valueOf(x, w_),
      Dune::VTK::FieldInfo("displacementProjection", Dune::VTK::FieldInfo::Type::scalar, 1));
    writer.addVertexData(stressP,
      Dune::VTK::FieldInfo("stressProjection", Dune::VTK::FieldInfo::Type::vector, 3));
    writer.addVertexData(makeGridFunction(evalAtQP(stressVector) - stressP, gv_), Dune::VTK::FieldInfo("Difference", Dune::VTK::FieldInfo::Type::vector,3));
    writer.write(0.);

   }

  /**
   * @brief Takes a SolutionVector x and calculates the corresponding rhs density to the VK eq

   * @tparam Traits
   * @tparam TP
   * @param x
   */
  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::projectIntoDefectDensity(SolutionVector const& x, int quadOrder)
  {
    using namespace Dune::Functions::BasisFactory;

    ProblemStat p("Projection", *probNewton_.grid(), composite(argyris(), scalar(), flatLexicographic()));
    p.initialize(INIT_ALL & ~INIT_FILEWRITER);

    p.addMatrixOperator(makeOperator(tag::test_trial{}, E_, quadOrder), _0, _0);
    p.addVectorOperator(makeOperator(tag::laplacetest_laplacetrial{}, valueOf(x, phi_), quadOrder), _0);
    p.addVectorOperator(makeOperator(tag::test_trial{},
      E_* MongeAmpereBracket(valueOf(x, w_), valueOf(x, w_))
      , quadOrder), _0);


    if constexpr (Lagrange)
    {
      // LHS
      // function update testet with multiplier variation
      p.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), _1, _0);
      // multiplier update testet with function variation
      p.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), _0, _1);
    }
    else
    {
      // error_exit("bc for point cloud projection not implemented");
    }

    Dune::Timer timer;
    AdaptInfo adaptInfo("problem");
    p.buildAfterAdapt(adaptInfo, Flag{0}, true, true);
    info("Total assembling took {} seconds", timer.elapsed());

    p.solve(adaptInfo);
    info("Solving System took {} seconds", timer.elapsed());


    auto writer = getVTKWriter("reconstructedBurgersDensity", gv_);

    writer.addVertexData(
      p.solution(_0),
      Dune::VTK::FieldInfo("burgersDensity", Dune::VTK::FieldInfo::Type::scalar, 1));

    writer.write(0.);

  }

  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::setInitialSolution(SolutionVector& x)
  {
    Dune::Timer timer;
    // TODO read some parameters
    std::string type = Parameters::get<std::string>(name_ + "->initial Solution").value_or("null");
    setInitialSolution(x, type);
    info("Setting initial solution took {} seconds", timer.elapsed());

  }

  template <class Traits, class TP>
  void VonKarmanProblem<Traits, TP>::setInitialSolution(SolutionVector& x, std::string type)
  {

    if (type == "restore")
    {
      restore();
      x = solutionNewton_;
    }
    else if (type == "points")
    {
      std::string filename = Parameters::get<std::string>
        (name_ + "->initial Solution->points->file").value_or("LucasData/pointsWithData.csv");
      int quad = Parameters::get<int>(name_ + "quad").value_or(5);
      projectPointCloud(x, filename, quad);
    }
    else if (type == "polynomial")
    {
      auto gf = makeGridFunction(evalAtQP(get_4thOrder_Polynomial(scale_)), gv_);
      valueOf(x, phi_) << gf;
      valueOf(x, w_) << gf;
    }
    else if (type == "constant")
    {
      auto gf = makeGridFunction(evalAtQP(get_Constant(scale_)), gv_);
      valueOf(x, phi_) << gf;
      valueOf(x, w_) << gf;
    }
    else if (type == "all Dofs")
    {
      auto gf = makeGridFunction(evalAtQP(getInitialGuess_AllDofs(scale_)), gv_);
      valueOf(x, phi_) << gf;
      valueOf(x, w_) << gf;
    }
    else if (type == "null")
    {
      auto gf = makeGridFunction(evalAtQP(getZeroVectorFunction()), gv_);
      valueOf(x, u_) << gf;
    }
    else if (type == "sin")
    {
      bool xDir = Parameters::get<bool>(name_ + "->initial Solution->sin->x").value_or(false);
      bool yDir = Parameters::get<bool>(name_ + "->initial Solution->sin->y").value_or(false);

      auto stretch = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});
      if (Parameters::get<bool>(name_ + "->growth").value_or(false))
        stretch = Dune::FieldVector<double, 2>{1., Parameters::get<double>(name_ + "->initial Solution->sin->stretch").value_or(1.)};
      auto gf = makeGridFunction(evalAtQP(get_Sum_Sin(scale_, xDir, yDir, absoluteShift_, stretch)), gv_);
      valueOf(x, phi_) << gf;
      valueOf(x, w_) << gf;
    }
    else if (type == "sin2")
    {
      bool xDir = Parameters::get<bool>(name_ + "->initial Solution->sin->x").value_or(false);
      bool yDir = Parameters::get<bool>(name_ + "->initial Solution->sin->y").value_or(false);
      auto gf = makeGridFunction(evalAtQP(get_Sin2(scale_, xDir, yDir)), gv_);

      valueOf(x, phi_) << gf;
      valueOf(x, w_) << gf;

    }
    else if (type == "sin2sin2")
    {
      auto gf = makeGridFunction(evalAtQP(get_Sin2Sin2(scale_)), gv_);
      valueOf(x, phi_) << gf;
      valueOf(x, w_) << gf;
    }
    else if (type == "defect")
    {
      projectDefectDistribution(x, scale_); // Riesz projection of (scaled) defect rhs into x
      valueOf(x, w_) << valueOf(x, phi_); // defects are only on airy rhs, use it for displacement too
    }
    else if (type == "manufactured")
    {
      assert(manufactured_);
      valueOf(x, u_) << get_Manufactured_Solution(
        Parameters::get<bool>("problem->initial Solution->perturbed").value_or(false),
        Parameters::get<double>("problem->initial Solution->perturbation Scale").value_or(0.));

    }
    else if (type == "minima")
      findZeroSolution(x);
    else
      error_exit("Invalid Initial Solution " + type);

    // modify initial solution to obey hom. boundary conditions
    if (bc_ == "clamped"){
      if (Parameters::get<bool>(name_ + "->compareToContinuum").value_or(false))
      {
        info("Applying inhomogeneous clamped BC");
        // int periodicity = Parameters::get<double>(name_ + "->evaluation->periodicity level").value_or(1);
        // auto sigma = getContinuumStress(dislocations_, E_, periodicity);
        // auto hessianAiry = [&sigma](auto const& x){
        //   auto fieldVec = sigma(x);
        //   return Dune::FieldMatrix<double, 2, 2>{{fieldVec[1], -fieldVec[2]}, {-fieldVec[2], fieldVec[0]}};
        // };
        // auto phi = Dune::Functions::makeDifferentiableFunctionFromCallables(
        //   Dune::Functions::SignatureTag<Dune::FieldVector<double, 1>(Dune::FieldVector<double, 2>),
        //   JacobianDerivativeTraits>(),
        //   [=](auto const& x) -> FieldVector<double, 1> { return 0.; },
        //   [=](auto const& x) -> FieldVector<double, 2> { return FieldVector<double, 2>{0, 0};},
        //   hessianAiry);


        // applyClampedBCToFunction(x, phi_, makeGridFunction(phi, gv_));
        // applyClampedBCToFunction(x,w_, makeGridFunction(getZeroFunction(), gv_));

        SolutionVector analyticSolution = x;
        projectContinuumSolution(analyticSolution, 10);
        applyClampedBCToFunction(x, u_, valueOf(analyticSolution, u_));

      }
      else if (Parameters::get<bool>(name_ + "->manufactured Solution").value_or(false))
      {
        info("Applying inhomogeneous clamped BC");
        applyClampedBCToFunction(x, u_, makeGridFunction(get_Manufactured_Solution(), gv_));
      }
      else if (type == "points")
        info("Applying inhomogeneous clamped BC");
      else
      {
        applyClampedBCToFunction(x, phi_);
        applyClampedBCToFunction(x, w_);

      }
    }
    else if(bc_ == "free")
    {
      applyClampedBCToFunction(x,phi_);
    }
    else if (bc_ == "mixed")
    {
      applyClampedBCToFunction(x, phi_);
      applyClampedBCToFunction(x, w_); //TODO introduce clamped boundary Ids here
    }
    if constexpr (Lagrange)
    {
      valueOf(x, makeTreePath(_1)) << 1;
      valueOf(x, makeTreePath(_2)) << 1;

      SolutionVector tmp = makeDOFVector(*probGradientFlow_.globalBasis());
      valueOf(tmp, u_) << integrate(valueOf(x, u_), gv_, 5);
      valueOf(tmp, makeTreePath(_1)) << 1;
      valueOf(tmp, makeTreePath(_2)) << 1;
      axPy(x, -1., tmp, x);
    }
    x.finish();
  }// setInitialSolution(...)
} // namespace AMDiS::VonKarman
