#pragma once
#include <amdis/AMDiS.hpp>

#include "amdis/gridfunctions/AnalyticGridFunction.hpp"
#include "amdis/operations/Basic.hpp"
#include "dune/common/fmatrix.hh"
#include "dune/localfunctions/utility/defaultbasisfactory.hh"
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/functions/common/differentiablefunctionfromcallables.hh>
#include <dune/functions/functionspacebases/sizeinfo.hh>

#include <amdis/ClampedBC.hpp>
#include <amdis/PointwiseConstraint.hpp>

#include <amdis/AdaptStationary.hpp>
#include <amdis/io/VTKSequenceWriter.hpp>

#include <amdis/vonKarman/utility.hpp>
#include <amdis/vonKarman/Defect.hpp>
#include <amdis/vonKarman/MatMult.hpp>
#include <amdis/vonKarman/kdtree.hh>
#include <amdis/localoperators/FourthOrderLaplaceTestLaplaceTrial.hpp>
#include <amdis/localoperators/FourthOrderClampedPlateOperator.hpp>

#include <amdis/localoperators/GenericVariableOrderOperator.hpp>
using namespace AMDiS;
using namespace AMDiS::VonKarman;

template <class Traits, class TP>
class FlatProblem : public Experiment
{
  public:
  using Problem = ProblemStat<Traits>;
  using SolutionVector = typename Problem::SolutionVector;
  using GridView = typename Problem::GridView;
  using GlobalBasis = typename Problem::GlobalBasis;
  static constexpr int NormOrder = 2;


  // if we have a Lagrange condition, then TP is of type makeTreePath(_i, int)
  static constexpr bool Lagrange = not std::is_same_v<TYPEOF(makeTreePath()), AMDiS::remove_cvref_t<TP>>;

public:
  FlatProblem() = delete;

  //Base constructor
  FlatProblem(Environment& e, Problem const& p, TP tpPhi, std::string name ,
    std::string directory)
    : Experiment(directory), env_(e), name_(name), gv_(p.gridView()), p_(p),
    phi_(tpPhi)
  {
    // Parameters
    readParameters();
    refineAroundDefects();
    p_.initialize(INIT_ALL & ~INIT_FILEWRITER);
    area_ = integrate(1., gv_, 1);
  }

  // Constructor without an index to prefix stress solutions
  FlatProblem(Environment& e, Problem const& p, std::string name,
    std::string directory )
    :FlatProblem(e, p, makeTreePath(), name, directory)
  {}

  auto createWriter(std::string file)
  {
    auto writer = getVTKWriter(file, gv_);

    auto hessianAiry = makeGridFunction(gradientOf(gradientOf(p_.solution(phi_))), gv_);
    auto stress = makeGridFunction(invokeAtQP(
      [&](auto const& mat) -> Dune::FieldVector<double, 3>
    {
      return {{Dune::MatVec::as_matrix(mat)[1][1],
               Dune::MatVec::as_matrix(mat)[0][0],
               -Dune::MatVec::as_matrix(mat)[0][1]}};
    },
      hessianAiry), gv_);

    writer.addVertexData(p_.solution(phi_),
      Dune::VTK::FieldInfo("airy", Dune::VTK::FieldInfo::Type::scalar, 1));

    writer.addVertexData(stress,
      Dune::VTK::FieldInfo("stress", Dune::VTK::FieldInfo::Type::vector, 3));
    return writer;
  }

  void initialize()
  {
    if (isInit_)
      return;
    else
    {
      // add operators
      addOperators(Parameters::get<int>(name_ + "->quad").value_or(5));
      addBoundaryConditions();
      isInit_ = true;
    }
  }

  void checkInitialized()
  {
    if (!isInit_)
      error_exit("Problem not initialized!");
  }

  void readParameters()
  {
    // Parameters for loops
    bc_ = Parameters::get<std::string>(name_ + "->boundary condition").value_or("invalid");

    Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});
    xmax = upperRight[0];
    ymax = upperRight[1];

    shift_ = Parameters::get<Dune::FieldVector<double, 2>>(name_ + "->shift data").value_or(Dune::FieldVector<double, 2>{0., 0.});
    absoluteShift_ = shift_;
    absoluteShift_[0] *= upperRight[0];
    absoluteShift_[1] *= upperRight[1];

    Parameters::get(name_ + "->maxRefinement", maxRefinement_);
    Parameters::get(name_ + "->initialE", E_);
    Parameters::get(name_ + "->poissonsRatio", Nu_);
    // Parameters for defect distributions
    Parameters::get(name_ + "->load defects", loadDefectsFromFile_);
    Parameters::get(name_ + "->gaussian approximation", approxDiracByGaussian_);
    Parameters::get(name_ + "->gaussian->sigma", gaussianSigma_);

    createDefects();
  }

  void createDefects()
  {
    bool relativeDefectPositions = Parameters::get<bool>(name_ + "->defects->relative position").value_or(false);
    dislocations_.clear();
    disclinations_.clear();
    // always add defects to make refineAroundDefects() work
    // loadDefectsFromFile_ = Parameters::get<bool>(name_ + "->load defects").value_or(false);
    // if (loadDefectsFromFile_)
    //   return;
    // dislocations
    Parameters::get(name_ + "->dislocation", dislocation_);
    if (dislocation_)
      for (int i = 0; i < dislocation_; ++i)
        dislocations_.push_back(createDislocation(name_ + "->dislocation", i, relativeDefectPositions, absoluteShift_));
    // disclinations
    Parameters::get(name_ + "->disclination", disclination_);
    if (disclination_)
      for (int i = 0; i < disclination_; ++i)
        disclinations_.push_back(createDisclination(name_ + "->disclination", i, relativeDefectPositions, absoluteShift_));
  }

  void refineAroundDefects()
  {
    auto refinements = Parameters::get<int>(name_ + "->local refinements").value_or(0);
    auto distance = Parameters::get<double>(name_ + "->local refinements->distance").value_or(1.);
    // fill vector with defect positions
    std::vector<Dune::FieldVector<double, 2>> coordinates;
    for (auto const& defect : dislocations_)
      coordinates.push_back(defect.position());
    for (auto const& defect : disclinations_)
      coordinates.push_back(defect.position());
    // refine locally 'refinements' times
    for (int i = 0; i < refinements; ++i)
    {
      for (auto const& e : elements(gv_))
      {
        for (auto const& p : coordinates)
          if ((e.geometry().center() - p).two_norm2() < distance * distance)
            p_.grid()->mark(1, e);
      }
      AdaptInfo a{"hans"};
      p_.adaptGrid(a);
    }
  }

  virtual void addOperatorsImpl(int quadOrder)
  {
    // auto opLaplace = makeOperator(tag::laplacetest_laplacetrial{}, 1. / E_,
    // quadOrder);
    auto lhs = [](auto && hessianCol, auto && hessianRow, auto&& factor){
      return (hessianCol[0][0] + hessianCol[1][1])*(hessianRow[0][0] +
      hessianRow[1][1]) * factor; };

    auto rhs = [](auto... args) { return 0.; };
    auto opLaplace =
        genericOperator<2>(10, lhs, rhs, Operation::Constant(E_));
    p_.addMatrixOperator(opLaplace, phi_, phi_);
  }

  void addOperators(int quadOrder = 5) {
    if (operatorsSet_)
      error_exit("Adding Operators twice");
    else
    {
      addOperatorsImpl(quadOrder);
      addDefectOperators(p_, quadOrder + 3);
      operatorsSet_ = true;
    }
    }

  void addDefectOperators(Problem& p, int quadOrder = 5)
  {
    if (loadDefectsFromFile_)
    {
      info("Loading Defect distribution from file");
      std::string filename = Parameters::get<std::string>(name_ + "->load defects->filename").value_or("LucasData/burgerDensity.csv");
      auto defectPointCloud = evalAtQP(Dune::pointCloudFromCSV<double, 2, double>
        (filename, {"x","y"}, {"diff"}));
      // TODO eventually add lambda for load control
      auto opDefectTerm = makeOperator(
        tag::test{}, defectPointCloud, quadOrder);
      p.addVectorOperator(opDefectTerm, phi_);
      reAssembleDirac_ = false;

      auto writer = getVTKWriter("rhs", gv_);
      writer.addVertexData(
        makeGridFunction(defectPointCloud, gv_),
        Dune::VTK::FieldInfo("rhs", Dune::VTK::FieldInfo::Type::scalar, 1));
      writer.write(0.);
    }
    else
    {
      // right hand side of dislocation problem
      if (dislocation_)
      {
        if (approxDiracByGaussian_)
        {
          for (auto const& dislocation : dislocations_)
          {

            auto dislocationTerm = VonKarman::Gaussian<double, 2, 1>(
              dislocation.position_, gaussianSigma_); // Gradient of Gaussian!
            Dune::FieldVector<double, 2> normalBurger = {-dislocation.burger_[1], dislocation.burger_[0]};
            auto opDislocationTerm = makeOperator(
              tag::test{},
              evalAtQP(dislocationTerm)* normalBurger,
              quadOrder);
            p.addVectorOperator(opDislocationTerm, phi_);

            auto writer = getVTKWriter("rhs", gv_);
            writer.addVertexData(
              makeGridFunction(evalAtQP(dislocationTerm) * -normalBurger, gv_),
              Dune::VTK::FieldInfo("rhs", Dune::VTK::FieldInfo::Type::scalar, 1));
            writer.write(0.);
          }
          reAssembleDirac_ = false;
        }
        else
        {
          if (reAssembleDirac_)
          {
            for (auto const& disl : dislocations_)
            {
              auto normalBurger = Dune::FieldVector<double, 2>{-disl.burger_[1], disl.burger_[0]};
              addDiracRhs<1>(*p.globalBasis(), *p.rhsVector(), disl.position_, normalBurger,
                phi_);
            }
          }
          reAssembleDirac_ = true;
        }
      }

      // right hand side of disclination problem
      if (disclination_)
      {
        if (approxDiracByGaussian_)
        {
          for (auto const& disc : disclinations_)
          {
            auto disclinationTerm = VonKarman::Gaussian<double, 2, 0>(disc.position_, gaussianSigma_);
            auto opDisclinationTerm = makeOperator(
              tag::test{},
              evalAtQP(disclinationTerm)*
              [&s = disc.strength_, &l = this->lambda_](auto const& x)
            { return -s * l; },
              quadOrder);
            p.addVectorOperator(opDisclinationTerm, phi_);
            reAssembleDirac_ = false;
          }
        }
        else
        {
          if (reAssembleDirac_)
          {
            for (auto const& disc : disclinations_)
            {
              addDiracRhs<0>(*p.globalBasis(), *p.rhsVector(), disc.position_,
                -disc.strength_ * lambda_, phi_);
            }
          }
          reAssembleDirac_ = true;
        }
      }
    } // else loadDefectsFromFile_
  }

  /**
   * @brief
   *        Currently only homogeneous clamped and periodic BCs are implemented
   *        TODO implement free bc
   */
  void addBoundaryConditions()
  {
    // TODO add different Boundary Conditions
    //  Homogeneous Clamped BC on both solutions
    if (bc_ == "clamped")
    {
      p_.boundaryManager()->setBoxBoundary({1, 1, 1, 1});
      auto zeroGridFct = makeGridFunction(evalAtQP(getZeroFunction()), p_.gridView());
      auto clampedBC = ClampedBC{p_.globalBasis(),
                               phi_,phi_,
                               {*(p_.boundaryManager()), BoundaryType{1}},
                               zeroGridFct};
      p_.addConstraint(clampedBC);
    }
    else if (bc_ == "free")
    {
      error_exit("Free Boundary conditions are currently not implemented!");
    }
    else if (bc_ == "lagrange")
    {
      if constexpr (Lagrange)
      {
        auto lambda = makeTreePath(Dune::Indices::_1);
        // function update testet with multiplier variation
        p_.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / area_), lambda, phi_);
        // multiplier update testet with function variation
        p_.addMatrixOperator(makeOperator(tag::test_trial{}, 1. / area_), phi_, lambda);
      }
      else
        error_exit("Lagrange conditions need a corresponding basis in the basis tree and a fitting treepath argument");
    } else if (bc_ == "complex") {
      if constexpr (Lagrange) {
        auto lambda = makeTreePath(Dune::Indices::_1);
        // multiplier test with multiplier variation
        p_.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), lambda,
                             lambda);
        // function update testet with multiplier variation
        p_.addMatrixOperator(makeOperator(tag::test_trial{}, -1.),
                             lambda, phi_);
        // multiplier update testet with function variation
        p_.addMatrixOperator(makeOperator(tag::test_trial{}, 1.), phi_,
                             lambda);
      } else
        error_exit("Complex conditions need a corresponding basis in the "
                   "basis tree and a fitting treepath argument");
    } else if (bc_ == "none")
      warning("Solving problem without boundary conditions!");
    else if (bc_ == "invalid")
      error_exit("Must specify boundary conditions!");
    else
      error_exit("Invalid boundary condition " + bc_ + " given in initfile");
  }

  void assemble(Problem& p, bool assembleMatrix = true, bool assembleRhs = true)
  {
    Dune::Timer timer;
    AdaptInfo adaptInfo("problem");
    p.buildAfterAdapt(adaptInfo, Flag{0}, assembleMatrix, assembleRhs);
    if (reAssembleDirac_)
    {
      addDefectOperators(p);
    }
    info("Total assembling took {} seconds", timer.elapsed());
  }

  void solve(Problem& p)
  {
    info("Solving Linear System of Problem {}", p.name());
    p.rhsVector()->finish();
    Dune::Timer timer;
    AdaptInfo adaptInfo("problem");
    p.solve(adaptInfo);
    info("Solving System took {} seconds", timer.elapsed());

  }

  void solveProblem()
  {
    assemble(p_);
    solve(p_);
    auto writer = createWriter("StressSolution");
    writer.write(0.);
  }

  void globalRefine(std::size_t i)
  {
    p_.globalRefine(i);
    gv_ = p_.gridView();
    area_ = integrate(1., gv_, 1);
    // assemble(p_, true, true);

  }

  void solveForRefinementLevels()
  {
    checkInitialized();

    auto const& u_h = *p_.solutionVector();
    std::vector<std::string> csvHeader = {
      "refinement",
      "h",
      "$||u-u_h||_\\Omega$",
      "$||u-u_h||_{\\Omega /defect}$"};
      if constexpr(Lagrange)
        csvHeader.push_back("$\\lambda$");
      csvHeader.push_back("$\\int_\\Omega u$");

    auto csvWriter = getCSVWriter("convergence", csvHeader);
    bool compareToContinuum = Parameters::get<bool>(name_ + "->compareToContinuum").value_or(false);

    int periodicity = Parameters::get<double>(name_ + "->evaluation->periodicity level").value_or(1);
    info("periodicity for continuum solution: {}", periodicity);
    auto sigma = evalAtQP(getContinuumStress(dislocations_, E_, periodicity));
    auto chi = getContinuumAiry(dislocations_, E_, periodicity);

    auto airySolution = makeGridFunction(chi, gv_);
    auto stressSolution = makeGridFunction(sigma, gv_);

    auto hessianAiry = makeGridFunction(gradientOf(gradientOf(valueOf(u_h, phi_))), gv_);
    auto stress = makeGridFunction(invokeAtQP(
      [&](auto const& mat) -> Dune::FieldVector<double, 3>
    {
      return {{Dune::MatVec::as_matrix(mat)[1][1],
              Dune::MatVec::as_matrix(mat)[0][0],
              -Dune::MatVec::as_matrix(mat)[0][1]}};
    }, hessianAiry), gv_);

    double cutOffRadius = Parameters::get<double>(name_+"->evaluation->cut off radius").value_or(100);//(xmax*xmax+ ymax*ymax)/1e5
    auto tmp  =getDefectIndicatorFunction(dislocations_, cutOffRadius);
    auto indicator = invokeAtQP(Operation::Minus{}, 1., tmp);

    auto airyAverage = integrate(airySolution * indicator, gv_, 10) / integrate(indicator, gv_, 10);
    auto airyDifference = makeGridFunction(valueOf(u_h, phi_) - airySolution + airyAverage, gv_);

    auto stressAverage = integrate(stressSolution * indicator, gv_, 10) / integrate(indicator, gv_, 10);
    auto stressDifference = makeGridFunction(stress - stressSolution, gv_);
    auto stressDifferenceAverage = makeGridFunction(stress - stressSolution + stressAverage, gv_);

    auto vtkWriter = createWriter("flatSolution");
    vtkWriter.addVertexData(makeGridFunction(stressSolution, gv_), Dune::VTK::FieldInfo("analyticalStress", Dune::VTK::FieldInfo::Type::vector, 3));
    vtkWriter.addVertexData(stressDifference, Dune::VTK::FieldInfo("stressDifference", Dune::VTK::FieldInfo::Type::vector, 3));
    vtkWriter.addVertexData(
        stressDifferenceAverage,
        Dune::VTK::FieldInfo("stressDifferenceAverage", Dune::VTK::FieldInfo::Type::vector, 3));

    vtkWriter.addVertexData(makeGridFunction(stressDifference * indicator, gv_), Dune::VTK::FieldInfo("stressDifferenceWithoutDefectAreas", Dune::VTK::FieldInfo::Type::vector, 3));
    vtkWriter.addVertexData(makeGridFunction(stressDifferenceAverage * indicator, gv_),
                            Dune::VTK::FieldInfo("stressDifferenceAverageWithoutDefectAreas",
                                                 Dune::VTK::FieldInfo::Type::vector, 3));

    // vtkWriter.addVertexData(makeGridFunction(airySolution, gv_), Dune::VTK::FieldInfo("analyticalAiry", Dune::VTK::FieldInfo::Type::scalar, 1));
    // vtkWriter.addVertexData(airyDifference, Dune::VTK::FieldInfo("airyDifference", Dune::VTK::FieldInfo::Type::scalar, 1));
    // vtkWriter.addVertexData(makeGridFunction(airyDifference * indicator, gv_), Dune::VTK::FieldInfo("airyDifferenceWithoutDefectAreas", Dune::VTK::FieldInfo::Type::scalar, 1));

    // for loop for uniform iterations
    for (std::size_t refine = 1; refine <= maxRefinement_; ++refine)
    {
      info("#######################  Refinement Level {}  #############################", refine);
      globalRefine(1);
      auto h = getMeshWidth(gv_);

      solveProblem();
      if (compareToContinuum)
      {
        stressAverage = integrate(stressSolution, gv_, 10) / area_;

        info("L2 difference to analytical solution; Without defect neighborhoods; Lagrange multiplier; Lagrange condition ");
        std::vector<double> values{(double) refine, h,
          sqrt(integrate(unary_dot(stressDifference), gv_, 10)),
          sqrt(integrate(unary_dot(stressDifference)*  indicator, gv_, 10))};
          if constexpr(Lagrange)
            values.push_back(p_.solution(Dune::Indices::_1)(Dune::FieldVector<double, 2>{1., 1.}));
          values.push_back(integrate(p_.solution(phi_), gv_));
        csvWriter.tee(values);
        vtkWriter.write(double(refine));
      }
    }
  }

protected:
  Environment& env_;
  std::string name_;
  GridView gv_;
  Problem p_;
  double area_ = 1.;
  double E_ = 1.;
  double Nu_ = 0.25;
  TP phi_;
  std::string bc_;
  //// Parameters
  double xmax = 1.;
  double ymax = 1.;
  Dune::FieldVector<double, 2> shift_ = {0.,0.};
  Dune::FieldVector<double, 2> absoluteShift_ = {0.,0.};

  /// Refinement
  std::size_t initialRefinement_ = 1;
  std::size_t maxRefinement_ = 1;

  double lambda_ = 1.;
  // Parameters for defect distributions
  bool loadDefectsFromFile_ = false;
  bool approxDiracByGaussian_ = true;
  double gaussianSigma_ = 0.1;

  // dislocations
  int dislocation_ = 0;
  std::vector<Dislocation> dislocations_;
  // disclinations
  int disclination_ = 0;
  std::vector<Disclination> disclinations_;
  bool reAssembleDirac_ = false;

  bool isInit_ = false;
  bool operatorsSet_ = false;
};



template <class Traits, class Index>
FlatProblem(Environment&, ProblemStat<Traits>, Index, std::string, std::string)
-> FlatProblem<Traits, TYPEOF(makeTreePath(std::declval<Index>()))>;

template<class Traits>
FlatProblem(Environment&, ProblemStat<Traits>, std::string, std::string)
    -> FlatProblem<Traits, TYPEOF(makeTreePath())>;

template<class Traits, class TP>
void run(FlatProblem<Traits, TP>& p)
{
  p.initialize();
  p.solveForRefinementLevels();
}