#include <amdis/AMDiS.hpp>

#include <dune/grid/uggrid.hh>

#include <amdis/localoperators/FourthOrderVonKarmanTest.hpp>
#include <amdis/localoperators/FourthOrderVonKarmanTestTrial.hpp>

#include <dune/functions/functionspacebases/argyrisbasis.hh>
#include <dune/functions/functionspacebases/morleybasis.hh>

#include <dune/functions/common/differentiablefunctionfromcallables.hh>

#include <dune/grid/utility/parmetisgridpartitioner.hh>

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/gmshwriter.hh>

#include <amdis/ClampedBC.hpp>

#include <amdis/AdaptStationary.hpp>
#include <amdis/io/VTKWriter.hpp>
#include <amdis/vonKarman/AmdisVonKarman.hpp>
#include <dune/common/test/testsuite.hh>

using namespace AMDiS;

template <class K = double, int n = 2>
auto testIntegral(auto gv, int quad = 10)
{
  Dune::TestSuite test("integral over " + std::to_string(n) + "dimensional gridView");

  Dune::FieldVector<K, n> x(0.5);
  for (double eps = 1; eps > 1e-10; eps *= 0.1)
  {
    double integral = integrate(VonKarman::Gaussian<K, n>(x, eps), gv, quad);
    std::cout << "Integral " << integral << std::endl;
    test.check(std::abs(1. - integral) < 1e-10,
               "with eps = " + std::to_string(eps) + " with quad order " + std::to_string(quad))
        << integral << " != 1";
  }
  return test;
}

int main(int argc, char **argv)
{
  Environment env(argc, argv);
  using namespace Dune::Functions::BasisFactory;

  Dune::MPIHelper &mpiHelper = env.mpiHelper();
  static constexpr int dim = 2;

  std::size_t initialRefinement = 5;

  using Grid = Dune::UGGrid<dim>;

  std::unique_ptr<Grid> gridptr;
  Dune::GmshReader<Grid> reader;
  gridptr = reader.read("testmesh.msh");

  gridptr->globalRefine(initialRefinement);
  auto gv = gridptr->leafGridView();

  std::vector<unsigned int> part
      = Dune::ParMetisGridPartitioner<decltype(gv)>::partition(gv, mpiHelper);
  bool hasChanged = gridptr->loadBalance();

  Dune::TestSuite test("Implementation of Gaussian Function");
  test.subTest(testIntegral(gridptr->leafGridView()));
  test.exit();
}