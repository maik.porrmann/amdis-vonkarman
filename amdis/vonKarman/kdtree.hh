#pragma once

#include <cstdlib>
#include <iostream>

// #include <nanoflann.hpp>
#include<libs/nanoflann/include/nanoflann.hpp>

#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/common/float_cmp.hh>

namespace Dune {

  /// \brief A simple vector-of-vectors adaptor for nanoflann, without duplicating the storage.
  template <class T, int dim>
  class KDTree
  {
  public:
    using WorldVector = FieldVector<T, dim>;

    /// \brief Type of the container to store the points
    using VectorOfVectorsType = std::vector<WorldVector>;

    /// \brief The type of the point coordinates (typically, double or float).
    using num_t = T;

    /// \brief The distance metric to use: nanoflann::metric_L1, nanoflann::metric_L2, nanoflann::metric_L2_Simple, etc.
    using Distance = nanoflann::metric_L2;

    /// \brief The type for indices in the KD-tree index (typically, size_t of int)
    using IndexType = std::size_t;

    /// \brief If set to >0, it specifies a compile-time fixed dimensionality for the points in the data set, allowing more compiler optimizations.
    static constexpr int DIM = WorldVector::size();

  private:
    using self_t = KDTree;
    using metric_t = typename Distance::template traits<num_t, self_t>::distance_t;
    using index_t = nanoflann::KDTreeSingleIndexAdaptor<metric_t, self_t, DIM, IndexType>;

  private:
    /// \brief The kd-tree index for the user to call its methods as usual with any other FLANN index.
    std::unique_ptr<index_t> index_;

    /// \brief The container storing all the points
    const VectorOfVectorsType* data_;

    /// \brief bounding box
    WorldVector minCorner_;
    WorldVector maxCorner_;

  public:
    /// Constructor: takes a const ref to the vector of vectors object with the data points
    KDTree(const VectorOfVectorsType& mat, const WorldVector& minCorner, const WorldVector& maxCorner, int leaf_max_size = 10)
      : data_(&mat)
      , minCorner_(minCorner)
      , maxCorner_(maxCorner)
    {
      index_ = std::make_unique<index_t>(DIM, *this /* adaptor */, nanoflann::KDTreeSingleIndexAdaptorParams(leaf_max_size));
    }

    /// \brief Must be called before the index can be used
    void update()
    {
#ifndef NDEBUG
      Timer t;
#endif

      assert(data_->size() != 0 && (*data_)[0].size() != 0);
      index_->buildIndex();

#ifndef NDEBUG
      std::cout << "update kdtree needed " << t.elapsed() << " seconds" << std::endl;
#endif
    }

    void update(const VectorOfVectorsType& mat)
    {
      data_ = &mat;
    }

    /// \brief Query for the numClosest closest points to a given queryPoint
    void query(const WorldVector& queryPoint, std::size_t numClosest, std::vector<IndexType>& outIndices, std::vector<num_t>& outDistancesSq) const
    {
      nanoflann::KNNResultSet<num_t, IndexType> resultSet(numClosest);
      resultSet.init(outIndices.data(), outDistancesSq.data());
      index_->findNeighbors(resultSet, queryPoint.data(), nanoflann::SearchParams());
    }

    /// \brief Search for all point in a given radius around the queryPoint. Store the indices and the squared distance in the `matches` output vector
    void query(const WorldVector& queryPoint, num_t radius, std::vector<std::pair<IndexType, num_t>>& matches) const
    {
      index_->radiusSearch(queryPoint.data(), radius, matches, nanoflann::SearchParams());
    }

    /** @name Interface expected by KDTreeSingleIndexAdaptor
      * @{ */

    const self_t& derived() const { return *this; }

    self_t& derived() { return *this; }

    // Must return the number of data points
    std::size_t kdtree_get_point_count() const
    {
      return data_->size();
    }

    // Returns the d'th component of the idx'th point in the class:
    num_t kdtree_get_pt(std::size_t idx, std::size_t d) const
    {
      return (*data_)[idx][d];
    }

    // Optional bounding-box computation: return false to default to a standard bbox computation loop.
    //   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
    //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
    template <class BBOX>
    bool kdtree_get_bbox(BBOX& bb) const
    {
      for (int i = 0; i < DIM; ++i) {
        bb[i].low = minCorner_[i];
        bb[i].high = maxCorner_[i];
      }
      return true;
    }

    /** @} */

  }; // end of KDTree


  template <class T, int dim, class Range>
  class NearestNeighborApproximation
  {
  public:
    using Tree = KDTree<T, dim>;
    using WorldVector = FieldVector<T, dim>;
    /// \brief Type of the container to store the points
    using VectorOfVectorsType = std::vector<WorldVector>;
    using RangeType = Range;
    using RangeVectorType = std::vector<Range>;
  private:
    // Tree only holds a pointer to data, thus we give ownership to this wrapper
    std::shared_ptr<VectorOfVectorsType> points_;
    std::shared_ptr<RangeVectorType> values_;
    std::shared_ptr<Tree> tree_;

  public:
    /// @brief  Constructor
    /// @param points point Set. Taken and passed as const ref
    /// @param minCorner
    /// @param maxCorner
    /// @param data Data to the point set. Matched via indices
    NearestNeighborApproximation() = delete;
    NearestNeighborApproximation(VectorOfVectorsType const& points,
      const WorldVector& minCorner, const WorldVector& maxCorner, RangeVectorType const& data)
      : points_(std::make_shared<VectorOfVectorsType>(points)),
      values_(std::make_shared<RangeVectorType>(data)),
      tree_(std::make_shared < KDTree<typename WorldVector::value_type, WorldVector::dimension>>(*points_, minCorner, maxCorner))
    {
      tree_->update();
    }

    // Constructor from rvalue data to avoid copying points and data
    NearestNeighborApproximation(VectorOfVectorsType&& points,
      const WorldVector& minCorner, const WorldVector& maxCorner, RangeVectorType&& data)
      : points_(std::make_shared<VectorOfVectorsType>(points)),
      values_(std::make_shared<RangeVectorType>(data)),
      tree_(std::make_shared < KDTree<typename WorldVector::value_type, WorldVector::dimension>>(*points_, minCorner, maxCorner))
    {
      tree_->update();
    }

    // Copy constructor
    NearestNeighborApproximation(NearestNeighborApproximation const& other)
    {
      tree_ = other.tree_;
      values_ = other.values_;
      points_ = other.points_;
    }


    /// @brief update Tree and values
    /// @param points
    /// @param data
    void update(VectorOfVectorsType const& points, RangeVectorType const& data)
    {
      *points_ = points;
      tree_->update(*points_);
      tree_->update();
      values_ = data;
    }

    /// @brief evaluate data corresponding to nearest neighbor of Point x
    /// @param x
    /// @return Range
    RangeType operator()(WorldVector const& x) const
    {
      std::vector<typename Tree::IndexType> outIndices(1);
      std::vector<typename Tree::num_t> outDistancesSq(1);
      tree_->query(x, 1, outIndices, outDistancesSq);
      assert(outIndices.size() == 1);
      return values_->at(outIndices[0]);
    }


  };

  namespace Impl{
  // Helper Functions
  template<class T, typename std::enable_if_t<std::is_floating_point<T>::value, int> = 0>
  void assign(T& s, std::size_t i, double data)
  {
    assert(i == 0);
    s = data;
  }

  template<class T, int n>
  void assign(Dune::FieldVector<T, n>& v, std::size_t i, double data)
  {
    assert(i < n);
    v[i] = data;
  }

  template<class T, int rows, int cols>
  void assign(Dune::FieldMatrix<T, rows, cols>& m, std::size_t i, double data)
  {
    assert(i < rows * cols);
    m[i / cols][i % cols] = data;
  }

  /// @brief adapts min and max Vectors component wise s.t. min[i] < pt[i] < max[i]
  /// @tparam T
  /// @tparam n
  /// @param min
  /// @param max
  /// @param pt
  template<class T, int n>
  void adaptMinMax(Dune::FieldVector<T, n>& min, Dune::FieldVector<T, n>& max, Dune::FieldVector<T, n> const& pt)
  {
    using Dune::FloatCmp::lt;
    min[0] = lt(pt[0], min[0]) ? pt[0] : min[0];
    min[1] = lt(pt[1], min[1]) ? pt[1] : min[1];
    max[0] = lt(pt[0], max[0]) ? max[0] : pt[0];
    max[1] = lt(pt[1], max[1]) ? max[1] : pt[1];
  }
  }
  /// @brief Create a NearestNeighborApproximation from a csv File
  /// @tparam T valuetype for points
  /// @tparam dim dimension of the points
  /// @tparam Range (container)type for data. If a container, its value_type needs to be assignable with a double
  /// @param filename file to the csv file
  /// @param pointColumns names of the columns that define the points
  /// @param dataColumns names of the columns that are used to fill the data objects
  /// @return NearestNeighborApproximation<T, dim, Range>
  template<class T, int dim, class Range = double>
  NearestNeighborApproximation<T, dim, Range> pointCloudFromCSV(std::string filename,
    std::vector<std::string> pointColumns, std::vector<std::string> dataColumns, std::string deliminator = ",")
  {

    std::vector<double> row;
    std::vector<std::string> header;
    std::vector<Dune::FieldVector<T, dim>> points;
    std::vector<Range> data;
    std::string line, word;
    Dune::FieldVector<T, dim> minCorner;
    Dune::FieldVector<T, dim> maxCorner;

    // TODO reserve vectors
    // parse CSV File
    std::fstream file(filename, std::ios::in);
    if (file.is_open())
    {
      std::vector<std::size_t> pointIndices;
      std::vector<std::size_t> dataIndices;
      pointIndices.resize(pointColumns.size(), 0);
      dataIndices.resize(dataColumns.size(), 0);

      // read header
      getline(file, line);
      std::stringstream str(line);

      while (getline(str, word, ','))
        header.push_back(word);

      for (std::size_t i = 0; i < header.size(); ++i)
      {
        for (std::size_t j = 0; j < pointColumns.size(); ++j)
          if (header[i] == pointColumns[j])
          {
            pointIndices[j] = i;
            pointColumns[j] = "found";
          }
        for (std::size_t j = 0; j < dataColumns.size(); ++j)
          if (header[i] == dataColumns[j])
          {
            dataIndices[j] = i;
            dataColumns[j] = "found";
          }
      }
      // check if all columns are found
      for (size_t j = 0; j < pointColumns.size(); j++)
        if (pointColumns[j] != "found")
          throw std::runtime_error("Couldn't find column '" + pointColumns[j] + "'");
      for (size_t j = 0; j < dataColumns.size(); j++)
        if (dataColumns[j] != "found")
          throw std::runtime_error("Couldn't find column '" + dataColumns[j] + "'");

      assert(pointColumns.size() == dim);

      // read Data
      for (std::size_t i = 0; getline(file, line); ++i)
      {
        row.clear();

        std::stringstream str(line);

        while (getline(str, word, ','))
          row.push_back(std::stod(word));

        Dune::FieldVector<T, dim> pt;
        for (size_t j = 0; j < pointColumns.size(); j++)
          pt[j] = row[pointIndices[j]];
        Impl::adaptMinMax(minCorner, maxCorner, pt);

        Range ptData;
        for (size_t j = 0; j < dataColumns.size(); j++)
          Impl::assign(ptData, j, row[dataIndices[j]]);
        // assign
        points.push_back(pt);
        data.push_back(ptData);
      }
    }
    else
      std::cout << "Could not open the file\n";
    file.close();

    return NearestNeighborApproximation<T, dim, Range>(points, minCorner, maxCorner, data);
  }

} // end namespace Dune

