#pragma once
#include "amdis/gridfunctions/ComposerGridFunction.hpp"
#include <amdis/AMDiS.hpp>

#include <dune/common/fmatrix.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <type_traits>

#include <dune/functions/common/differentiablefunctionfromcallables.hh>
#include <dune/functions/functionspacebases/sizeinfo.hh>

#include <amdis/ClampedBC.hpp>
#include <amdis/PointwiseConstraint.hpp>
#include <amdis/common/FieldMatVec.hpp>
#include <amdis/typetree/TreePath.hpp>

#include <amdis/AdaptStationary.hpp>
#include <amdis/io/VTKSequenceWriter.hpp>

#include <amdis/localoperators/GenericVariableOrderOperator.hpp>

#include <amdis/vonKarman/Defect.hpp>
#include <amdis/vonKarman/MatMult.hpp>
#include <amdis/vonKarman/kdtree.hh>
#include <amdis/vonKarman/utility.hpp>
using namespace AMDiS;
using namespace AMDiS::VonKarman;
using Dune::Indices::_0;
using Dune::Indices::_1;

template<class Traits>
class FlatAWProblem : public Experiment
{
    using Problem = ProblemStat<Traits>;
    using SolutionVector = typename Problem::SolutionVector;
    using GridView = typename Problem::GridView;
    using GlobalBasis = typename Problem::GlobalBasis;
    static constexpr int NormOrder = 2;

  public:
    FlatAWProblem() = delete;

    // Base constructor
    FlatAWProblem(Environment& e, Problem p, std::string name = "problem",
                  std::string directory = "output")
        : Experiment(directory), env_(e), name_(name), gv_(p.gridView()), p_(p)
    {
      // Parameters
      readParameters();
      refineAroundDefects();
      p_.initialize(INIT_ALL & ~INIT_FILEWRITER);
      // area_ = integrate(1., gv_, 1);
    }

    auto createWriter(std::string file)
    {
      auto writer = getVTKWriter(file, gv_);
      writer.addVertexData(p_.solution(airy_),
                           Dune::VTK::FieldInfo("airy", Dune::VTK::FieldInfo::Type::scalar, 1));

      auto vectorProxy =
          [](Dune::FieldMatrix<double, 2, 2> const& mat) -> Dune::FieldVector<double, 3> {
        return {mat[0][0], mat[1][1], mat[0][1]};
      };
      writer.addVertexData(makeGridFunction(invokeAtQP(vectorProxy, p_.solution(sigma_)), gv_),
                           Dune::VTK::FieldInfo("stress", Dune::VTK::FieldInfo::Type::vector, 3));
      return writer;
    }

    void initialize()
    {
      if (isInit_)
        return;
      else {
        // add operators
        addOperators(Parameters::get<int>(name_ + "->quad").value_or(5));
        addBoundaryConditions();
        isInit_ = true;
      }
    }

    void checkInitialized()
    {
      if (!isInit_)
        error_exit("Problem not initialized!");
    }

    void readParameters()
    {
      // Parameters for loops
      bc_ = Parameters::get<std::string>(name_ + "->boundary condition").value_or("invalid");

      Dune::FieldVector<double, 2> upperRight =
          Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right")
              .value_or(Dune::FieldVector<double, 2>{1., 1.});
      xmax = upperRight[0];
      ymax = upperRight[1];

      shift_ = Parameters::get<Dune::FieldVector<double, 2>>(name_ + "->shift data")
                   .value_or(Dune::FieldVector<double, 2>{0., 0.});
      absoluteShift_ = shift_;
      absoluteShift_[0] *= upperRight[0];
      absoluteShift_[1] *= upperRight[1];

      Parameters::get(name_ + "->maxRefinement", maxRefinement_);
      Parameters::get(name_ + "->initialE", E_);
      Parameters::get(name_ + "->poissonsRatio", Nu_);
      // Parameters for defect distributions
      Parameters::get(name_ + "->load defects", loadDefectsFromFile_);
      Parameters::get(name_ + "->gaussian approximation", approxDiracByGaussian_);
      Parameters::get(name_ + "->gaussian->sigma", gaussianSigma_);

      createDefects();
    }

    void createDefects()
    {
      // TODO add Growth
      bool relativeDefectPositions =
          Parameters::get<bool>(name_ + "->defects->relative position").value_or(false);
      dislocations_.clear();
      disclinations_.clear();
      // always add defects to make refineAroundDefects() work
      // loadDefectsFromFile_ = Parameters::get<bool>(name_ + "->load defects").value_or(false);
      // if (loadDefectsFromFile_)
      //   return;
      // dislocations
      Parameters::get(name_ + "->dislocation", dislocation_);
      if (dislocation_)
        for (int i = 0; i < dislocation_; ++i)
          dislocations_.push_back(createDislocation(name_ + "->dislocation", i,
                                                    relativeDefectPositions, absoluteShift_));
      // disclinations
      Parameters::get(name_ + "->disclination", disclination_);
      if (disclination_)
        for (int i = 0; i < disclination_; ++i)
          disclinations_.push_back(createDisclination(name_ + "->disclination", i,
                                                      relativeDefectPositions, absoluteShift_));
    }

    void refineAroundDefects()
    {
      auto refinements = Parameters::get<int>(name_ + "->local refinements").value_or(0);
      auto distance = Parameters::get<double>(name_ + "->local refinements->distance").value_or(1.);
      // fill vector with defect positions
      std::vector<Dune::FieldVector<double, 2>> coordinates;
      for (auto const& defect : dislocations_)
        coordinates.push_back(defect.position());
      for (auto const& defect : disclinations_)
        coordinates.push_back(defect.position());
      // refine locally 'refinements' times
      for (int i = 0; i < refinements; ++i) {
        for (auto const& e : elements(gv_)) {
          for (auto const& p : coordinates)
            if ((e.geometry().center() - p).two_norm2() < distance * distance)
              p_.grid()->mark(1, e);
        }
        AdaptInfo a{"hans"};
        p_.adaptGrid(a);
      }
    }

    void addOperators(int quadOrder = 5)
    {
      if (operatorsSet_)
        error_exit("Adding Operators twice");
      else {
        auto ZeroFunctor = [](auto... args) { return 0.; };
        // First equation tested with psi from V_1
        {
          auto lhs = [](auto const& psi, auto const& phi) { return psi * phi; };
          auto op = genericOperator<0>(10, lhs, ZeroFunctor);
          p_.addMatrixOperator(op, airy_, airy_);
        }
        {
          auto lhs = [](auto const& hessPsi, auto const& sigma, auto Nu, auto E) {
            return -frobeniusProduct((1. + Nu) / E * sigma - Nu / E * tr(sigma) * identity(),
                                     cof(hessPsi));
          };
          auto op = genericOperator<2, 0>(10, lhs, ZeroFunctor, Operation::Constant(Nu_),
                                          Operation::Constant(E_));
          p_.addMatrixOperator(op, airy_, sigma_);
        }
        {
          auto lhs = [](auto const& tau, auto const& hessPhi, auto Nu, auto E) {
            return frobeniusProduct(
                (1. + Nu) / E * cof(hessPhi) - Nu / E * tr(hessPhi) * identity(), tau);
          };
          auto op = genericOperator<0, 2>(10, lhs, ZeroFunctor, Operation::Constant(Nu_),
                                          Operation::Constant(E_));
          p_.addMatrixOperator(op, sigma_, airy_);
        }
        {
          auto lhs = [](auto const& gradTau, auto const& gradSigma) {
            return Dune::dot(rowTr(gradSigma), rowTr(gradTau));
          };
          auto op = genericOperator<1, 1>(10, lhs, ZeroFunctor);
          p_.addMatrixOperator(op, sigma_, sigma_);
        }

        {
          auto rhs = [](auto const& tau, auto const& f) { return frobeniusProduct(tau, f); };
          auto force = [](auto const& x) {
            return Dune::FieldMatrix<double, 2, 2>{{-32./30, 0.}, {0,-32./30}};
          };

          auto op = genericOperator<0>(10, ZeroFunctor, rhs, force);
          p_.addVectorOperator(op, sigma_);
        }
        // addDefectOperators(p_, quadOrder + 3);
        operatorsSet_ = true;
      }
    }

    /**
     * @brief
     *        Currently only homogeneous clamped and periodic BCs are implemented
     *        TODO implement free bc
     */
    void addBoundaryConditions()
    {
      // TODO add different Boundary Conditions
      //  Homogeneous Clamped BC on both solutions
      // if (bc_ == "clamped") {
      //   p_.boundaryManager()->setBoxBoundary({1, 1, 1, 1});
      //   auto zeroGridFct = makeGridFunction(evalAtQP(getZeroFunction()), p_.gridView());
      //   auto clampedBC = ClampedBC{p_.globalBasis(),
      //                              sigma_,
      //                              sigma_,
      //                              {*(p_.boundaryManager()), BoundaryType{1}},
      //                              zeroGridFct};
      //   p_.addConstraint(clampedBC);
      // } else
      if (bc_ == "free") {
        // error_exit("Free Boundary conditions are currently not implemented!");
      } else if (bc_ == "lagrange") {
        error_exit("Lagrange condition not implemented!");
        // error_exit("Lagrange conditions need a corresponding basis in the basis tree and a
        // fitting treepath argument");
      } else if (bc_ == "none")
        warning("Solving problem without boundary conditions!");
      else if (bc_ == "invalid")
        error_exit("Must specify boundary conditions!");
      else
        error_exit("Invalid boundary condition " + bc_ + " given in initfile");
    }

    void assemble(Problem& p, bool assembleMatrix = true, bool assembleRhs = true)
    {
      Dune::Timer timer;
      AdaptInfo adaptInfo("problem");
      p.buildAfterAdapt(adaptInfo, Flag{0}, assembleMatrix, assembleRhs);
      // if (reAssembleDirac_) {
      //   addDefectOperators(p);
      // }
      info("Total assembling took {} seconds", timer.elapsed());
    }

    void solve(Problem& p)
    {
      info("Solving Linear System of Problem {}", p.name());
      p.rhsVector()->finish();
      Dune::Timer timer;
      AdaptInfo adaptInfo("problem");
      p.solve(adaptInfo);
      info("Solving System took {} seconds", timer.elapsed());
    }

    void solveProblem()
    {
      assemble(p_);
      solve(p_);
      auto writer = createWriter("StressSolution");
      writer.write(0.);
    }

    void globalRefine(std::size_t i)
    {
      p_.globalRefine(i);
      gv_ = p_.gridView();
      // area_ = integrate(1., gv_, 1);
      // assemble(p_, true, true);
    }

    void solveForRefinementLevels()
    {
      checkInitialized();

      auto const& u_h = *p_.solutionVector();
      auto csvWriter = getCSVWriter("convergence", {"refinement", "h", "$||u-u_h||_\\Omega$",
                                                    // "$||u-u_h||_{\\Omega /defect}$",
                                                    "$\\lambda$", "$\\int_\\Omega u$"});
      bool compareToContinuum =
          Parameters::get<bool>(name_ + "->compareToContinuum").value_or(false);

      int periodicity =
          Parameters::get<double>(name_ + "->evaluation->periodicity level").value_or(1);
      info("periodicity for continuum solution: {}", periodicity);

      // auto sigma = evalAtQP(getContinuumStress(dislocations_, E_, periodicity));
      auto sigma = [](auto const& x)->Dune::FieldMatrix<double, 2, 2>
      {
        auto tr = 1/7.5 * x.two_norm2();
        return {{0.4*x[0]*x[0] + tr, 0},{0, 0.5*x[1]*x[1] + tr}};
      };

      auto vectorProxy =
          [](Dune::FieldMatrix<double, 2, 2> const& mat) -> Dune::FieldVector<double, 3> {
        return {mat[0][0], mat[1][1], mat[0][1]};
      };
      auto stressSolution = invokeAtQP(vectorProxy, makeGridFunction(sigma, gv_));

      auto stress = makeGridFunction(invokeAtQP(vectorProxy, valueOf(u_h, sigma_)), gv_);

      // double cutOffRadius = Parameters::get<double>(name_ + "->evaluation->cut off radius")
      // .value_or(100); //(xmax*xmax+ ymax*ymax)/1e5
      // auto tmp = getDefectIndicatorFunction(dislocations_, cutOffRadius);
      // auto indicator = invokeAtQP(Operation::Minus{}, 1., tmp);

      // auto stressAverage =
      // integrate(stressSolution * indicator, gv_, 10) / integrate(indicator, gv_, 10);
      auto stressDifference = makeGridFunction(stress - stressSolution, gv_);

      auto vtkWriter = createWriter("flatSolution");
      vtkWriter.addVertexData(
          makeGridFunction(stressSolution, gv_),
          Dune::VTK::FieldInfo("analyticalStress", Dune::VTK::FieldInfo::Type::vector, 3));
      vtkWriter.addVertexData(
          stressDifference,
          Dune::VTK::FieldInfo("stressDifference", Dune::VTK::FieldInfo::Type::vector, 3));
      // vtkWriter.addVertexData(makeGridFunction(stressDifference * indicator, gv_),
      // Dune::VTK::FieldInfo("stressDifferenceWithoutDefectAreas",
      //  Dune::VTK::FieldInfo::Type::vector, 3));

      // for loop for uniform iterations
      for (std::size_t refine = 1; refine <= maxRefinement_; ++refine) {
        info("#######################  Refinement Level {}  #############################", refine);
        globalRefine(1);
        auto h = getMeshWidth(gv_);

        solveProblem();
        if (compareToContinuum) {
          // stressAverage = integrate(stressSolution, gv_, 10) / area_;

          info("L2 difference to analytical solution; Without defect neighborhoods; Lagrange "
               "multiplier; Lagrange condition ");
          std::vector<double> values{
              (double)refine, h, sqrt(integrate(unary_dot(stressDifference), gv_, 10)),
              //     sqrt(integrate(unary_dot(stressDifference) * indicator, gv_, 10)),
              //     p_.solution(sigma_)(Dune::FieldVector<double, 2>{1., 1.}),
              sqrt(integrate(invokeAtQP([](auto const& m) { return m.frobenius_norm2(); },
                                        p_.solution(sigma_)),
                             gv_, 10))};
          csvWriter.tee(values);
          vtkWriter.write(double(refine));
        }
      }
    }

  private:
    Environment& env_;
    std::string name_;
    GridView gv_;
    Problem p_;
    // double area_ = 1.;
    double E_ = 1.;
    double Nu_ = 0.25;

    std::decay_t<decltype(makeTreePath(_0))> airy_ = makeTreePath(_0);
    std::decay_t<decltype(makeTreePath(_1))> sigma_ = makeTreePath(_1);

    std::string bc_;
    //// Parameters
    double xmax = 1.;
    double ymax = 1.;
    Dune::FieldVector<double, 2> shift_ = {0., 0.};
    Dune::FieldVector<double, 2> absoluteShift_ = {0., 0.};

    /// Refinement
    std::size_t initialRefinement_ = 1;
    std::size_t maxRefinement_ = 1;

    double lambda_ = 1.;
    // Parameters for defect distributions
    bool loadDefectsFromFile_ = false;
    bool approxDiracByGaussian_ = true;
    double gaussianSigma_ = 0.1;

    // dislocations
    int dislocation_ = 0;
    std::vector<Dislocation> dislocations_;
    // disclinations
    int disclination_ = 0;
    std::vector<Disclination> disclinations_;
    bool reAssembleDirac_ = false;

    bool isInit_ = false;
    bool operatorsSet_ = false;
};

template<class Traits>
FlatAWProblem(Environment&, ProblemStat<Traits>, std::string, std::string) -> FlatAWProblem<Traits>;

template<class Traits>
void run(FlatAWProblem<Traits>& p)
{
  p.initialize();
  p.solveForRefinementLevels();
}