#pragma once

#include <amdis/linearalgebra/Traits.hpp>
#include <amdis/linearalgebra/MatrixFacade.hpp>
#include <amdis/linearalgebra/VectorFacade.hpp>
#include <amdis/algorithm/InnerProduct.hpp>

namespace Impl{
#if AMDIS_HAS_MTL

#elif AMDIS_HAS_EIGEN

#elif AMDIS_HAS_PETSC
  template <class Matrix, class Vector>
  void mv(Matrix const& mat, Vector const& x, Vector& y)
  {
    MatMult(mat, x, y);
  }

#else // ISTL
  template <class Matrix, class Vector>
  void mv(Matrix const& mat, Vector const& x, Vector& y)
  {
    mat.mv(x, y);
  }

#endif
}

template <class T, template<class> class MatrixImpl, template <class> class VectorImpl>
void mv(AMDiS::MatrixFacade<T, MatrixImpl> const& m, AMDiS::VectorFacade<T, VectorImpl> const& x, AMDiS::VectorFacade<T, VectorImpl>& y)
{
  Impl::mv(m.impl().matrix(), x.impl().vector(), y.impl().vector());
}

template <class T, template<class> class MatrixImpl, template <class> class VectorImpl>
T vmv(AMDiS::MatrixFacade<T, MatrixImpl> const& m, AMDiS::VectorFacade<T, VectorImpl> const& x)
{
  AMDiS::VectorFacade<T, VectorImpl> tmp(x);
  mv(m, x, tmp);
  T init = 0.;
  return AMDiS::Recursive::innerProduct(x.impl(), tmp.impl(), init);
}