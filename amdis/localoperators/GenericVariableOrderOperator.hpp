#pragma once

#include "amdis/Output.hpp"
#include "amdis/common/FieldMatVec.hpp"
#include "dune/common/transpose.hh"
#include "dune/geometry/quadraturerules.hh"
#include <tuple>
#include <type_traits>

#include <dune/common/hybridutilities.hh>

#include <amdis/LocalOperator.hpp>
#include <amdis/common/Order.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/typetree/FiniteElementType.hpp>
#include <utility>
#include <vector>

namespace AMDiS
{
namespace Impl
{
template<class Node, class ContextGeo>
struct RealJacobian {
    using JacobianT = typename ContextGeo::ElementGeometry::JacobianInverseTransposed;
    using LocalCoord = typename ContextGeo::ElementGeometry::LocalCoordinate;
    using type = AMDiS::remove_cvref_t<decltype(std::declval<Node>().localBasisJacobiansAt(
                                                    std::declval<LocalCoord>())[0] *
                                                Dune::transpose(std::declval<JacobianT>()))>;
};

template<class Node, class ContextGeo, class Enabled = bool>
struct RealHessian {
    using Jacobian = typename ContextGeo::ElementGeometry::Jacobian;
    using type = Dune::FieldMatrix<double, Jacobian::rows, Jacobian::rows>;
};

template<class Node, class ContextGeo>
struct RealHessian<Node, ContextGeo,
                   decltype((
                       std::declval<Node>().localBasisHesiansAt(
                           std::declval<typename ContextGeo::ElementGeometry::LocalCoordinate>()),
                       true))> {
    using JacobianT = typename ContextGeo::ElementGeometry::JacobianInverseTransposed;
    using LocalCoord = typename ContextGeo::ElementGeometry::LocalCoordinate;

    using type = AMDiS::remove_cvref_t<decltype(std::declval<JacobianT>() *
                                                std::declval<Node>().localBasisHesiansAt(
                                                    std::declval<LocalCoord>())[0] *
                                                Dune::transpose(std::declval<JacobianT>()))>;
};
} // namespace Impl

/**
 * \addtogroup operators
 * @{
 **/

// forward declaration for GenericLocalOperator
template<int ordRow, int ordCol, class LHS, class RHS, class... LocalFct>
class GenericLocalOperator;

// factory method to allow partial template deduction
template<int orderRow, int orderCol, class LHS, class RHS, class... LocalFct>
GenericLocalOperator<orderRow, orderCol, LHS, RHS, LocalFct...>
make_GenericLocalOperator(int quadOrder, LHS const &lhs, RHS const &rhs, LocalFct const &...lfs)
{
  return GenericLocalOperator<orderRow, orderCol, LHS, RHS, LocalFct...>(quadOrder, lhs, rhs,
                                                                         lfs...);
}

// GenericOperator class currently holds value to gridFunctions.
/**
 * @brief Generic Local Operator that holds a Functor for left hand side and
 * right hand side and an arbitrary number of GridFunctions. The order parameter
 * specifies the derivativeOrder of the shapefunctions to be evaluated. In
 * Contrast to the naming scheme of other operators, it works the following way.
 * order == 0 means no derivatives are evaluated. The lhs functor evaluation has
              the argument list (shapevalueRow, shapevalueCol, gridFunctions...)
 * order == i > 0 means only derivatives of order i are evaluted. The lhs
                  functor evaluation has the argument list
                  (derivative_i_Row, derivative_i_Col, gridFunctions...)
 * order == -i means derivatives up to order i are evaluated. The lhs function
 evaluation has the argument list (shapevalueRow, derivative_1_Row, ..., derivative_i_Row,
  shapevalueCol, derivative_1_Col, ..., derivative_i_Col,
 gridFunctions...)
 * Currently only derivatives up to order two (for each Node) are implemented.
 * @tparam ord
 * @tparam LHS
 * @tparam RHS
 * @tparam GridFct
 */
template<int orderRow, int orderCol, class LHS, class RHS, class... GridFct>
class GenericOperator
{
  public:
    static constexpr int degree = orderRow + orderCol;
    /// \brief Constructor. Stores a copy of all passed gridfunctions and
    /// Functors
    GenericOperator(int quadOrder, LHS const &lhs, RHS const &rhs, GridFct const &...gridFcts)
        : gridFunctions_(std::make_tuple(gridFcts...)), lhs_(lhs), rhs_(rhs), quadOrder_(quadOrder)
    {
    }

    template<class GridView>
    void update(GridView const &)
    { /* do nothing */
    }

    friend auto localOperator(GenericOperator const &op)
    {
      auto lambda = [&](auto const &...gfs) {
        return make_GenericLocalOperator<orderRow, orderCol>(op.quadOrder_, op.lhs_, op.rhs_,
                                                             localFunction(gfs)...);
      };
      return std::apply(lambda, op.gridFunctions_);
    }

  private:
    std::tuple<GridFct...> gridFunctions_;
    LHS lhs_;
    RHS rhs_;
    int quadOrder_;
};

// factory method to allow partial template deduction
template<int orderRow, int orderCol, class LHS, class RHS, class... GridFct>
GenericOperator<orderRow, orderCol, LHS, RHS, GridFct...>
make_GenericOperator(int quadOrder, LHS const &lhs, RHS const &rhs, GridFct const &...gfs)
{
  return GenericOperator<orderRow, orderCol, LHS, RHS, GridFct...>(quadOrder, lhs, rhs, gfs...);
}

template<int orderRow, int orderCol, class LHS, class RHS, class... LocalFct>
class GenericLocalOperator
{
    static constexpr int derivativeOrder = std::abs(orderRow) + std::abs(orderCol);

  public:
    GenericLocalOperator(int quadOrder, LHS const &lhs, RHS const &rhs,
                         LocalFct const &...localFcts)
        : localFunctions_(std::forward_as_tuple(localFcts...)), lhs_(lhs), rhs_(rhs),
          quadOrder_(quadOrder)
    {
    }

    template<class Element>
    void bind(Element const &element)
    {
      std::apply([&](auto &...lfs) { (lfs.bind(element), ...); }, localFunctions_);
    }

    /// Unbinds operator from element.
    void unbind()
    {
      std::apply([](auto &...lfs) { (lfs.unbind(), ...); }, localFunctions_);
    }

    template<class CG, class RN, class CN, class Mat>
    void assemble(CG const &contextGeo, RN const &rowNode, CN const &colNode,
                  Mat &elementMatrix) const
    {
      // TODO optimize this for same type of nodes
      // TODO extend this to PowerNodes
      static_assert(RN::isLeaf && CN::isLeaf, "Operator can be applied to Leaf-Nodes only.");

      using RangeFieldType = typename RN::LocalBasis::Traits::RangeFieldType;
      using ShapeValueTypeRow = typename RN::LocalBasis::Traits::RangeType;
      using ShapeValueTypeCol = typename CN::LocalBasis::Traits::RangeType;

      // TODO is this necessary?
      static_assert(std::is_same_v<RangeFieldType, typename CN::LocalBasis::Traits::RangeFieldType>,
                    "Test and Trial fe spaces should use the same FieldType");
      using RealGradientRow = typename Impl::RealJacobian<RN, CG>::type;
      using RealGradientCol = typename Impl::RealJacobian<CN, CG>::type;

      using RealHessianRow = typename Impl::RealHessian<RN, CG>::type;
      using RealHessianCol = typename Impl::RealHessian<CN, CG>::type;

      std::vector<ShapeValueTypeRow> shapeValuesRow;
      std::vector<ShapeValueTypeCol> shapeValuesCol;

      std::vector<RealGradientRow> gradientsRow;
      std::vector<RealGradientCol> gradientsCol;

      std::vector<RealHessianRow> hessiansRow;
      std::vector<RealHessianCol> hessiansCol;

      // std::size_t numLocalFe = rowNode.size();

      auto contextGeometry = contextGeo.geometry();
      int quadDeg;
      if (quadOrder_ == -1) {
        if constexpr(sizeof...(LocalFct) == 0)
        {
          quadDeg = getQuadratureDegree(contextGeometry, derivativeOrder >= 0 ? derivativeOrder : 0,0, rowNode, colNode);
        }
        else
          quadDeg = std::apply(
            [&](auto const &...args) {
              return std::max({getQuadratureDegree(contextGeometry,
                                                       derivativeOrder >= 0 ? derivativeOrder : 0,
                                                       coeffOrder(args), rowNode, colNode)...});
            },
            localFunctions_);
      } else
        quadDeg = quadOrder_;

      using QuadratureRules =
          Dune::QuadratureRules<typename CG::Geometry::ctype, CG::Geometry::mydimension>;
      auto const &quad = QuadratureRules::rule(contextGeo.type(), quadDeg);

      for (std::size_t iq = 0; iq < quad.size(); ++iq) {
        // Position of the current quadrature point in the reference element
        auto &&local = contextGeo.coordinateInElement(quad[iq].position());

        // evaluation of localFunctions as a tuple
        auto localFunctionValues = std::apply(
            [&](auto const &...args) { return std::make_tuple(args(local)...); }, localFunctions_);
        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(quad[iq].position()) * quad[iq].weight();

        // the values of the shape functions on the reference element at the
        // quadrature point
        if constexpr (orderRow <= 0)
          shapeValuesRow = rowNode.localBasisValuesAt(local);
        if constexpr (orderCol <= 0)
          shapeValuesCol = colNode.localBasisValuesAt(local);

        // for zero order operators we have everything we need
        // For all others we need the Jacobian and corresponding derivatives
        if constexpr (!(orderCol == 0 and orderRow == 0)) {
          // The transposed inverse Jacobian of the map from the reference element
          // to the element
          const auto JIT = contextGeo.elementGeometry().jacobianInverseTransposed(local);
          if constexpr (orderRow < 0 or orderRow == 1) {
            // The gradients of the shape functions on the reference element
            auto const &shapeGradientsRow = rowNode.localBasisJacobiansAt(local);

            // Compute the shape function gradients on the real element
            gradientsRow.resize(shapeGradientsRow.size());
            for (std::size_t i = 0; i < gradientsRow.size(); ++i)
              gradientsRow[i] = shapeGradientsRow[i] * Dune::transpose(JIT);
          }
          if constexpr (orderCol < 0 or orderCol == 1) {
            // The gradients of the shape functions on the reference element
            auto const &shapeGradientsCol = colNode.localBasisJacobiansAt(local);

            // Compute the shape function gradients on the real element
            gradientsCol.resize(shapeGradientsCol.size());
            for (std::size_t i = 0; i < gradientsCol.size(); ++i)
              gradientsCol[i] = shapeGradientsCol[i] * Dune::transpose(JIT);
          }

          if constexpr (orderRow == 2 or orderRow == -2) {
            // The Hessianss of the shape functions on the reference element
            auto const &shapeHessiansRow = rowNode.localBasisHessiansAt(local);

            // Compute the shape function Hessians on the real element
            hessiansRow.resize(shapeHessiansRow.size());
            for (std::size_t i = 0; i < hessiansRow.size(); ++i)
              hessiansRow[i] =
                  JIT * Dune::MatVec::as_matrix(shapeHessiansRow[i]) * Dune::transpose(JIT);
          }

          if constexpr (orderCol == 2 or orderCol == -2) {
            // The Hessianss of the shape functions on the reference element
            auto const &shapeHessiansCol = colNode.localBasisHessiansAt(local);

            // Compute the shape function Hessians on the real element
            hessiansCol.resize(shapeHessiansCol.size());
            for (std::size_t i = 0; i < hessiansCol.size(); ++i)
              hessiansCol[i] =
                  JIT * Dune::MatVec::as_matrix(shapeHessiansCol[i]) * Dune::transpose(JIT);
          }
        }

        // A wild construct that allows partial evaluation of the LHS functor
        auto partialEvaluator = std::apply(
            [&](auto const &...lfValues) {
              return [&](auto const &...args) { return lhs_(args..., lfValues...); };
            },
            localFunctionValues);

        // we have everything we need
        // Loop over all pairs of shapefunctions
        for (std::size_t i = 0; i < rowNode.size(); ++i) {
          const auto local_i = rowNode.localIndex(i);
          for (std::size_t j = 0; j < colNode.size(); ++j) {
            const auto local_j = colNode.localIndex(j);
            // first the zero order variant
            if constexpr (orderRow == 0 and orderCol == 0)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], shapeValuesCol[j]) * factor;
            // first order
            else if constexpr (orderRow == 0 and orderCol == 1)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], gradientsCol[j]) * factor;
            else if constexpr (orderRow == 1 and orderCol == 0)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(gradientsRow[i], shapeValuesCol[j]) * factor;
            else if constexpr (orderRow == -1 and orderCol == 0)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], gradientsRow[i], shapeValuesCol[j]) * factor;
            else if constexpr (orderRow == 0 and orderCol == -1)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], shapeValuesCol[j], gradientsCol[j]) * factor;
            // second order variants
            // pure second order
            else if constexpr (orderRow == 1 and orderCol == 1)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(gradientsRow[i], gradientsCol[j]) * factor;
            // up to second order
            else if constexpr (orderRow == -1 and orderCol == 1)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], gradientsRow[i], gradientsCol[j]) * factor;
            else if constexpr (orderRow == 1 and orderCol == -1)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(gradientsRow[i], shapeValuesCol[j], gradientsCol[j]) * factor;
            else if constexpr (orderRow == -1 and orderCol == -1)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], gradientsRow[i], shapeValuesCol[j],
                                   gradientsCol[j]) *
                  factor;
            // second order with all derivatives on one side
            else if constexpr (orderRow == 2 and orderCol == 0)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(hessiansRow[i], shapeValuesCol[j]) * factor;
            else if constexpr (orderRow == 0 and orderCol == 2)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], hessiansCol[j]) * factor;
            // TODO up to second order all on one side
            // TODO Third order
            // fourth order variants
            else if constexpr (orderRow == 2 and orderCol == 2)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(hessiansRow[i], hessiansCol[j]) * factor;
            else if constexpr (orderRow == -2 and orderCol == 2)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], gradientsRow[i], hessiansRow[i],
                                   hessiansCol[j]) *
                  factor;
            else if constexpr (orderRow == 2 and orderCol == -2)
              elementMatrix[local_i][local_j] += partialEvaluator(hessiansRow[i], shapeValuesCol[j],
                                                                  gradientsCol[j], hessiansCol[j]) *
                                                 factor;
            else if constexpr (orderRow == -2 and orderCol == -2)
              elementMatrix[local_i][local_j] +=
                  partialEvaluator(shapeValuesRow[i], gradientsRow[i], hessiansRow[i],
                                   shapeValuesCol[j], gradientsCol[j], hessiansCol[j]) *
                  factor;
            else
              error_exit("Desired order is not implemented!");
          }
        }
      }
    }

    template<class CG, class Node, class Vec>
    void assemble(CG const &contextGeo, Node const &node, Vec &elementVector) const
    {
      // TODO optimize this for same type of nodes
      // TODO extend this to PowerNodes
      static_assert(Node::isLeaf, "Operator can be applied to Leaf-Nodes only.");

      using RangeFieldType = typename Node::LocalBasis::Traits::RangeFieldType;
      using ShapeValueTypeRow = typename Node::LocalBasis::Traits::RangeType;

      using RealGradientRow = typename Impl::RealJacobian<Node, CG>::type;

      using RealHessianRow = typename Impl::RealHessian<Node, CG>::type;

      std::vector<ShapeValueTypeRow> shapeValues;

      std::vector<RealGradientRow> gradients;

      std::vector<RealHessianRow> hessians;

      auto contextGeometry = contextGeo.geometry();

      int quadDeg;
      if (quadOrder_ == -1) {
        if constexpr (sizeof...(LocalFct) == 0) {
          quadDeg = getQuadratureDegree(contextGeometry, derivativeOrder >= 0 ? derivativeOrder : 0,
                                        0, node);
        } else
          quadDeg = std::apply(
              [&](auto const &...args) {
                return std::max({getQuadratureDegree(contextGeometry,
                                                     derivativeOrder >= 0 ? derivativeOrder : 0,
                                                     coeffOrder(args), node)...});
              },
              localFunctions_);
      } else
        quadDeg = quadOrder_;

      using QuadratureRules =
          Dune::QuadratureRules<typename CG::Geometry::ctype, CG::Geometry::mydimension>;
      auto const &quad = QuadratureRules::rule(contextGeo.type(), quadDeg);

      for (std::size_t iq = 0; iq < quad.size(); ++iq) {
        // Position of the current quadrature point in the reference element
        auto &&local = contextGeo.coordinateInElement(quad[iq].position());

        // evaluation of localFunctions as a tuple
        auto localFunctionValues = std::apply(
            [&](auto const &...args) { return std::make_tuple(args(local)...); }, localFunctions_);
        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(quad[iq].position()) * quad[iq].weight();

        // the values of the shape functions on the reference element at the
        // quadrature point
        if constexpr (orderRow <= 0)
          shapeValues = node.localBasisValuesAt(local);

        // for zero order operators we have everything we need
        // For all others we need the Jacobian and corresponding derivatives
        if constexpr (orderCol != 0) {
          // The transposed inverse Jacobian of the map from the reference element
          // to the element
          const auto JIT = contextGeo.elementGeometry().jacobianInverseTransposed(local);
          if constexpr (orderRow < 0 or orderRow == 1) {
            // The gradients of the shape functions on the reference element
            auto const &shapeGradients = node.localBasisJacobiansAt(local);

            // Compute the shape function gradients on the real element
            gradients.resize(shapeGradients.size());
            for (std::size_t i = 0; i < gradients.size(); ++i)
              gradients[i] = shapeGradients[i] * Dune::transpose(JIT);
          }

          if constexpr (orderRow == 2 or orderRow == -2) {
            // The Hessianss of the shape functions on the reference element
            auto const &shapeHessians = node.localBasisHessiansAt(local);

            // Compute the shape function Hessians on the real element
            hessians.resize(shapeHessians.size());
            for (std::size_t i = 0; i < hessians.size(); ++i)
              hessians[i] = JIT * Dune::MatVec::as_matrix(shapeHessians[i]) * Dune::transpose(JIT);
          }
        }

        // A wild construct that allows partial evaluation of the LHS functor
        auto partialEvaluator = std::apply(
            [&](auto const &...lfValues) {
              return [&](auto const &...args) { return rhs_(args..., lfValues...); };
            },
            localFunctionValues);

        // we have everything we need
        // Loop over all pairs of shapefunctions
        for (std::size_t i = 0; i < node.size(); ++i) {
          const auto local_i = node.localIndex(i);

          // first the zero order variant
          if constexpr (orderRow == 0)
            elementVector[local_i] += partialEvaluator(shapeValues[i]) * factor;
          // first order
          else if constexpr (orderRow == 1)
            elementVector[local_i] += partialEvaluator(shapeValues[i]) * factor;

          else if constexpr (orderRow == -1)
            elementVector[local_i] += partialEvaluator(shapeValues[i], gradients[i]) * factor;

          // second order with all derivatives
          else if constexpr (orderRow == 2)
            elementVector[local_i] += partialEvaluator(hessians[i]) * factor;

          else if constexpr (orderRow == -2)
            elementVector[local_i] +=
                partialEvaluator(shapeValues[i], gradients[i], hessians[i]) * factor;
          else
            error_exit("Desired order is not implemented!");
        }
      }
    }

  private:
    // TODO think about some better default value than 0
    template<class LFct>
    int coeffOrder(LFct const &localFct) const
    {
      if constexpr (Concepts::Polynomial<LFct>)
        return order(localFct);
      else
        return 0;
    }

  private:
    std::tuple<LocalFct...> localFunctions_;
    LHS lhs_;
    RHS rhs_;
    int quadOrder_;
};

#ifndef DOXYGEN
template<int derivativeOrderRow, int derivativeOrderCol, class LHS, class RHS, class... PreGridFct>
struct GenericOperatorTerm {
    GenericOperatorTerm(int quadOrder, LHS const &lhs, RHS const &rhs, PreGridFct const &...pgfs)
        : lhs_(lhs), rhs_(rhs), preGridFcts_(std::make_tuple(pgfs...)), quadOrder_(quadOrder)
    {
    }
    LHS lhs_;
    RHS rhs_;
    std::tuple<PreGridFct...> preGridFcts_;
    int quadOrder_;
};

// this function is called by the BilinearForm. It does not correspond to the
// usual makeOperator(tag...) pattern
template<class Context, int derivativeOrderRow, int derivativeOrderCol, class LHS, class RHS,
         class... PreGridFct, class GridView>
auto makeOperator(
    GenericOperatorTerm<derivativeOrderRow, derivativeOrderCol, LHS, RHS, PreGridFct...> const &pre,
    GridView const &gridView)
{
  auto gridFunctionTuple = std::apply(
      [&gridView](auto const &...args) {
        return std::make_tuple(makeGridFunction(args, gridView)...);
      },
      pre.preGridFcts_);
  // todo unify apply calls
  auto l = [&](auto... args) {
    return make_GenericOperator<derivativeOrderRow, derivativeOrderCol>(
        pre.quadOrder_, pre.lhs_, pre.rhs_, std::move(args)...);
  };
  return std::apply(l, gridFunctionTuple);
}
#endif

// This function is the replacement for the usual makeOperator(tag...) pattern
/// Generator function for constructing a Generic Operator
template<int derivativeOrderRow, int derivativeOrderCol = derivativeOrderRow, class LHS, class RHS,
         class... PreGridFct>
auto genericOperator(int quadOrder, LHS const &lhs, RHS const &rhs, PreGridFct const &...gridFct)
{
  using Pre = GenericOperatorTerm<derivativeOrderRow, derivativeOrderCol, LHS, RHS, PreGridFct...>;
  return Pre{quadOrder, lhs, rhs, gridFct...};
}

/** @} **/

} // end namespace AMDiS
