#pragma once

#include <dune/common/transpose.hh>
#include <amdis/common/FieldMatVec.hpp>
#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag{
    template <bool update>
    struct VonKarmanIteration
    {
      VonKarmanIteration() = delete;
      VonKarmanIteration(double& d, double& e, double n): D(d), E(e), Nu(n) {}
      VonKarmanIteration(VonKarmanIteration const& other): D(other.D), E(other.E), Nu(other.Nu) {}

      double& D;
      double& E;
      double Nu = 0.3;
    };
  } // namespace tag

  namespace Impl{
    template <class K, int n>
    K sumDiagonal(Dune::FieldMatrix<K, n, n> const& m)
    {
      K sum = 0.;
      for (int i = 0; i < n; ++i)
        sum += m[i][i];
      return sum;
    }

    template <class K, class L>
    K mongeAmpereStructure(Dune::FieldMatrix<K, 2, 2> const& l, Dune::FieldMatrix<L, 2, 2> const& r)
    {
      return l[0][0] * r[1][1] + l[1][1] * r[0][0] - l[1][0] * r[0][1] - l[0][1] * r[1][0];
    }
  } // namespace Impl

  /**
   * \brief Fourth-order operator providing the left and right hand side to a newton iteration for
   * the von Karman plate problem in stressfunction description.
   * \tparam update Boolean indicating whether the newton iteration solves for and update $du$ such
   *that $u_{n+1} = u_n + du$ (true) or the next solution $u_{n+1}$ directly (false).
   **/
  template <bool update>
  class FourthOrderVonKarmanIteration
  {
  public:
    FourthOrderVonKarmanIteration(tag::VonKarmanIteration<update> tag): tag_(tag) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode, Quad const& quad,
      LocalFct const& localFct, Mat& elementMatrix) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 2,
        "Expression must be a vector of size two.");
      static_assert(RN::isPower && CN::isPower,
        "von Karman operator only implemented for power nodes");
      static_assert(CG::dow == 2, "von Karman operator only implemented in 2d!");

      const bool sameFE = std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>;
      using Category = ValueCategory_t<typename LocalFct::Range>;

      if (sameFE)
        getElementMatrixStandard(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
      else
        error_exit(
          "Not implemented: currently only the implementation for equal fespaces available");
    }

    template <class CG, class N, class Quad, class LocalFct, class Vec>
    void assemble(CG const& contextGeo, N const& node, Quad const& quad, LocalFct const& localFct,
      Vec& elementVector) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 2,
        "Expression must be a vector of size two.");
      static_assert(N::isPower, "von Karman operator only implemented for power nodes");
      static_assert(CG::dow == 2, "von Karman operator only implemented in 2d!");

      getElementVectorStandard(contextGeo, quad, node, localFct, elementVector);
    }

  protected:
    template <class CG, class QR, class N, class LocalFct, class Vec>
    void getElementVectorStandard(CG const& contextGeo, QR const& quad, N const& node,
      LocalFct const& localFct, Vec& elementVector) const
    {
      std::size_t size = node.child(0).size();

      using RangeFieldType = typename N::ChildType::LocalBasis::Traits::RangeFieldType;
      using WorldMatrix = Dune::FieldMatrix<RangeFieldType, CG::dow, CG::dow>;
      std::vector<WorldMatrix> hessians;

      for (auto const& qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
            "Fourth order operators are only implemented for affine transformations");
// The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        auto valueOld = localFct(local);
        auto const& displacementOld = valueOld[1];
        auto const& stressOld = valueOld[0];
        auto df = derivativeOf(localFct, tag::gradient{});
        auto Hf = derivativeOf(df, tag::gradient{});
        auto hessiansOld = Hf(local);
        auto const& hessianStressOld = hessiansOld[0];
        auto const& hessianDisplacementOld = hessiansOld[1];
        auto const& childNode = node.child(0);

        // The values of the shape functions on the reference element
        auto const& shapeValues = childNode.localBasisValuesAt(local);

        // The hessians of the shape functions on the reference element
        auto const& shapeHessians = childNode.localBasisHessiansAt(local);

        // Compute the shape function gradients on the real element
        hessians.resize(shapeHessians.size());

        for (std::size_t i = 0; i < hessians.size(); ++i)
          hessians[i]
          = jacobian * Dune::MatVec::as_matrix(shapeHessians[i]) * Dune::transpose(jacobian);

        auto const& stressNode = node.child(0);
        auto const& displacementNode = node.child(1);

        for (std::size_t i = 0; i < size; ++i)
        {
          const auto localStress_i = stressNode.localIndex(i);
          const auto localDisp_i = displacementNode.localIndex(i);

          if constexpr (update)
          {
            elementVector[localStress_i]
              += ((Impl::sumDiagonal(hessianStressOld) * Impl::sumDiagonal(hessians[i])
                - (1. + tag_.Nu) * Impl::mongeAmpereStructure(hessianStressOld, hessians[i]))
                / tag_.E
                + 0.5 * displacementOld
                * Impl::mongeAmpereStructure(hessians[i], hessianDisplacementOld))
              * factor;
            elementVector[localDisp_i]
              += (((1. - tag_.Nu)
                * Impl::mongeAmpereStructure(hessianDisplacementOld, hessians[i])
                - Impl::sumDiagonal(hessianDisplacementOld) * Impl::sumDiagonal(hessians[i]))
                * tag_.D
                + 0.5 * displacementOld
                * Impl::mongeAmpereStructure(hessianStressOld, hessians[i])
                + 0.5 * shapeValues[i]
                * Impl::mongeAmpereStructure(hessianStressOld, hessianDisplacementOld))
              * factor;
          }
          else
          {
            elementVector[localStress_i]
              -= (0.5 * shapeValues[i]
                * Impl::mongeAmpereStructure(hessianDisplacementOld, hessianDisplacementOld))
              * factor;
            elementVector[localDisp_i]
              -= (stressOld * Impl::mongeAmpereStructure(hessianDisplacementOld, hessians[i]))
              * factor;
          }
        }
      }
    }

    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const& contextGeo, QR const& quad, RN const& rowNode,
      CN const& colNode, LocalFct const& localFct,
      Mat& elementMatrix) const
    {
      std::size_t size = rowNode.child(0).size();

      using RangeFieldType = typename RN::ChildType::LocalBasis::Traits::RangeFieldType;
      using WorldMatrix = Dune::FieldMatrix<RangeFieldType, CG::dow, CG::dow>;
      std::vector<WorldMatrix> hessians;

      for (auto const& qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
            "Fourth order operators are only implemented for affine transformations");
// The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        auto valueOld = localFct(local);
        // auto stressOld = valueOld[0];
        auto displacementOld = valueOld[1];
        auto df = derivativeOf(localFct, tag::gradient{});
        // auto dfContext = df.localContext();
        auto Hf = derivativeOf(df, tag::gradient{});
        // auto HfContext = Hf.localContext();
        auto hessiansOld = Hf(local);
        auto const& hessianStressOld = hessiansOld[0];
        auto const& hessianDisplacementOld = hessiansOld[1];
        auto const& childNode = rowNode.child(0);

        // The values of the shape functions on the reference element
        auto const& shapeValues = childNode.localBasisValuesAt(local);

        // The hessians of the shape functions on the reference element
        auto const& shapeHessians = childNode.localBasisHessiansAt(local);

        // Compute the shape function gradients on the real element
        hessians.resize(shapeHessians.size());

        for (std::size_t i = 0; i < hessians.size(); ++i)
          hessians[i]
          = jacobian * Dune::MatVec::as_matrix(shapeHessians[i]) * Dune::transpose(jacobian);

        auto const& rowStress = rowNode.child(0);
        auto const& rowDisplacement = rowNode.child(1);
        auto const& colStress = colNode.child(0);
        auto const& colDisplacement = colNode.child(1);

        auto const& D = tag_.D;
        auto const& E = tag_.E;
        auto const& Nu = tag_.Nu;

        for (std::size_t i = 0; i < size; ++i)
        {
          const auto localStress_i = rowStress.localIndex(i);
          const auto localDisp_i = rowDisplacement.localIndex(i);

          for (std::size_t j = 0; j < size; ++j)
          {
            const auto localStress_j = colStress.localIndex(j);
            const auto localDisp_j = colDisplacement.localIndex(j);

            elementMatrix[localStress_i][localStress_j]
              -= (Impl::sumDiagonal(hessians[i]) * Impl::sumDiagonal(hessians[j])
                - (1. + Nu) * Impl::mongeAmpereStructure(hessians[i], hessians[j]))
              * factor / E;

            elementMatrix[localDisp_i][localDisp_j]
              += (D
                * (Impl::sumDiagonal(hessians[i]) * Impl::sumDiagonal(hessians[j])
                  - (1. - Nu) * Impl::mongeAmpereStructure(hessians[i], hessians[j]))
                - 0.5
                * (shapeValues[i]
                  * Impl::mongeAmpereStructure(hessianStressOld, hessians[j])
                  + shapeValues[j]
                  * Impl::mongeAmpereStructure(hessianStressOld, hessians[i])))
              * factor;

            elementMatrix[localDisp_i][localStress_j]
              -= 0.5
              * (shapeValues[i] * Impl::mongeAmpereStructure(hessians[j], hessianDisplacementOld)
                + displacementOld * Impl::mongeAmpereStructure(hessians[j], hessians[i]))
              * factor;

            elementMatrix[localStress_i][localDisp_j]
              -= 0.5
              * (shapeValues[j] * Impl::mongeAmpereStructure(hessians[i], hessianDisplacementOld)
                + displacementOld * Impl::mongeAmpereStructure(hessians[j], hessians[i]))
              * factor;
          }
        }
      }
    }

  private:
    tag::VonKarmanIteration<update> tag_;
  };

  template <bool update, class LC>
  struct GridFunctionOperatorRegistry<tag::VonKarmanIteration<update>, LC>
  {
    static constexpr int degree = 4;
    using type = FourthOrderVonKarmanIteration<update>;
  };

  /** @} **/

} // end namespace AMDiS
