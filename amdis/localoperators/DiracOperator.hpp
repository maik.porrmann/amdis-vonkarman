#pragma once

#include <type_traits>

#include <dune/common/hybridutilities.hh>

#include <amdis/LocalOperator.hpp>
#include <amdis/common/Order.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/
  template <class MultiIndex, class K, int dim, int derivativeOrder>
  class DiracLocalOperator;

  template <class MultiIndex, class K, int dim, int derivativeOrder = 0>
  class DiracOperator
  {
    // TODO add concept K is assignable from double
    Dune::FieldVector<K, dim> location_;

  public:
    /// \brief Constructor. Stores a copy of all passed gridfunctions
    DiracOperator() : location_(0.) {}
    DiracOperator(Dune::FieldVector<K, dim> x) : location_(x) {}

    template <class GridView>
    void update(GridView const &)
    {
      static_assert(GridView::dimensionworld == dim,
                    "GridView does not match dimension of Operator!");
    }

    friend auto localOperator(DiracOperator<MultiIndex, K, dim, derivativeOrder> const &op)
    {
      return DiracLocalOperator<MultiIndex, K, dim, derivativeOrder>(op.location_);
    }

  private:
  };

  template <class MultiIndex, class K, int dim, int derivativeOrder = 0>
  class DiracLocalOperator
  {
    std::vector<MultiIndex> assembled_;
    bool assembleElement_ = false;
    Dune::FieldVector<K, dim> location_;

  public:
    DiracLocalOperator() : location_(0.) {}
    DiracLocalOperator(Dune::FieldVector<K, dim> x) : location_(x) {}

    template <class Element>
    void bind(Element const &element)
    {
      auto refElement = Dune::referenceElement(element.geometry());
      if (refElement.checkInside(element.geometry().local(location_)))
        assembleElement_ = true;
    }

    /// Unbinds operator from element.
    void unbind() { assembleElement_ = false; }

    template <class CG, class Node, class Vec>
    void assemble(CG const &contextGeo, Node const &node, Vec &elementVector) const
    {
      static_assert(Node::isLeaf, "Operator can be applied to Leaf-Nodes only.");
      if (!assembleElement_)
        return;

      std::size_t numLocalFe = node.size();

      // Position of the current quadrature point in the reference element
      auto &&local = contextGeo.coordinateInElement(location_);

      if constexpr (derivativeOrder == 0)
      {

        // the values of the shape functions on the reference element at the quadrature point
        auto const &shapeValues = node.localBasisValuesAt(local);

        for (std::size_t i = 0; i < numLocalFe; ++i)
        {
          const auto local_i = node.localIndex(i);
          MultiIndex global_i; // I can't get the global Index from node
          if (std::find(std::begin(assembled_), std::end(assembled_), global_i)
              == std::end(assembled_))
            elementVector[local_i] += shapeValues[i];
          else
            assembled_.push_back(global_i);
        }
      }
      else
        error_exit("Higher Derivatives of DiracDelta are not implemented"); // TODO add them!
    }
  };

#ifndef DOXYGEN
  template <class GlobalBasis, int d>
  struct DiracOperatorTerm
  {
    using MultiIndex = typename GlobalBasis::MultiIndex;
    static constexpr int derivativeOrder = d;

  private:
    using K = typename GlobalBasis::GridView::ctype;
    static constexpr int dim = GlobalBasis::GridView::dimensionworld;

  public:
    Dune::FieldVector<K, dim> location_;
  };

  template <class Context, class GB, class GridView, int derivativeOrder = 0>
  auto makeOperator(DiracOperatorTerm<GB, derivativeOrder> const &pre, GridView const &gridView)
  {
    return DiracOperator<
        typename GB::MultiIndex, typename GridView::Codim<0>::Entity::Geometry::ctype,
        GridView::template Codim<0>::Entity::Geometry::coorddimension, derivativeOrder>{
        pre.location_};
  }
#endif

  /** Generator function for constructing a dirac Operator
   * \tparam GB GlobalBasis to extract type information like MultiIndex type
   * \tparam derivativeOrder Order of the derivative of dirac distribution to be assembled
   * */
  //
  template <class GB, int derivativeOrder = 0>
  auto dirac(Dune::FieldVector<typename GB::GridView::ctype, GB::GridView::dimensionworld> x)
  {
    using Pre = DiracOperatorTerm<GB, derivativeOrder>;
    return Pre{x};
  }

  /** @} **/

} // end namespace AMDiS
