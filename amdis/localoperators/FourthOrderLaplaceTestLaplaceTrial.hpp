#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag{
    struct laplacetest_laplacetrial{};
  } // namespace tag

  namespace Impl{

    template <class K>
    K trace(Dune::FieldMatrix<K, 2, 2> const& m)
    {
      return m[0][0]+ m[1][1];
    }
  } // namespace Impl

  class LaplaceTest_LaplaceTrial
  {
  public:
    LaplaceTest_LaplaceTrial(tag::laplacetest_laplacetrial t) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode, Quad const& quad,
      LocalFct const& localFct, Mat& elementMatrix) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 1,
        "Expression must be a Scalar.");
      static_assert(RN::isLeaf && CN::isLeaf,
        "Laplace operator only implemented for leaf nodes");
      static_assert(CG::dow == 2, "laplace operator only implemented in 2d!");

      const bool sameFE = std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>;
      using Category = ValueCategory_t<typename LocalFct::Range>;

      if (sameFE)
        getElementMatrixStandard(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
      else
        error_exit(
          "Not implemented: currently only the implementation for equal fespaces available");
    }

    template <class CG, class N, class Quad, class LocalFct, class Vec>
    void assemble(CG const& contextGeo, N const& node, Quad const& quad, LocalFct const& localFct,
      Vec& elementVector) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 1,
        "Expression must be a scalar.");
      static_assert(N::isLeaf, "Laplace operator only implemented for leaf nodes");
      static_assert(CG::dow == 2, "Laplace operator only implemented in 2d!");

      getElementVectorStandard(contextGeo, quad, node, localFct, elementVector);
    }

  protected:
    template <class CG, class QR, class N, class LocalFct, class Vec>
    void getElementVectorStandard(CG const& contextGeo, QR const& quad, N const& node,
      LocalFct const& localFct, Vec& elementVector) const
    {
      std::size_t size = node.size();

      using RangeFieldType = typename N::LocalBasis::Traits::RangeFieldType;
      using WorldMatrix = Dune::FieldMatrix<RangeFieldType, CG::dow, CG::dow>;
      std::vector<WorldMatrix> hessians;

      for (auto const& qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
            "Fourth order operators are only implemented for affine transformations");
        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();

        auto rhsValues = derivativeOf(derivativeOf(localFct, tag::jacobian{}), tag::jacobian{})(local);

        // The hessians of the shape functions on the reference element
        auto const& shapeHessians = node.localBasisHessiansAt(local);

        // Compute the shape function gradients on the real element
        hessians.resize(shapeHessians.size());

        for (std::size_t i = 0; i < hessians.size(); ++i)
          hessians[i]
          = jacobian * Dune::MatVec::as_matrix(shapeHessians[i]) * Dune::transpose(jacobian);


        for (std::size_t i = 0; i < size; ++i)
        {
          const auto local_i = node.localIndex(i);
          elementVector[local_i]
            += Impl::trace(hessians[i]) * Impl::trace(rhsValues) * factor;
        }
      }
    }

    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const& contextGeo, QR const& quad, RN const& rowNode,
      CN const& colNode, LocalFct const& localFct,
      Mat& elementMatrix) const
    {
      std::size_t size = rowNode.size();

      using RangeFieldType = typename RN::LocalBasis::Traits::RangeFieldType;
      using WorldMatrix = Dune::FieldMatrix<RangeFieldType, CG::dow, CG::dow>;
      std::vector<WorldMatrix> hessians;

      for (auto const& qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
            "Fourth order operators are only implemented for affine transformations");
        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        auto value = localFct(local);

        // The hessians of the shape functions on the reference element
        auto const& shapeHessians = colNode.localBasisHessiansAt(local);

        // Compute the shape function gradients on the real element
        hessians.resize(shapeHessians.size());

        for (std::size_t i = 0; i < hessians.size(); ++i)
          hessians[i]
          = jacobian * Dune::MatVec::as_matrix(shapeHessians[i]) * Dune::transpose(jacobian);

        for (std::size_t i = 0; i < size; ++i)
        {
          const auto local_i = rowNode.localIndex(i);

          for (std::size_t j = 0; j < size; ++j)
          {
            const auto local_j = colNode.localIndex(j);

            elementMatrix[local_i][local_j]
              += Impl::trace(hessians[i]) * Impl::trace(hessians[j]) * value *factor;
          }
        }
      }
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::laplacetest_laplacetrial, LC>
  {
    static constexpr int degree = 4;
    using type = LaplaceTest_LaplaceTrial;
  };

  /** @} **/

} // end namespace AMDiS
