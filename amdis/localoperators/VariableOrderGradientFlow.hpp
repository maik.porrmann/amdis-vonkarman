#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS
{
  namespace {
    template <class V, int N, int M>
    V matrixInnerProduct(Dune::FieldMatrix<V, N, M> const& A, Dune::FieldMatrix<V, N, M> const& B)
    {
      V sum = 0.; // V should be castable from double
      for (std::size_t i = 0; i < N; ++i)
        for (std::size_t j = 0; j < M; ++j)
          sum += A[i][j] * B[i][j];
      return sum;
    }
  }
  /**
   * \addtogroup operators
   * @{
   **/
  namespace tag
  {
    template <int deg>
    struct gradientFlow
    {
      static constexpr int degree = deg;
      double tau = 0.1;
      double factor = 1.;
    };
  } // namespace tag

  /// 2i-order operator $ \langle H \psi, c\, H \phi\rangle \f$, or \f$
  template <int deg>
  class VariableOrderGradientFlow
  {
  public:
    VariableOrderGradientFlow(tag::gradientFlow<deg> t) : tag_(t) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const &contextGeo, RN const &rowNode, CN const &colNode, Quad const &quad,
                  LocalFct const &localFct /*unused*/, Mat &elementMatrix) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 1
                        || (static_num_rows_v<expr_value_type> == CG::dow
                            && static_num_cols_v<expr_value_type> == CG::dow),
                    "Expression must be of scalar or matrix type.");
      static_assert(RN::isLeaf && CN::isLeaf, "Operator can be applied to Leaf-Nodes only.");

      const bool sameFE = std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>;
      // const bool sameNode = rowNode.treeIndex() == colNode.treeIndex();
      using Category = ValueCategory_t<typename LocalFct::Range>;

      // if (sameFE && sameNode)
      //   getElementMatrixOptimized(contextGeo, quad, rowNode, colNode, localFct, elementMatrix,
      //                             Category{});
      // else
      if (sameFE)
        getElementMatrixStandard(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
      else
        error_exit(
            "Not implemented: currently only the implementation for equal fespaces available");
    }

    template <class CG, class RN, class Quad, class LocalFct, class Vec>
    void assemble(CG const &contextGeo, RN const &rowNode, Quad const &quad,
                  LocalFct const &localFct, Vec &elementVector) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 1
                        || (static_num_rows_v<expr_value_type> == CG::dow
                            && static_num_cols_v<expr_value_type> == CG::dow),
                    "Expression must be of scalar or matrix type.");
      static_assert(RN::isLeaf, "Operator can be applied to Leaf-Nodes only.");
      std::size_t size = rowNode.size();

      using RangeFieldType = typename RN::LocalBasis::Traits::RangeFieldType;
      using WorldMatrix = Dune::FieldMatrix<RangeFieldType, CG::dow, CG::dow>;
      std::vector<WorldMatrix> hessians;
      using WorldVector = Dune::FieldVector<RangeFieldType, CG::dow>;
      std::vector<WorldVector> gradients;

      for (auto const &qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto &&local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto JIT = contextGeo.geometry().jacobianInverseTransposed(local);

        // TODO think about ways to obtain the derivative of the jacobian
        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
                     "Fourth order operators are only implemented for affine transformations");
        // The multiplicative factor in the integral transformation formula
        const auto factor
            = contextGeo.integrationElement(qp.position()) * qp.weight() / tag_.tau * tag_.factor;

        const auto exprValue = localFct(local);

        auto const &shapeValues = rowNode.localBasisValuesAt(local);

        for (std::size_t i = 0; i < size; ++i)
        {
          const auto local_i = rowNode.localIndex(i);

          elementVector[local_i] += exprValue * shapeValues[i] * factor;
        }

        if constexpr (tag_.degree >= 1)
        {

          auto const &shapeJacobians = rowNode.localBasisJacobiansAt(local);

          // Compute the shape function gradients on the real element
          gradients.resize(shapeJacobians.size());

          for (std::size_t i = 0; i < gradients.size(); ++i)
            JIT.mv(Dune::MatVec::as_vector(shapeJacobians[i]), gradients[i]);

          const auto &df = derivativeOf(localFct, tag::gradient{});
          const auto dfValue = df(local);

          for (std::size_t i = 0; i < size; ++i)
          {
            const auto local_i = rowNode.localIndex(i);

            elementVector[local_i] += gradients[i] * dfValue * factor;
          }
        }
        if constexpr (tag_.degree == 2)
        {

          // The gradients of the shape functions on the reference element
          auto const &shapeHessians = rowNode.localBasisHessiansAt(local);

          // Compute the shape function gradients on the real element
          hessians.resize(shapeHessians.size());

          for (std::size_t i = 0; i < hessians.size(); ++i)
            hessians[i] = JIT * Dune::MatVec::as_matrix(shapeHessians[i]) * Dune::transpose(JIT);

          const auto &ddf = derivativeOf(derivativeOf(localFct, tag::gradient{}), tag::gradient{});
          const auto ddfValue = ddf(local);

          for (std::size_t i = 0; i < size; ++i)
          {
            const auto local_i = rowNode.localIndex(i);

            elementVector[local_i] += factor * matrixInnerProduct(hessians[i], ddfValue);
          }
        }
      }
    }

  protected:
    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const &contextGeo, QR const &quad, RN const &rowNode,
                                  CN const &colNode, LocalFct const &localFct,
                                  Mat &elementMatrix) const
    {
      std::size_t size = rowNode.size();

      using RangeFieldType = typename RN::LocalBasis::Traits::RangeFieldType;
      using WorldMatrix = Dune::FieldMatrix<RangeFieldType, CG::dow, CG::dow>;
      std::vector<WorldMatrix> hessians;
      using WorldVector = Dune::FieldVector<RangeFieldType, CG::dow>;
      std::vector<WorldVector> gradients;
      for (auto const &qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto &&local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto JIT = contextGeo.geometry().jacobianInverseTransposed(local);

        // TODO think about ways to obtain the derivative of the jacobian
        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
                     "Fourth order operators are only implemented for affine transformations");
        // The multiplicative factor in the integral transformation formula
        const auto factor
            = contextGeo.integrationElement(qp.position()) * qp.weight() / tag_.tau * tag_.factor;

        // const auto exprValue = localFct(local);

        auto const &shapeValues = rowNode.localBasisValuesAt(local);

        for (std::size_t i = 0; i < size; ++i)
        {
          const auto local_i = rowNode.localIndex(i);
          for (std::size_t j = 0; j < size; ++j)
          {
            const auto local_j = colNode.localIndex(j);
            elementMatrix[local_i][local_j] += shapeValues[i] * shapeValues[j] * factor;
          }
        }

        if constexpr (tag_.degree >= 1)
        {

          auto const &shapeJacobians = rowNode.localBasisJacobiansAt(local);

          // Compute the shape function gradients on the real element
          gradients.resize(shapeJacobians.size());

          for (std::size_t i = 0; i < gradients.size(); ++i)
            JIT.mv(Dune::MatVec::as_vector(shapeJacobians[i]), gradients[i]);

          for (std::size_t i = 0; i < size; ++i)
          {
            const auto local_i = rowNode.localIndex(i);
            for (std::size_t j = 0; j < size; ++j)
            {
              const auto local_j = colNode.localIndex(j);
              elementMatrix[local_i][local_j] += gradients[i] * gradients[j] * factor;
            }
          }
        }
        if constexpr (tag_.degree == 2)
        {

          // The gradients of the shape functions on the reference element
          auto const &shapeHessians = rowNode.localBasisHessiansAt(local);

          // Compute the shape function gradients on the real element
          hessians.resize(shapeHessians.size());

          for (std::size_t i = 0; i < hessians.size(); ++i)
            hessians[i] = JIT * shapeHessians[i][0] * Dune::transpose(JIT);

          for (std::size_t i = 0; i < size; ++i)
          {
            const auto local_i = rowNode.localIndex(i);
            for (std::size_t j = 0; j < size; ++j)
            {
              const auto local_j = colNode.localIndex(j);
              elementMatrix[local_i][local_j]
                  += factor * matrixInnerProduct(hessians[i], hessians[j]);
            }
          }
        }
      }
    }

  private:
    tag::gradientFlow<deg> tag_;
  };

  template <int deg, class LC>
  struct GridFunctionOperatorRegistry<tag::gradientFlow<deg>, LC>
  {
    static constexpr int degree = 2 * deg;
    using type = VariableOrderGradientFlow<deg>;
  };

  /** @} **/

} // end namespace AMDiS
