#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag{
    struct normal_grad_test{};
  } // namespace tag

  namespace Impl{
    template<class T>
    Dune::FieldVector<T, 2> normalTo(Dune::FieldVector<T, 2> in)
    {
      return {in[1], -in[0]};
    }
  } // namespace Impl

/**
 * \brief Fourth-order operator providing the left and right hand side to a newton iteration for
 * the von Karman plate problem in stressfunction description.
 * \tparam update Boolean indicating whether the newton iteration solves for and update $du$ such
 *that $u_{n+1} = u_n + du$ (true) or the next solution $u_{n+1}$ directly (false).
 **/
  class NormalGradOperator
  {
  public:
    NormalGradOperator(tag::normal_grad_test) {}

    template <class CG, class N, class Quad, class LocalFct, class Vec>
    void assemble(CG const& contextGeo, N const& node, Quad const& quad, LocalFct const& localFct,
      Vec& elementVector) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 2,
        "Expression must be a Vector of size two .");
      static_assert(N::isLeaf, "Normal Grad operator only implemented for leaf nodes");
      static_assert(CG::dow == 2, "Normal Grad operator only implemented in 2d!");

      getElementVectorStandard(contextGeo, quad, node, localFct, elementVector);
    }

  protected:
    template <class CG, class QR, class N, class LocalFct, class Vec>
    void getElementVectorStandard(CG const& contextGeo, QR const& quad, N const& node,
      LocalFct const& localFct, Vec& elementVector) const
    {
      std::size_t size = node.size();

      using RangeFieldType = typename N::LocalBasis::Traits::RangeFieldType;
      using WorldVector = Dune::FieldVector<RangeFieldType, CG::dow>;
      std::vector<WorldVector> jacobians;

      for (auto const& qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
            "Fourth order operators are only implemented for affine transformations");
        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();

        auto rhsValues = localFct(local);

        // The jacobian of the shape functions on the reference element
        auto const& shapeJacobians = node.localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        jacobians.resize(shapeJacobians.size());

        for (std::size_t i = 0; i < jacobians.size(); ++i)
          jacobian.mv(Dune::MatVec::as_vector(shapeJacobians[i]), jacobians[i]);


        for (std::size_t i = 0; i < size; ++i)
        {
          const auto local_i = node.localIndex(i);
          elementVector[local_i]
            += rhsValues * Impl::normalTo(jacobians[i]) * factor;
        }
      }
    }


  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::normal_grad_test, LC>
  {
    static constexpr int degree = 1;
    using type = NormalGradOperator;
  };

  /** @} **/

} // end namespace AMDiS
