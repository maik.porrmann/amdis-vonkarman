#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct clampedPlate
    {
    };
  } // namespace tag

  /// fourth-order operator solving the clamped plate problem. The given expression describes
  /// poissons ratio
  class FourthOrderClampedPlate
  {
  public:
    FourthOrderClampedPlate(tag::clampedPlate) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const &contextGeo, RN const &rowNode, CN const &colNode, Quad const &quad,
                  LocalFct const &localFct, Mat &elementMatrix) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 1
                        || (static_num_rows_v<expr_value_type> == CG::dow
                            && static_num_cols_v<expr_value_type> == CG::dow),
                    "Expression must be of scalar or matrix type.");
      static_assert(RN::isLeaf && CN::isLeaf, "Operator can be applied to Leaf-Nodes only.");

      const bool sameFE = std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>;
      const bool sameNode = rowNode.treeIndex() == colNode.treeIndex();
      using Category = ValueCategory_t<typename LocalFct::Range>;

      // if (sameFE && sameNode)
      //   getElementMatrixOptimized(contextGeo, quad, rowNode, colNode, localFct, elementMatrix,
      //                             Category{});
      // else
      if (sameFE)
        getElementMatrixStandard(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
      else
        error_exit(
            "Not implemented: currently only the implementation for equal fespaces available");
    }

  protected:
    template <class K, int n>
    K sumDiagonal(Dune::FieldMatrix<K, n, n> const &m) const
    {
      K sum = 0.;
      for (int i = 0; i < n; ++i)
        sum += m[i][i];
      return sum;
    }

    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const &contextGeo, QR const &quad, RN const &rowNode,
                                  CN const &colNode, LocalFct const &localFct,
                                  Mat &elementMatrix) const
    {
      std::size_t size = rowNode.size();

      using RangeFieldType = typename RN::LocalBasis::Traits::RangeFieldType;
      using WorldMatrix = Dune::FieldMatrix<RangeFieldType, CG::dow, CG::dow>;
      static_assert(CG::dow == 2, "Clamped Plate operator only implemented in 2d!");
      std::vector<WorldMatrix> hessians;

      for (auto const &qp : quad)
      {
        // Position of the current quadrature point in the reference element
        auto &&local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        // TODO think about ways to obtain the derivative of the jacobian
        if (!contextGeo.geometry().affine())
          DUNE_THROW(Dune::NotImplemented,
                     "Fourth order operators are only implemented for affine transformations");
        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        const auto exprValue = localFct(local);

        // The gradients of the shape functions on the reference element
        auto const &shapeHessians = rowNode.localBasisHessiansAt(local);

        // Compute the shape function gradients on the real element
        hessians.resize(shapeHessians.size());

        for (std::size_t i = 0; i < hessians.size(); ++i)
          hessians[i] = jacobian * shapeHessians[i][0] * Dune::transpose(jacobian);

        for (std::size_t i = 0; i < size; ++i)
        {
          const auto local_i = rowNode.localIndex(i);
          for (std::size_t j = 0; j < size; ++j)
          {
            const auto local_j = colNode.localIndex(j);
            elementMatrix[local_i][local_j]
                += factor
                 * (sumDiagonal(hessians[i]) * sumDiagonal(hessians[j])
                    - (1. - exprValue)
                          * (hessians[i][0][0] * hessians[j][1][1]
                             + hessians[i][1][1] * hessians[j][0][0]
                             - 2. * hessians[i][0][1] * hessians[j][0][1]));
          }
        }
      }
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::clampedPlate, LC>
  {
    static constexpr int degree = 4;
    using type = FourthOrderClampedPlate;
  };

  /// Create a second-order term
  template <class Expr>
  auto fotClampedPlate(Expr &&expr, int quadOrder = -1)
  {
    return makeOperator(tag::clampedPlate{}, FWD(expr), quadOrder);
  }

  /** @} **/

} // end namespace AMDiS
