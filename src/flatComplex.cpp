#include <amdis/AMDiS.hpp>

#include <filesystem>


#include <dune/common/timer.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/scalarbasis.hh>
#include <dune/periodic/periodicgrid.hh>

#include <amdis/C1Elements.hpp>
#include <amdis/vonKarman/FlatProblem.hpp>

namespace fs = std::filesystem;
using namespace AMDiS;

template<class Traits, class TP>
class FlatComplexProblem : public FlatProblem<Traits, TP> {
  public:
    using Base = FlatProblem<Traits, TP>;
    using Problem = typename Base::Problem;
    using Base::E_;
    using Base::Nu_;
    using Base::p_;
    using Base::phi_;

    FlatComplexProblem() = delete;
    FlatComplexProblem(Environment &e, Problem const& p, TP tpPhi, std::string name,
                       std::string directory)
        : Base(e, p, tpPhi, name, directory) {}

    FlatComplexProblem(Environment &e, Problem const& p, std::string name,
                       std::string directory)
        : Base(e, p, name, directory) {}

    void addOperatorsImpl(int quadOrder) override {
      auto lhs = [](auto &&hessianRow, auto &&hessianCol, auto &&firstFactor, auto &&secondFactor) {
        return frobeniusProduct(firstFactor * cof(hessianCol) +
                                    secondFactor * tr(hessianCol) *
                                        identity(),
                                cof(hessianRow));
      };

      auto rhs = [](auto... args) { return 0.; };
      auto opLaplace = genericOperator<2>(10, lhs, rhs, Operation::Constant((1. + Nu_) / E_),
                                          Operation::Constant((-Nu_) / E_));
      p_.addMatrixOperator(opLaplace, phi_, phi_);
    }
};

template<class Traits, class Index>
FlatComplexProblem(Environment &, ProblemStat<Traits>const&, Index, std::string, std::string)
    -> FlatComplexProblem<Traits, TYPEOF(makeTreePath(std::declval<Index>()))>;

template<class Traits>
FlatComplexProblem(Environment &, ProblemStat<Traits> const&, std::string, std::string)
    -> FlatComplexProblem<Traits, TYPEOF(makeTreePath())>;

template<class Grid>
auto createGrid() {
  int initialRefinement = Parameters::get<int>("mesh->global refinements").value_or(0);

  Dune::FieldVector<double, 2> lowerleft =
      Parameters::get<Dune::FieldVector<double, 2>>("mesh->lower left")
          .value_or(Dune::FieldVector<double, 2>{0., 0.});
  Dune::FieldVector<double, 2> upperRight =
      Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right")
          .value_or(Dune::FieldVector<double, 2>{1., 1.});
  auto gridptr =
      Dune::StructuredGridFactory<Grid>::createSimplexGrid(lowerleft, upperRight, {4, 4});
  gridptr->globalRefine(initialRefinement);
  return gridptr;
}

template<class HostGrid>
auto getPeriodicGrid(std::unique_ptr<HostGrid> const &hostGridptr) {
  // auto gridptr = VonKarman::readGrid(initialRefinement, mpiHelper);
  return std::make_unique<Dune::Periodic::PeriodicGrid<HostGrid>>(*hostGridptr,
                                                                  Dune::Periodic::torus());
}

int main(int argc, char **argv) {

  std::cout << "Argc: " << argc << " Argv: ";
  for (int i = 0; i < argc; ++i)
    std::cout << std::string(argv[i]) << " ";
  std::cout << std::endl;
  std::string file = "";
  if (argc == 1)
    file = "init/flat.dat";
  else
    file = argv[1];
  Environment env(argc, argv, file);
  Dune::MPIHelper &mpiHelper = env.mpiHelper();

  std::string name = "problem";
  std::string dir = "output";
  Parameters::get("name", name);
  Parameters::get(name + "->directory", dir);

  auto path = fs::current_path() / fs::path(dir + "/init");
  if (!fs::exists(path) || !fs::is_directory(path))
    fs::create_directories(path);
  auto option = fs::copy_options::recursive | fs::copy_options::overwrite_existing;
  if (fs::current_path() / fs::path(file) !=
      fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename()) {
    fs::copy(fs::current_path() / fs::path(file),
             fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename(), option);
    fs::copy(fs::current_path() / fs::path("init/vonKarman.dat"),
             fs::current_path() / fs::path(dir + "/init/vonKarman.dat"), option);
  }

  adaptBackupAndRestorePaths(dir);

  // Set up grid
  using HostGrid = Dune::AlbertaGrid<GRIDDIM, WORLDDIM>;
  // reads rectangle domain and initial refinement from initfile
  // both pointers have to survive throughout the program
  auto hostGridptr = createGrid<HostGrid>();
  if (Parameters::get<bool>(name + "->mixed").value_or(false)) {
    // create periodic Grid
    auto gridptr = getPeriodicGrid(hostGridptr);
    // create Problem from ProblemStat
    using namespace Dune::Functions::BasisFactory;
    ProblemStat prob("NewtonStep", *gridptr, composite(argyris(), scalar(), flatLexicographic()));
    FlatComplexProblem p{env, prob, makeTreePath(Dune::Indices::_0), name, dir};
    run(p);
  } else {
    // create periodic Grid
    auto gridptr = getPeriodicGrid(hostGridptr);
    // create Problem from ProblemStat
    using namespace Dune::Functions::BasisFactory;
    ProblemStat prob("NewtonStep", *gridptr, argyris());
    FlatComplexProblem p{env, prob, name, dir};
    run(p);
  }
}
