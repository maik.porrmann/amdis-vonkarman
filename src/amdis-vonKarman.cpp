#include <amdis/vonKarman/AmdisVonKarman.hpp>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/utility/parmetisgridpartitioner.hh>
#include <amdis/C1Elements.hpp>
#include <filesystem>
namespace fs = std::filesystem;
using namespace AMDiS;

  template <int dim = 2>
  auto readGrid(int initialRefinement, Dune::MPIHelper &mpiHelper,
                std::string path = "testmesh.msh")
  {
    // create Grid
    using Grid = Dune::UGGrid<dim>;

    std::unique_ptr<Grid> gridptr;
    Dune::GmshReader<Grid> reader;
    gridptr = reader.read(path);

    gridptr->globalRefine(initialRefinement);
    auto gv = gridptr->leafGridView();

    std::vector<unsigned int> part
        = Dune::ParMetisGridPartitioner<decltype(gv)>::partition(gv, mpiHelper);
    // bool hasChanged = gridptr->loadBalance();
    return gridptr;
  }

void adaptBackupParameters(std::string outputDir)
{
  std::string tmp = "";

  tmp = Parameters::get<std::string>("NewtonStep->backup->grid").value_or("newton.grid");
  Parameters::set("NewtonStep->backup->grid", outputDir + tmp);
  tmp = Parameters::get<std::string>("GradientFlow->backup->grid").value_or("gradientflow.grid");
  Parameters::set("GradientFlow->backup->grid", outputDir + tmp);
  tmp = Parameters::get<std::string>("NewtonStep->backup->solution").value_or("newton.solution");
  Parameters::set("NewtonStep->backup->solution", outputDir + tmp);
  tmp = Parameters::get<std::string>("GradientFlow->backup->solution")
            .value_or("gradientflow.solution");
  Parameters::set("GradientFlow->backup->solution", outputDir + tmp);
  tmp = Parameters::get<std::string>("NewtonStep->restore->grid ").value_or("newton.grid");
  Parameters::set("NewtonStep->restore->grid ", outputDir + tmp);
  tmp = Parameters::get<std::string>("GradientFlow->restore->grid").value_or(" gradientflow.grid");
  Parameters::set("GradientFlow->restore->grid", outputDir + tmp);
  tmp = Parameters::get<std::string>("NewtonStep->restore->solution").value_or(" newton.solution");
  Parameters::set("NewtonStep->restore->solution", outputDir + tmp);
  tmp = Parameters::get<std::string>("GradientFlow->restore->solution")
            .value_or(" gradientflow.solution");
  Parameters::set("GradientFlow->restore->solution", outputDir + tmp);
}

int main(int argc, char **argv)
{
  std::cout << "Argc: " << argc << " Argv: ";
  for (int i = 0; i < argc; ++i)
    std::cout << std::string(argv[i]) << " ";
  std::cout << std::endl;
  std::string pathToFile = "";
  if (argc == 1)
    pathToFile = "init/newton.dat";
  else
    pathToFile = argv[1];
  Environment env(argc, argv, pathToFile);
  Dune::MPIHelper &mpiHelper = env.mpiHelper();

  int initialRefinement = Parameters::get<int>("mesh->global refinements").value_or(0);

  using HostGrid = Dune::AlbertaGrid<GRIDDIM, WORLDDIM>;
  Dune::FieldVector<double, 2> lowerleft = Parameters::get<Dune::FieldVector<double, 2>>("mesh->lower left").value_or(Dune::FieldVector<double, 2>{0., 0.});
  Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});
  auto hostGridptr = Dune::StructuredGridFactory<HostGrid>::createSimplexGrid(lowerleft, upperRight, {4,4});

  hostGridptr->globalRefine(initialRefinement);



  // create Problem from ProblemStat
  using namespace Dune::Functions::BasisFactory;
  std::string file = "";
  if (argc == 1)
    file = "init/comparisonLucas.dat";
  else
    file = argv[1];

  std::string name = "problem";
  std::string dir = "output";
  Parameters::get("name", name);
  Parameters::get(name + "->directory", dir);
  adaptBackupParameters(dir);
  ProblemStat prob("NewtonStep", *hostGridptr, power<2>(argyris(), flatLexicographic()));
  VonKarman::VonKarmanProblem p{env, prob, name, dir};


  auto path = fs::current_path() / fs::path(dir + "/init");
  if (!fs::exists(path) || !fs::is_directory(path))
    fs::create_directories(path);
  auto option = fs::copy_options::recursive | fs::copy_options::overwrite_existing;
  if (fs::current_path() / fs::path(file) != fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename())
  {
    fs::copy(fs::current_path() / fs::path(file), fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename(),
      option);
    fs::copy(fs::current_path() / fs::path("init/vonKarman.dat"),
      fs::current_path() / fs::path(dir + "/init/vonKarman.dat"), option);
  }
  run(p);
}
