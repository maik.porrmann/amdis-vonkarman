#include <amdis/AMDiS.hpp>
#include <amdis/vonKarman/AmdisVonKarman.hpp>
#include <amdis/C1Elements.hpp>
#include <filesystem>
#include <dune/periodic/periodicgrid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/albertagrid.hh>

#include <dune/common/timer.hh>
namespace fs = std::filesystem;
using namespace AMDiS;

int main(int argc, char** argv)
{

  std::cout << "Argc: " << argc << " Argv: ";
  for (int i = 0; i < argc; ++i)
    std::cout << std::string(argv[i]) << " ";
  std::cout << std::endl;
  std::string file = "";
  if (argc == 1)
    file = "init/LucasScale.dat";
  else
    file = argv[1];
  Environment env(argc, argv, file);
  Dune::MPIHelper& mpiHelper = env.mpiHelper();

  Dune::Timer timer;
  int initialRefinement = Parameters::get<int>("mesh->global refinements").value_or(0);
  // auto gridptr = VonKarman::readGrid(initialRefinement, mpiHelper);
  using HostGrid = Dune::AlbertaGrid<GRIDDIM, WORLDDIM>;
  Dune::FieldVector<double, 2> lowerleft = Parameters::get<Dune::FieldVector<double, 2>>("mesh->lower left").value_or(Dune::FieldVector<double, 2>{0., 0.});
  Dune::FieldVector<double, 2> upperRight = Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right").value_or(Dune::FieldVector<double, 2>{1., 1.});
  auto hostGridptr = Dune::StructuredGridFactory<HostGrid>::createSimplexGrid(lowerleft, upperRight, {4,4});

  hostGridptr->globalRefine(initialRefinement);


  auto gridptr = std::make_unique<Dune::Periodic::PeriodicGrid<HostGrid>>(*hostGridptr, Dune::Periodic::torus());

  // create Problem from ProblemStat
  using namespace Dune::Functions::BasisFactory;


  std::string name = "problem";
  std::string dir = "output";
  Parameters::get("name", name);
  Parameters::get(name + "->directory", dir);

  ProblemStat prob("NewtonStep", *gridptr, power<2>(argyris(), flatLexicographic()));
  VonKarman::VonKarmanProblem p(env, prob, name, dir);
  static_assert(not TYPEOF(p)::Lagrange);
  auto path = fs::current_path() / fs::path(dir + "/init");
  if (!fs::exists(path) || !fs::is_directory(path))
    fs::create_directories(path);
  auto option = fs::copy_options::recursive | fs::copy_options::overwrite_existing;
  if (fs::current_path() / fs::path(file) != fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename())
  {
    fs::copy(fs::current_path() / fs::path(file), fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename(),
      option);
    fs::copy(fs::current_path() / fs::path("init/vonKarman.dat"),
      fs::current_path() / fs::path(dir + "/init/vonKarman.dat"), option);
  }
  run(p);
}
