add_amdis_executable(
  NAME "amdis-vonKarman"
  SOURCES amdis-vonKarman.cpp
  DIM 2 DOW 2
  ALBERTA_GRID)
add_amdis_executable(
    NAME "periodicPointConstraint"
    SOURCES periodicPointConstraint.cpp
    DIM 2 DOW 2
    ALBERTA_GRID)

add_amdis_executable(
    NAME "periodicLagrange"
    SOURCES periodicLagrange.cpp
    DIM 2 DOW 2
    ALBERTA_GRID)

add_amdis_executable(
    NAME "periodicFlat"
    SOURCES periodicFlat.cpp
    DIM 2 DOW 2
    ALBERTA_GRID)

add_amdis_executable(
    NAME "flatComplex"
    SOURCES flatComplex.cpp
    DIM 2 DOW 2
    ALBERTA_GRID)

add_amdis_executable(
    NAME "flatAW"
    SOURCES flatAW.cpp
    DIM 2 DOW 2
    ALBERTA_GRID)