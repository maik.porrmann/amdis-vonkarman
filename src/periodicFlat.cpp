#include <amdis/AMDiS.hpp>
#include <amdis/C1Elements.hpp>
#include <amdis/vonKarman/FlatProblem.hpp>
#include <dune/functions/functionspacebases/scalarbasis.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/periodic/periodicgrid.hh>
#include <filesystem>

#include <dune/common/timer.hh>
namespace fs = std::filesystem;
using namespace AMDiS;

template<class Grid>
auto createGrid() {
  int initialRefinement = Parameters::get<int>("mesh->global refinements").value_or(0);

  Dune::FieldVector<double, 2> lowerleft =
      Parameters::get<Dune::FieldVector<double, 2>>("mesh->lower left")
          .value_or(Dune::FieldVector<double, 2>{0., 0.});
  Dune::FieldVector<double, 2> upperRight =
      Parameters::get<Dune::FieldVector<double, 2>>("mesh->upper right")
          .value_or(Dune::FieldVector<double, 2>{1., 1.});
  auto gridptr =
      Dune::StructuredGridFactory<Grid>::createSimplexGrid(lowerleft, upperRight, {4, 4});
  gridptr->globalRefine(initialRefinement);
  return gridptr;
}

template<class HostGrid>
auto getPeriodicGrid(std::unique_ptr<HostGrid> const &hostGridptr) {
  // auto gridptr = VonKarman::readGrid(initialRefinement, mpiHelper);
  return std::make_unique<Dune::Periodic::PeriodicGrid<HostGrid>>(*hostGridptr,
                                                                  Dune::Periodic::torus());
}

int main(int argc, char **argv) {

  std::cout << "Argc: " << argc << " Argv: ";
  for (int i = 0; i < argc; ++i)
    std::cout << std::string(argv[i]) << " ";
  std::cout << std::endl;
  std::string file = "";
  if (argc == 1)
    file = "init/flat.dat";
  else
    file = argv[1];
  Environment env(argc, argv, file);
  Dune::MPIHelper &mpiHelper = env.mpiHelper();

  std::string name = "problem";
  std::string dir = "output";
  Parameters::get("name", name);
  Parameters::get(name + "->directory", dir);

  auto path = fs::current_path() / fs::path(dir + "/init");
  if (!fs::exists(path) || !fs::is_directory(path))
    fs::create_directories(path);
  auto option = fs::copy_options::recursive | fs::copy_options::overwrite_existing;
  if (fs::current_path() / fs::path(file) !=
      fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename()) {
    fs::copy(fs::current_path() / fs::path(file),
             fs::current_path() / fs::path(dir) / "init" / fs::path(file).filename(), option);
    fs::copy(fs::current_path() / fs::path("init/vonKarman.dat"),
             fs::current_path() / fs::path(dir + "/init/vonKarman.dat"), option);
  }

  adaptBackupAndRestorePaths(dir);

  // Set up grid
  using HostGrid = Dune::AlbertaGrid<GRIDDIM, WORLDDIM>;
  // reads rectangle domain and initial refinement from initfile
  // both pointers have to survive throughout the program
  auto hostGridptr = createGrid<HostGrid>();
  if (Parameters::get<bool>(name + "->mixed").value_or(false)) {
    // create periodic Grid
    auto gridptr = getPeriodicGrid(hostGridptr);
    // create Problem from ProblemStat
    using namespace Dune::Functions::BasisFactory;
    ProblemStat prob("NewtonStep", *gridptr, composite(argyris(), scalar(), flatLexicographic()));
    FlatProblem p{env, prob, makeTreePath(Dune::Indices::_0), name, dir};
    run(p);
  } else {
    // create periodic Grid
    auto gridptr = getPeriodicGrid(hostGridptr);
    // create Problem from ProblemStat
    using namespace Dune::Functions::BasisFactory;
    ProblemStat prob("NewtonStep", *gridptr, argyris());
    FlatProblem p{env, prob, name, dir};
    run(p);
  }
}
