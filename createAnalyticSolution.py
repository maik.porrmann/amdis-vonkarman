from sympy import *

def inc(u):
  assert(shape(u) == (2,2))
  return diff(u[0,0], y, y) + diff(u[1,1], y, y) - diff(u[0,1], x, y) - diff(u[1,0], y, x)

def airy(phi):
  return Matrix(((diff(phi, y,y), - diff(phi, x,y)),(-diff(phi, y,x), diff(phi, x,x))))

def C(Nu, E, u):
  return (1+Nu)/E * u - Nu/E * trace(u) * Identity(2)

def E(la, mu, u):
  return 2*mu*u + la*trace(u)*Identity(2)

def div(tau):
  return Matrix((diff(tau[0,0],x) + diff(tau[0,1],y), diff(tau[1,0],x) + diff(tau[1,1],y)))

def symgrad(v):
  return Matrix(((diff(v[0], x), 1/2*(diff(v[0], y) + diff(v[1], x))), \
                 ( 1/2*(diff(v[0], y) + diff(v[1], x)), diff(v[1],y))))

def getAnalyticRHS(l, mu, Nu, E, sigma):


  u = C(Nu, E, sigma)
  # pprint(sigma)
  phi = inc(u)
  f = div(sigma)
  # pprint(f)

  rhs_K2 = C(Nu, E, airy(phi)) -symgrad(f)
  rhs_K1 = inc(rhs_K2)
  return rhs_K1, rhs_K2

if __name__ == "__main__":

  # Y, nu = symbols("E nu")
  Y = 1.
  nu = 0.25
  l = Y*nu/((1+nu)*(1-nu))
  mu = Y/(2*(1+nu))

  x,y = symbols("x y")
  u = Matrix(((1/2*x*x,0),(0,1/2. *y*y)))
  s = E(l, mu, u)
  a,b,c,d = Function("a")(x,y), Function("b")(x,y), Function("c")(x,y), Function("d")(x,y)
  # s = Matrix(((a,b),(c,d)))
  # s = MatrixSymbol("sigma", 2,2)(x,y)
  print("Stress:")
  pprint(s)
  print("Rhs k=1, k=2")
  pprint(getAnalyticRHS(l, mu, Y, nu, s))