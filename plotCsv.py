#! /usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import json
from os import path
import math
import numpy as np
import matplotlib
from cycler import cycler
#matplotlib.rcParams['font.family'] = 'serif'
#matplotlib.rcParams['font.serif'] = ['cmr10']
#matplotlib.rcParams['font.size'] = 13
#matplotlib.rcParams['mathtext.fontset'] = 'cm'
#matplotlib.rcParams['axes.prop_cycle']="cycler('color', ['0C5DA5', '00B945', 'FF9500', 'FF2C00', '845B97', '474747', '9e9e9e'])"
matplotlib.rcParams['axes.prop_cycle']="cycler('color', ['b', 'r', 'g', 'm', 'c', 'k'])+cycler('marker',['2','p', 'x','*','D','+'])"

#plt.style.use("science")

def adaptList(which):
    if which is None:
        return which
    newList = []
    for s in which:
        try:
            newList.append(int(s))
        except ValueError:
            return which
    return newList

def plotFile(file, sep, subplots, xAxis, which, save = False, displayTitle = True):
    #check whether json file is present
    fileBase = file[:-4]
    values = dict()
    if (path.exists(fileBase+".json")):
      with open(fileBase+".json",'r') as f:
        values = json.load(f, strict = False)
    description = values.pop("description","")
    if not displayTitle:
      t = values.pop("title","")

    df = pd.read_csv(file, sep = sep)
    logx = False
    logy = values.pop("logy", "") == "true"
    
    #parse xAxis argument to key
    try:
      print(xAxis)
      index = int(xAxis[0])
      xaxis = df.keys()[index]
    except ValueError as e:
      assert(xAxis in df.keys())
    
    
    #if 'h' in df.columns:
      #xaxis = 'h'
      #if not df.columns[0] == 'h':
        #df = df.drop(df.columns[0], axis = 1)
      #logx = True
    convergenceOrder = values.pop("convergenceOrder","dummy")
    lstyle = '-x'
    if len(df.index) > 10:
      lstyle = '-.'
    legend = values.pop("legend", "True")
    fig, ax = plt.subplots(nrows=1, ncols=1)
    which = adaptList(which)
    ax = df.plot(x=xaxis, y=which,ax = ax, subplots=subplots,# style = lstyle,
                 logy=logy, logx = logx, **values)
    if logx:
      ax.set_xticks(df['h'][::2])
      ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
      ax.get_xaxis().set_minor_formatter(matplotlib.ticker.NullFormatter())
      #ax.get_xaxis().set_minor_locator(matplotlib.ticker.LogLocator())
    if logy:
      
      y_major = matplotlib.ticker.LogLocator(base = 100.0)
      ax.yaxis.set_major_locator(y_major)
      y_minor = matplotlib.ticker.LogLocator(base = 100.0, subs = (0,20,40,80,100))
      ax.yaxis.set_minor_locator(y_minor)
      ax.get_yaxis().set_major_formatter(matplotlib.ticker.LogFormatter())
      ax.get_yaxis().set_minor_formatter(matplotlib.ticker.NullFormatter())
      #ax.get_yaxis().set_minor_locator(matplotlib.ticker.LogLocator(subs =(0.2,0.4,0.6,0.8)))
    if convergenceOrder != "dummy" and not subplots:
      try:
        sign = 1
        convCycler=['gray', 'darkgray','silver','lightgray']
        j =0
        for letter in convergenceOrder:
          #if letter == '-':
           # sign = -1
            #continue
          #print(letter)
          i = sign * int(letter)
          #if not letter == '-':
          #  sign = 1
          if len(df['h']) > 4:
            ax.plot(df['h'][1:-1], (i/4.5)**(-i)*df['h'][1:-1]**i,color = convCycler[j%4],ls = '--', marker = '', label = "$\\mathcal{O}(h^{"+str(i)+"})$") #, alpha=0.3
          else:
            ax.plot(df['h'][1:],(i/1.5)**(-i)*df['h'][1:]**i,color = convCycler[j%4],ls = '--', marker = '', label = "$\\mathcal{O}(h^{"+str(i)+"})$") #, alpha=0.3
          j+=1
      except ValueError as e:
        print("ValueError ", letter, e)
    if not subplots:
      plt.text(0.95,0.3,description, horizontalalignment='right',
     verticalalignment='center', transform = ax.transAxes)
      if legend == "False":
        ax.get_legend().remove()
      elif legend == "outside":
        ax.legend(loc = (1.04,0), frameon = False)
      elif legend == "True":
        ax.legend(frameon = False)
      elif legend == "seperate":
        fig_leg, ax_leg = plt.subplots(nrows = 1, ncols = 1)
        l = ax_leg.legend(*ax.get_legend_handles_labels(), loc='center', frameon= False)
        lf = l.figure
        lf.canvas.draw()
        ax_leg.axis('off')
        if save:
          box = l.get_window_extent().transformed(lf.dpi_scale_trans.inverted())
          fig_leg.savefig(fileBase+"_legend.eps", bbox_inches = box)
        ax.get_legend().remove()
      else :
        ax.legend(loc = legend, frameon = False)
    if save:
        #fig.savefig(fileBase+".svg", format = 'svg', dpi = 600, bbox_inches='tight')
        #fig.savefig(fileBase+".png", format = 'png', dpi = 600, bbox_inches='tight')
        fig.savefig(fileBase+".eps", format = 'eps', bbox_inches='tight')
    else:
      plt.show()
    #plt.close(fig)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help = "file to be plotted")
    parser.add_argument("-x", "--xAxis", nargs = 1, default = [0])
    parser.add_argument("-m","--mergePlots", action = "store_true")
    parser.add_argument("-w","--which",nargs = '+', default =None)
    parser.add_argument("-s","--save", action = "store_true")
    parser.add_argument("--sep", help = "file seperator", default = ";")
    args = parser.parse_args()

    plotFile(args.file,args.sep, not args.mergePlots, args.xAxis, args.which, args.save)

