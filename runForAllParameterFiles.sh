#! /bin/bash
base="init/vonKarman.dat"
for var in "$@"
do
    if [[ "$base" == "$var" ]]; then
        echo "base"
    else
        ./build-cmake/src/amdis-vonKarman "$var"
    fi
done
